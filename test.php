<table>
	<tr>
		<th>Wedstrijd</th>
		<th>Set</th>
		<th>Team ID</th>
		<th>Score</th>
		<th>Punten</th>
	</tr>
<?php

$servername = "localhost";
$username = "mbvolley";
$password = "mbvolley";
$dbname = "mbvolley";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$spwq="SELECT `wedstrijdid`,`set` FROM `uitslag_set_team`";
$spwr = $conn->query($spwq);
$wedstrijden=array();
$sets=array();
$score=array();
$scoretot=array();
$teamids=array();

while ($spw = $spwr->fetch_assoc()) {
	if (!in_array($spw['wedstrijdid'],$wedstrijden)) {
		array_push($wedstrijden,$spw['wedstrijdid']);
	}
	if (!in_array($spw['set'],$sets)) {
		array_push($sets,$spw['set']);
	}
}
foreach ($wedstrijden as $wedstrijd) {
	foreach ($sets as $set) {
		$puntenq = "SELECT `teamid`,`score` FROM `uitslag_set_team` WHERE `wedstrijdid`='$wedstrijd' AND `set`='$set'";
		$puntenr = $conn->query($puntenq);
		while ($punten = $puntenr->fetch_assoc()) {
			$score[$punten['teamid']] = $punten['score'];
			array_push($teamids,$punten['teamid']);
		}
		if (!isset($scoretot[$teamids[0]])) {
			$scoretot[$teamids[0]] = 0;
		}
		if (!isset($scoretot[$teamids[1]])) {
			$scoretot[$teamids[1]] = 0;
		}
		if ($score[$teamids[0]] < $score[$teamids[1]]) {
			$scoretot[$teamids[1]] = $scoretot[$teamids[1]] + 2;
			$displayScoreA = 0;
			$displayScoreB = 2;
		} elseif ($score[$teamids[0]] == $score[$teamids[1]]) {
			$scoretot[$teamids[0]] = $scoretot[$teamids[0]] + 1;
			$scoretot[$teamids[1]] = $scoretot[$teamids[1]] + 1;
			$displayScoreA = 1;
			$displayScoreB = 1;
		} else {
			$scoretot[$teamids[0]] = $scoretot[$teamids[0]] + 2;
			$displayScoreA = 2;
			$displayScoreB = 0;
		}
		echo "<tr><td>$wedstrijd</td><td>$set</td><td>".$teamids[0]."</td><td>".$score[$teamids[0]]."</td><td>$displayScoreA</td></tr>";
		echo "<tr><td>$wedstrijd</td><td>$set</td><td>".$teamids[1]."</td><td>".$score[$teamids[1]]."</td><td>$displayScoreB</td></tr>";
		echo "<tr><td colspan='5'>-</td></tr>";
		$score=array();
		$teamids=array();
	}
}
arsort($scoretot);
echo "</table><table>";
echo "<tr><th>Positie</th><th>TeamID</th><th>Punten</th></tr>";
$i = 1;
foreach ($scoretot as $teamid => $punten) {
	echo "<tr><td>#$i</td><td>$teamid</td><td>$punten</td></tr>";
	$i++;
}
echo "</table>";

?>