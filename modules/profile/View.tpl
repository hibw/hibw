{block name=title}Profiel Pagina{/block}
{block name=main}
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<h3>
	  		Mijn Profiel 
	  	</h3>
	  </div>
	  <div class="panel-body">
		  <p>Welkom {$model->getFullName()},</p>
		  <p>Hier onder kunt u uw informatie vinden.</p><br />
		  <div class="col-md-6">
			  <table class="table table-striped table-hover ">
				<tr>
				  <th colspan="2">Mijn info {if $model->getGroupId() >= 4}
				  	<a href="{$model->home}/users/edit?userid={$model->getUsername()}" class="btn btn-success pull-right"><i class="fa fa-pencil fa-spin"></i></button>
				  {/if}
				  </th>
				</tr>
				<tr>
				  <td>Voornaam:</td>
				  <td>{$model->getFirstName()}</td>
				</tr>
				<tr>
				  <td>Achternaam:</td>
				  <td>{$model->getLastName()}</td>
				</tr>
				<tr>
				  <td>Email:</td>
				  <td>{$model->getEmail()}</td>
				</tr>
				<tr>
				  <td>Telefoon:</td>
				  <td>{$model->getPhone()}</td>
				</tr>
				<tr>
				  <td>Gebruikersnaam:</td>
				  <td>{$model->getUsername()}</td>
				</tr>
				<tr>
				  <td>Gebruiksgroep:</td>
				  <td>{$model->getGroupName()}</td>
				</tr>
				<tr>
				  <td>IP adres:</td>
				  <td>{$model->getIpAddress()}</td>
				</tr>
			  </table>
		  </div>
	  </div>
	</div>
{/block}