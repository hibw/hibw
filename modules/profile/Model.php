<?php

class Model extends Model\AbstractPageModel {

	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user, 'Profiel');
	}

	public function getFullName() {
		return $this -> current_user -> getFullName();
	}

	public function getFirstName() {
		return $this -> current_user -> getNameFirst();
	}

	public function getLastName() {
		return $this -> current_user -> getNameLast();
	}

	public function getEmail() {
		return $this -> current_user -> getEmail();
	}

	public function getPhone() {
		return $this -> current_user -> getTelephone();
	}

	public function getUsername() {
		return $this -> current_user -> getUserid();
	}

	public function getGroupId() {
		return $this -> current_user -> getUser_Groups()->getGroupid();
	}
	
	public function getGroupName() {
		return ucfirst($this -> current_user -> getUser_Groups() -> getGroupName());
	}

	public function getIpAddress() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	public function showAllInfo() {
		$value = var_dump($this -> current_user);
		return $value;
	}

}
