{block name=title}Ledenbeheer{/block}
{block name=main}
		<main class="container">
		<div class="well"><h1>Team {$model->item->getId()}: {$model->item->getNaam()}</h1></div>
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#spelers" aria-controls="spelers" role="tab" data-toggle="tab">De spelers</a></li>
				<li role="presentation"><a href="#wedstrijden" aria-controls="wedstrijden" role="tab" data-toggle="tab">Wedstrijden</a></li>
				<li role="presentation"><a href="#uitslagen" aria-controls="uitslagen" role="tab" data-toggle="tab">Uitslagen</a></li>
				<li role="presentation"><a href="#statistieken" aria-controls="statistieken" role="tab" data-toggle="tab">Statistieken</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="spelers">
					{if $model->item->countLids()>0}
						<table class="table table-striped">
							{foreach $model->item->getLids() as $lid}
								<tr>
									<td class="col-sm-1">
										<i class="fa fa-user fa-3x"></i>
									</td>
									<td class="col-sm-11">
										<strong>{$lid->getNaam()}</strong><br/>
										<small>Al 3 sezoenen actief</small>
									</td>
								</tr>
							{/foreach} 
						</table>
					{else}
						<div class="alert alert-warning" role="alert">
							<i class="fa fa-exclamation-triangle"></i> Er zijn geen spelers in dit team
						</div>
					{/if}
				</div>
				<div role="tabpanel" class="tab-pane fade" id="wedstrijden">
					{if $model->item->countTeamWedstrijds()>0}
					  <table class="table table-striped">
							<tr>
								<th>Datum</th>
								<th>Tijd</th>
								<th>Veld</th>
								<th>Tegen</th>
							</tr>
							{foreach $model->item->getTeamWedstrijds() as $w}
								<tr>
									<td class="col-sm-2">
										{$w->getDatum()->format('j F Y')}<br/>
									</td>
									<td class="col-sm-1">
										{$w->getTijd()->format('G:i')}<br/>
									</td>
									<td class="col-sm-1">
										{$w->getVeld()}<br/>
									</td>
									<td class="col-sm-10">
										<strong>{$w->getNaam()}</strong><br/>
									</td>
								</tr>
							{/foreach} 
						</table>
					{else}
						<div class="alert alert-warning" role="alert">
							<i class="fa fa-exclamation-triangle"></i> Er zijn geen wedstrijden voor dit team
						</div>
					{/if}
				</div>
				<div role="tabpanel" class="tab-pane fade" id="uitslagen">
					Hier komen de uitslagen
				</div>
				<div role="tabpanel" class="tab-pane fade" id="statistieken">
					<table class="table table-striped">
							<tr>
								<td>
									<strong>W</strong><br/>
									<small><i>Aantal gespeelde wedstrijden</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getW()}</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>P</strong><br/>
									<small><i>Competitiepunten (gewonnen sets min strafpunten)</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getP()}</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Sv</strong><br/>
									<small><i>Scores voor</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getSv()}</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>St</strong><br/>
									<small><i>Scores tegen</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getSt()}</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>S</strong><br/>
									<small><i>Score saldo (Sv - St)</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getS()}</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Str.P</strong><br/>
									<small><i>Strafpunten</i></small>
								</td>
								<td>
									<strong>{$model->item->getStatistics()->getStrp()}</strong><br/>
								</td>
							</tr>
						</table>

				</div>
			</div>

		</div>

		</main>
{/block}