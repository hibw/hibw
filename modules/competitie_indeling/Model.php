<?php

class Model extends Model\DefaultTableModel
{
		private $cachedKlas = null;

		private $cachedTeams = null;

    public function __construct($home, $uri, $module_path, $current_user) {
    	parent::__construct($home, $uri, $module_path, $current_user, 'Klas');
   	}

   	public function KlasHasTeams($klas) {
   		if($this->cachedKlas!=$klas || $this->cachedTeams == null) {
   			$this->loadTeamsForKlas($klas);
   		}
   		return sizeof($this->getTeamsForKlas($klas))>0;
   	}

   	public function getTeamsForKlas($klas) {
   		if($this->cachedKlas!=$klas || $this->cachedTeams == null) {
   			loadTeamsForKlas($klas);
   		}
   		return \Data\TeamQuery::create()->filterByKlas($klas)->find();
   	}

   	private function loadTeamsForKlas($klas) {
   		$this->cachedTeams = \Data\TeamQuery::create()->filterByKlas($klas)->find();
 			$this->cachedKlas = $klas;
   	}
}
