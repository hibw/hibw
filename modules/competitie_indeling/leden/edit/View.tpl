{block name=title}Ledenbeheer{/block}
{block name=main}
<h3>Lid details <a type="button" class="btn btn-primary" href="{$model->home}/competitie_indeling/leden">Terug</a></h3>
<form method="post">
<input type="hidden" name="action" value="{$model->state}"/>
<div class="form-group">
	 <label class="control-label" for="id">ID</label>
	 <input  type="text" class="form-control" name="id" readonly value="{$model->item->getId()}"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="naam">Naam *</label>
	 <input  type="text" class="form-control" name="naam" 
	 		required value="{$model->item->getNaam()}"/>
</div>		 
<input type="submit" class="btn btn-primary"></input>
</form>
{/block}