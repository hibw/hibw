<?php

class Model extends Model\DefaultFormModel {
	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user, 'Lid');
	}

	public function insert() {
		$this -> item = new \Data\Lid();
		$this -> update();
	}

	public function update() {
		if (isset($_REQUEST['naam']))
			$this -> item -> setNaam($_REQUEST['naam']);
		$this -> item -> save();
		$this -> success[] = 'Item aangemaakt/bijgewerkt';
		$this -> redirect = $this -> home . '/competitie_indeling/leden';
	}

}
