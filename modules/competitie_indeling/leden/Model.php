<?php

class Model extends Model\DefaultTableModel {

	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user, 'Lid');
	}

	public function isPostSet() {
		if (isset($_POST['select'])) {
			return true;
		} else {
			return false;
		}
	}

	public function getSelectedLeden() {
		foreach ($_POST['select'] as $lid_rm) {
			foreach ($this->items->getData() as $data) {
				if ($data -> getId() == $lid_rm) {
					echo "<tr><td>" . $data -> getId() . "</td>";
					echo "<td>" . $data -> getNaam() . "</td></tr>";
					echo "<input name='toRemove[]' type='hidden' value='" . $data -> getId() . "'/>";
				}
			}
		}
	}

	public function removeSelectedLeden() {
		if (isset($_POST['toRemove']) && isset($_POST['verwijderen'])) {
			foreach ($_POST['toRemove'] as $selected) {
				$item = Data\LidQuery::create() -> requirePk($selected);
				$item -> delete();
			}
			header('Location: leden');
		}
	}

}
