{block name=title}Ledenbeheer{/block}
{block name=main}
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<h3>
	  		Leden 
	  		<a type="button" class="btn btn-primary" href="{$model->uri}/edit">
	  			Nieuw lid
	  		</a>
	  	</h3>
	  </div>
	  <div class="panel-body">
	  	{if $model->isPostSet()}
		  	<div class="alert alert-info alert-dismissible" role="alert">
				<strong>U staat op het punt de volgende leden te verwijderen:</strong>
				<form method="post">
					<table class="table table-striped">
						<tr>
							<th>Lidnummer</th>
							<th>Naam</th>
						</tr>
						{$model->getSelectedLeden()}
					</table>
					<div style="text-align:right;">
						<button type="submit" class="btn btn-danger" name="verwijderen">Verwijderen</button>
						<button type="button" class="btn btn-primary" data-dismiss="alert" aria-label="Close">Annuleren</button>
					</div>
				</form>
			</div>
		{elseif !$model->isPostSet()}
			{$model->removeSelectedLeden()}
	  	{/if}
	  </div>
	  <!-- Table -->
	  <form method="post">
		  <table class="table table-striped table-hover">
		  	<tr>
		  		<th></th>
		  		<th>id</th>
		  		<th>Naam</th>
		  		<th></th>
		  	</tr>
				{foreach $model->items as $item}
				<tr>
					<td><input name="select[]" type="checkbox" id="{$item->getId()}" value="{$item->getId()}"></td>
					<td>{$item->getId()}</td>
					<td>{$item->getNaam()}</td>
					<td>
						<div class="btn-group" role="group" aria-label="edit buttons">
		  					<a href="{$model->uri}/edit?id={$item->getId()}" class="btn btn-default btn-success">
		  						<i class="fa fa-pencil"></i>
		  					</a>
		  					<a href="{$model->uri}/delete?id={$item->getId()}" class="btn btn-default btn-danger">
		  						<i class="fa fa-remove"></i>
		  					</a>
		  				</div>
		  			</td>
				</tr>
				{/foreach}
		  </table>
		<input type="submit" class="btn btn-primary" name="submit" value="Geselecteerde leden verwijderen"></input>
		</form>
	</div>
{/block}