<?php

class Model extends Model\AbstractPageModel {
	public $item;

	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user);
		$this -> item = Data\LidQuery::create() -> requirePk($_GET['id']);
	}

	public function delete() {
		$this -> item -> delete();
		$this -> success[] = 'Gebruiker verwijderd';
		$this -> redirect = $this -> home . '/competitie_indeling/leden';
	}
}
