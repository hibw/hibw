{block name=title}Ledenbeheer{/block}
{block name=main}
		<main class="container">	
			<div class="well">
				<h1>Indeling Competitie</h1>
				<p>Bekijk hier welke teams er bij elkaar in een klasse zitten. Klik op een teamnaam om meer te weten te komen van dat team.</p>
			</div>
			{if $model->hasItems()}
				{foreach $model->items as $item}
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2><i class="fa fa-cubes"></i> {$item->getNaam()}</h2>
							</div>
							<table class="table table-striped">
								{if $model->KlasHasTeams($item)}
									{foreach $model->getTeamsForKlas($item) as $team}
									<tr>
										<td><i class="fa fa-users"></i></td>
										<td><a href="{$model->uri}/team?id={$team->getId()}">{$team->getNaam()}</a></td>
									</tr>
									{/foreach}
								{else}
									<div class="alert alert-warning" role="alert">
										<i class="fa fa-exclamation-triangle"></i> Er zijn geen teams in deze klasse
									</div>
								{/if}
							</table>
							</div>
				{/foreach}
			{else}
				<div class="alert alert-warning" role="alert">'.
						<i class="fa fa-exclamation-triangle"></i> Er zijn geen klassen in deze competitie
				</div>
			{/if}
		</main>
{/block}