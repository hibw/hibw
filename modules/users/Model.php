<?php
use Model\TableColumn;

class Model extends Model\DefaultTableModel
{

    public function __construct($home, $uri, $module_path, $current_user) {
    	parent::__construct($home, $uri, $module_path, $current_user, 'User');
    	$this->columns[] = new TableColumn('Userid', 'ID');
    	$this->columns[] = new TableColumn('Fullname', 'Naam');
    	$this->columns[] = new TableColumn('Email', '<i class="fa fa-at"></i>');
    	$this->columns[] = new TableColumn('Telephone', '<i class="fa fa-phone"></i>');
		$this->columns[] = new TableColumn('groupid', 'Gebruikersgroep');
   	}

    public function getIdName() {
      return 'userid';
    }

    public function getIdValue($row) {
      return $row->getUserid();
    }

	public function getgroupid($row) {
		return ucfirst($row->getUser_Groups()->getGroupName());
	}
}
