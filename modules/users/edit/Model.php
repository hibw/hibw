<?php

class Model extends Model\DefaultFormModel {
	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user, 'User', 'userid');
	}

	public function insert() {
		$this -> item = new \Data\User();
		if (isset($_REQUEST['userid'])) {
			$this -> item -> setUserid($_REQUEST['userid']);
		}
		$this -> update();
	}

	public function update() {
		if (isset($_REQUEST['nameFirst']))
			$this -> item -> setNameFirst($_REQUEST['nameFirst']);
		if (isset($_REQUEST['nameLast']))
			$this -> item -> setNameLast($_REQUEST['nameLast']);
		if (isset($_REQUEST['email']))
			$this -> item -> setEmail($_REQUEST['email']);
		if (isset($_REQUEST['telephone']))
			$this -> item -> setTelephone($_REQUEST['telephone']);
		if (isset($_REQUEST['password']) && strlen($_REQUEST['password']) > 0 && isset($_REQUEST['password_retype']))
			$this -> item -> setPasswordWithConfirm($_REQUEST['password'], $_REQUEST['password_retype']);
		if (isset($_REQUEST['groupid']))
			$this -> item -> setGroupid($_REQUEST['groupid']);
		$this -> item -> save();
		$this -> success[] = 'Gebruiker aangemaakt/bijgewerkt';
		$this -> redirect = $this -> home . '/users';
	}

	public function retrieveGetData() {
		if (isset($_GET['userid'])) {
			$model -> item -> setUserid($_GET['userid']);
		}
	}

	public function getAllUserGroups() {
		$userGroups = \Data\User_GroupsQuery::create() -> find();
		return $userGroups;
	}

	public function returnCurrentUserId() {
		return $this->current_user->getUserid();
	}
	
	public function returnCurrentUserGroupId() {
		return $this->current_user->getUser_Groups()->getGroupid();
	}
}
