{block name=title}Gebruikersbeheer{/block}
{$model->retrieveGetData()}
{block name=main}
<h3>Gebruikersdetails 
<a type="button" class="btn btn-primary" href="{$model->home}/users">
	  			Terug
	  		</a>
	  	</h3>
<form method="post">
<input type="hidden" name="action" value="{$model->state}"/>
<div class="form-group">
	 <label class="control-label" for="userid">Gebruikersnaam *</label>
	 <input  type="text" class="form-control" name="userid" 
	 		{if $model->state == 'update'}
	 			readonly
	 		{else}
	 			required minlength="2" 
	 		{/if}
	 		value="{$model->item->getUserid()}"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameFirst">Voornaam *</label>
	 <input  type="text" class="form-control" name="nameFirst" 
	 		required value="{$model->item->getNameFirst()}"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameLast">Achternaam *</label>
	 <input  type="text" class="form-control" name="nameLast" 
	 		required value="{$model->item->getNameLast()}"/>
</div>
<div class="form-group">
	 <label class="control-label" for="email">Email Adres *</label>
	 <input  type="text" class="form-control" name="email" 
	 		required value="{$model->item->getEmail()}"/>		 
</div>
<div class="form-group">
	 <label class="control-label" for="telephone">Telefoon Nummer</label>
	 <input  type="text" class="form-control" name="telephone" 
	 		value="{$model->item->getTelephone()}"/>
</div>
<div class="form-group">
	{if $model->state == 'update'}
		<label class="control-label" for="password">Wachtwoord</label>
		 <input  type="password" class="form-control" name="password" />
		 <label class="control-label" for="password_retype">Bevestig wachtwoord</label>
		 <input  type="password" class="form-control" name="password_retype" />
	{else}
		 <label class="control-label" for="password">Wachtwoord *</label>
		 <input  type="password" class="form-control" name="password" 
				required value="{$model->item->getPassword()}"/>
		 <label class="control-label" for="password_retype">Bevestig wachtwoord *</label>
		 <input  type="password" class="form-control" name="password_retype" 
				required value="{$model->item->getPassword()}"/>
	{/if}
</div>
<div>
	<label class="control-label" for="groupid">Gebruikersgroep *</label>
	<select class="form-control" name="groupid" required {if $model->item->getUserid() == $model->returnCurrentUserId()}
				 disabled 
			{/if}>
		{foreach $model->getAllUserGroups() as $userGroup}
			<option value="{$userGroup->getGroupid()}" {if $userGroup->getGroupid() == $model->item->getGroupid() && $model->state == 'update'}
				 selected 
			{elseif $model->state != 'update' && $userGroup->getGroupid() == 2}
				 selected 
			{/if}
			>{ucfirst($userGroup->getGroupName())}</option>	
		{/foreach}
	</select>
</div>
<br/><input type="submit" class="btn btn-primary" {if $model->returnCurrentUserGroupId() < 5}
	 disabled 
{/if}></input>
</form>
{/block}