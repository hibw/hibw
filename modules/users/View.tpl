{block name=title}Gebruikersbeheer{/block}
{block name=main}
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<h3>
	  		Gebruikers 
	  		<a type="button" class="btn btn-primary" href="{$model->uri}/edit">
	  			Nieuwe gebruiker
	  		</a>
	  	</h3>
	  </div>
	  <div class="panel-body">
	  </div>
	  {include file="tableview.tpl"}
	</div>
{/block}