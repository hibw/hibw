<?php

class Model extends Model\AbstractPageModel {
	public $item;

	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path, $current_user);
		$this -> item = Data\UserQuery::create() -> requirePk($_GET['userid']);
	}

	public function delete() {
		$this -> item -> delete();
		$this -> success[] = 'Gebruiker verwijderd';
		$this -> redirect = $this -> home . '/users';
	}

	public function getAllUserGroups() {
		$userGroups = \Data\User_GroupsQuery::create() -> find();
		return $userGroups;
	}

}
