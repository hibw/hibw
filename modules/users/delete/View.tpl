{block name=title}Gebruikersbeheer{/block}
{block name=main}
<h3>Bevestig verwijdering 
<a type="button" class="btn btn-primary" href="{$model->home}/users">
	  			Terug
	  		</a>
	  	</h3>
<form method="post">
<input type="hidden" name="action" value="delete"/>
<div class="form-group">
	 <label class="control-label" for="userid">Gebruikersnaam *</label>
	 <input  type="text" class="form-control" name="userid" readonly 
	 		value="{$model->item->getUserid()}"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameFirst">Voornaam *</label>
	 <input  type="text" class="form-control" name="nameFirst" readonly 
	 		required value="{$model->item->getNameFirst()}"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameLast">Achternaam *</label>
	 <input  type="text" class="form-control" name="nameLast" readonly 
	 		required value="{$model->item->getNameLast()}"/>
</div>
<div class="form-group">
	 <label class="control-label" for="email">Email Adres *</label>
	 <input  type="text" class="form-control" name="email" readonly 
	 		required value="{$model->item->getEmail()}"/>		 
</div>
<div class="form-group">
	 <label class="control-label" for="telephone">Telefoon Nummer</label>
	 <input  type="text" class="form-control" name="telephone" readonly 
	 		value="{$model->item->getTelephone()}"/>
</div>
<div class="form-group">
	 <label class="control-label" for="password">Wachtwoord</label>
	 <input  type="password" class="form-control" name="password" readonly />
	 <label class="control-label" for="password_retype">Bevestig wachtwoord</label>
	 <input  type="password" class="form-control" name="password_retype" readonly />
</div>
<div>
	<label class="control-label" for="groupid">Gebruikersgroep *</label>
	<select class="form-control" name="groupid" readonly required >
		{foreach $model->getAllUserGroups() as $userGroup}
			<option value="{$userGroup->getGroupid()}" {if $userGroup->getGroupid() == $model->item->getGroupid()}
				 selected 
			{/if}
			>{ucfirst($userGroup->getGroupName())}</option>	
		{/foreach}
	</select>
</div>
<input type="submit" class="btn btn-primary"></input>
</form>
{/block}