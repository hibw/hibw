	  <table class="table table-striped table-hover">
	  	<tr>
	  		{if $model->hasBulkActions()}<th/>{/if}
	  		{foreach $model->columns as $col}
	  			<th>{$col->headername}</th>
	  		{/foreach}
	  		{if $model->hasButtons()}<th/>{/if}

	  	</tr>
			{foreach $model->items as $row}
			<tr>
				{* Render the checkbox for bulk actions NOT IMPLEMENTED YET*}
				{if $model->hasBulkActions()}
					<td><input type="checkbox" class="cbDelete" id="{$item->getUserid()}"></td>
				{/if}

				{* Render the data *}
	  		{foreach $model->columns as $col}
	  			<td>
	  				{$model->renderCell($col, $row)}
	  			</td>
	  		{/foreach}

	  		{* Render buttons for navigation and manipulation *}
	  		{if $model->hasButtons()}
	  			<td>
	  			<div class="btn-group" role="group" aria-label="edit buttons">
	  				{if $model->hasDetails()}
	  					<a href="{$model->uri}/details?{$model->getIdName()}={$model->getIdValue($row)}" class="btn btn-default btn-primary">
	  						<i class="fa fa-list"></i>
	  					</a>
	  				{/if}
	  				{if $model->canEdit()}
	  					<a href="{$model->uri}/edit?{$model->getIdName()}={$model->getIdValue($row)}" class="btn btn-default btn-success">
	  						<i class="fa fa-pencil"></i>
	  					</a>
	  				{/if}
	  				{if $model->canDelete()}
	  					<a href="{$model->uri}/delete?{$model->getIdName()}={$model->getIdValue($row)}" class="btn btn-default btn-danger">
	  						<i class="fa fa-remove"></i>
	  					</a>
	  				{/if}
	  			</div></td>
	  		{/if}
			</tr>
			{/foreach}
	  </table>
