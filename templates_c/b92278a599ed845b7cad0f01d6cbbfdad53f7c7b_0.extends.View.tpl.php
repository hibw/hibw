<?php /* Smarty version 3.1.27, created on 2015-10-07 09:06:41
         compiled from "D:\HZ\Blok 1\Deelopdracht03\HIBW\modules\competitie_indeling\View.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:313875614c4810aa6a7_85634323%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '74c66024dae77e23284ed2b9ca771be195825941' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\modules\\competitie_indeling\\View.tpl',
      1 => 1442583686,
      2 => 'file',
    ),
    '3366103d9c3c5b5885bc0cc4670b4929f8eda843' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\templates\\site.tpl',
      1 => 1444201529,
      2 => 'file',
    ),
    '35afe861e540bb97b48942d083eda6e7119098c3' => 
    array (
      0 => '35afe861e540bb97b48942d083eda6e7119098c3',
      1 => 0,
      2 => 'string',
    ),
    '73e3973365da7c2644fc067c67156585eb3b2e4e' => 
    array (
      0 => '73e3973365da7c2644fc067c67156585eb3b2e4e',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '313875614c4810aa6a7_85634323',
  'variables' => 
  array (
    'model' => 0,
    'menu_item' => 0,
    'child_item' => 0,
    'msg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5614c4811f0d36_89642827',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5614c4811f0d36_89642827')) {
function content_5614c4811f0d36_89642827 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '313875614c4810aa6a7_85634323';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  
  <title><?php
$_smarty_tpl->properties['nocache_hash'] = '313875614c4810aa6a7_85634323';
?>
Ledenbeheer</title>
  <meta name="description" content="">
  <meta name="author" content="waar0003">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/apple-touch-icon.png">
  
	<!-- Bootstrap -->
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
</head>

<body style="padding-top: 70px;">
	<div class="container">
		<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
">DBSTest</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      	<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->menu;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['menu_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['menu_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['menu_item']->value) {
$_smarty_tpl->tpl_vars['menu_item']->_loop = true;
$foreach_menu_item_Sav = $_smarty_tpl->tpl_vars['menu_item'];
?>
		      		<li <?php if ($_smarty_tpl->tpl_vars['model']->value->isCurrentModule($_smarty_tpl->tpl_vars['menu_item']->value->uri)) {?>class="active"<?php }?>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown"<?php }?> >
		      			<a href="<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->uri;?>
" <?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"<?php }?>>
		      				<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->readablename;?>

		      			</a>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>
		      				<ul class="dropdown-menu">
		      					<?php
$_from = $_smarty_tpl->tpl_vars['menu_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['child_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['child_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['child_item']->value) {
$_smarty_tpl->tpl_vars['child_item']->_loop = true;
$foreach_child_item_Sav = $_smarty_tpl->tpl_vars['child_item'];
?>
		      						<li><a href="<?php echo $_smarty_tpl->tpl_vars['child_item']->value->uri;?>
"><?php echo $_smarty_tpl->tpl_vars['child_item']->value->readablename;?>
</a></li>
		      					<?php
$_smarty_tpl->tpl_vars['child_item'] = $foreach_child_item_Sav;
}
?>
		      				</ul>
		      			<?php }?>
		      		</li>
		      	<?php
$_smarty_tpl->tpl_vars['menu_item'] = $foreach_menu_item_Sav;
}
?>
		      </ul>
				<ul class="nav navbar-nav navbar-right">
				    <?php if ($_smarty_tpl->tpl_vars['model']->value->hasAuthenticatedUser()) {?>
					    <li class="dropdown">
					        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['model']->value->current_user->getFullname();?>
 <span class="caret"></span></a>
					        <ul class="dropdown-menu" role="menu">
						        <li><a id="profile" href="#">Profiel</a></li>
						        <li class="divider"></li>
						        <li><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
?action=logoff">Afmelden</a></li>
					        </ul>
				    <?php } else { ?>
						 <!-- <li><a id="login" href="#">Log in</a></li> -->
				          <li class="dropdown" id="menuLogin">
				            <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
				            <div class="dropdown-menu" style="width:300px; padding:17px;">
				              <form class="form" id="formLogin" role="form" action="?action=login"  method="post"> 
										 <label>Login</label>
										 <input  type="text" class="form-control" name="userid" placeholder="Gebruikersnaam"/>
										 <input  type="password" class="form-control" name="passwd" placeholder="Wachtwoord"/>
										 <br/>
				        		<button type="submit" class="btn btn-primary">Inloggen</button>
				              </form>
				            </div>
				          </li>

				    <?php }?>
				    </li>
				</ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasErrors()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->errors;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-exclamation-sign"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasSuccess()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->success;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-saved"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		
		<?php
$_smarty_tpl->properties['nocache_hash'] = '313875614c4810aa6a7_85634323';
?>

		<main class="container">	
			<div class="well">
				<h1>Indeling Competitie</h1>
				<p>Bekijk hier welke teams er bij elkaar in een klasse zitten. Klik op een teamnaam om meer te weten te komen van dat team.</p>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['model']->value->hasItems()) {?>
				<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->items;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2><i class="fa fa-cubes"></i> <?php echo $_smarty_tpl->tpl_vars['item']->value->getNaam();?>
</h2>
							</div>
							<table class="table table-striped">
								<?php if ($_smarty_tpl->tpl_vars['model']->value->KlasHasTeams($_smarty_tpl->tpl_vars['item']->value)) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->getTeamsForKlas($_smarty_tpl->tpl_vars['item']->value);
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['team'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['team']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['team']->value) {
$_smarty_tpl->tpl_vars['team']->_loop = true;
$foreach_team_Sav = $_smarty_tpl->tpl_vars['team'];
?>
									<tr>
										<td><i class="fa fa-users"></i></td>
										<td><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->uri;?>
/team?id=<?php echo $_smarty_tpl->tpl_vars['team']->value->getId();?>
"><?php echo $_smarty_tpl->tpl_vars['team']->value->getNaam();?>
</a></td>
									</tr>
									<?php
$_smarty_tpl->tpl_vars['team'] = $foreach_team_Sav;
}
?>
								<?php } else { ?>
									<div class="alert alert-warning" role="alert">
										<i class="fa fa-exclamation-triangle"></i> Er zijn geen teams in deze klasse
									</div>
								<?php }?>
							</table>
							</div>
				<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
			<?php } else { ?>
				<div class="alert alert-warning" role="alert">'.
						<i class="fa fa-exclamation-triangle"></i> Er zijn geen klassen in deze competitie
				</div>
			<?php }?>
		</main>

		
		
		<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="loginform" id="detailsform" role="form" action="login.php"  method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Sluiten</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Inloggen</h4>
						</div>
		  				<div class="modal-body">
							<div class="form-group">
								 <label class="control-label" for="userid">Gebruikersnaam</label>
								 <input  type="text" class="form-control" name="userid"/>
							</div>		 
							<div class="form-group">
								 <label class="control-label" for="passwd">Wachtwoord</label>
								 <input  type="password" class="form-control" name="passwd"/>
							</div>
		  				</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			        		<button type="submit" class="btn btn-primary" id="btnSubmitDetails">Inloggen</button>
			      		</div>
			      	</form>
		      	</div>
			</div>
		</div>

		
	</div>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	
</body>
</html>
<?php }
}
?>