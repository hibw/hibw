<?php /* Smarty version 3.1.27, created on 2015-10-07 11:36:26
         compiled from "C:\Users\User\Sites\hibw\modules\competitie_indeling\team\View.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:269565614e79ad20b85_88071326%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'afde44b763547af39db85f2bd3438043099db82d' => 
    array (
      0 => 'C:\\Users\\User\\Sites\\hibw\\modules\\competitie_indeling\\team\\View.tpl',
      1 => 1443428548,
      2 => 'file',
    ),
    'c600d2e1304bf1cb882dbc72215fd36af89eacb1' => 
    array (
      0 => 'C:\\Users\\User\\Sites\\hibw\\templates\\site.tpl',
      1 => 1444203931,
      2 => 'file',
    ),
    'ac9133c76cc42f100f796906601b423328e0fc1e' => 
    array (
      0 => 'ac9133c76cc42f100f796906601b423328e0fc1e',
      1 => 0,
      2 => 'string',
    ),
    '8dfe6a3e46731c61956925d6b12d1b7943be1d5d' => 
    array (
      0 => '8dfe6a3e46731c61956925d6b12d1b7943be1d5d',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '269565614e79ad20b85_88071326',
  'variables' => 
  array (
    'model' => 0,
    'menu_item' => 0,
    'child_item' => 0,
    'msg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5614e79ae18f85_58067788',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5614e79ae18f85_58067788')) {
function content_5614e79ae18f85_58067788 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '269565614e79ad20b85_88071326';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  
  <title><?php
$_smarty_tpl->properties['nocache_hash'] = '269565614e79ad20b85_88071326';
?>
Ledenbeheer</title>
  <meta name="description" content="">
  <meta name="author" content="waar0003">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/apple-touch-icon.png">
  
	<!-- Bootstrap -->
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
</head>

<body style="padding-top: 70px;">
	<div class="container">
		<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
">DBSTest</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      	<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->menu;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['menu_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['menu_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['menu_item']->value) {
$_smarty_tpl->tpl_vars['menu_item']->_loop = true;
$foreach_menu_item_Sav = $_smarty_tpl->tpl_vars['menu_item'];
?>
		      		<li <?php if ($_smarty_tpl->tpl_vars['model']->value->isCurrentModule($_smarty_tpl->tpl_vars['menu_item']->value->uri)) {?>class="active"<?php }?>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown"<?php }?> >
		      			<a href="<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->uri;?>
" <?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"<?php }?>>
		      				<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->readablename;?>

		      			</a>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>
		      				<ul class="dropdown-menu">
		      					<?php
$_from = $_smarty_tpl->tpl_vars['menu_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['child_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['child_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['child_item']->value) {
$_smarty_tpl->tpl_vars['child_item']->_loop = true;
$foreach_child_item_Sav = $_smarty_tpl->tpl_vars['child_item'];
?>
		      						<li><a href="<?php echo $_smarty_tpl->tpl_vars['child_item']->value->uri;?>
"><?php echo $_smarty_tpl->tpl_vars['child_item']->value->readablename;?>
</a></li>
		      					<?php
$_smarty_tpl->tpl_vars['child_item'] = $foreach_child_item_Sav;
}
?>
		      				</ul>
		      			<?php }?>
		      		</li>
		      	<?php
$_smarty_tpl->tpl_vars['menu_item'] = $foreach_menu_item_Sav;
}
?>
		      </ul>
				<ul class="nav navbar-nav navbar-right">
				    <?php if ($_smarty_tpl->tpl_vars['model']->value->hasAuthenticatedUser()) {?>
					    <li class="dropdown">
					        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['model']->value->current_user->getFullname();?>
 <span class="caret"></span></a>
					        <ul class="dropdown-menu" role="menu">
						        <li><a id="profile" href="#">Profiel</a></li>
						        <li class="divider"></li>
						        <li><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
?action=logoff">Afmelden</a></li>
					        </ul>
				    <?php } else { ?>
						 <!-- <li><a id="login" href="#">Log in</a></li> -->
				          <li class="dropdown" id="menuLogin">
				            <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
				            <div class="dropdown-menu" style="width:300px; padding:17px;">
				              <form class="form" id="formLogin" role="form" action="?action=login"  method="post"> 
										 <label>Login</label>
										 <input  type="text" class="form-control" name="userid" placeholder="Gebruikersnaam"/>
										 <input  type="password" class="form-control" name="passwd" placeholder="Wachtwoord"/>
										 <br/>
				        		<button type="submit" class="btn btn-primary">Inloggen</button>
				              </form>
				            </div>
				          </li>

				    <?php }?>
				    </li>
				</ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasErrors()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->errors;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-exclamation-sign"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasSuccess()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->success;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-saved"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		
		<?php
$_smarty_tpl->properties['nocache_hash'] = '269565614e79ad20b85_88071326';
?>

		<main class="container">
		<div class="well"><h1>Team <?php echo $_smarty_tpl->tpl_vars['model']->value->item->getId();?>
: <?php echo $_smarty_tpl->tpl_vars['model']->value->item->getNaam();?>
</h1></div>
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#spelers" aria-controls="spelers" role="tab" data-toggle="tab">De spelers</a></li>
				<li role="presentation"><a href="#wedstrijden" aria-controls="wedstrijden" role="tab" data-toggle="tab">Wedstrijden</a></li>
				<li role="presentation"><a href="#uitslagen" aria-controls="uitslagen" role="tab" data-toggle="tab">Uitslagen</a></li>
				<li role="presentation"><a href="#statistieken" aria-controls="statistieken" role="tab" data-toggle="tab">Statistieken</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="spelers">
					<?php if ($_smarty_tpl->tpl_vars['model']->value->item->countLids() > 0) {?>
						<table class="table table-striped">
							<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->item->getLids();
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['lid'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['lid']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['lid']->value) {
$_smarty_tpl->tpl_vars['lid']->_loop = true;
$foreach_lid_Sav = $_smarty_tpl->tpl_vars['lid'];
?>
								<tr>
									<td class="col-sm-1">
										<i class="fa fa-user fa-3x"></i>
									</td>
									<td class="col-sm-11">
										<strong><?php echo $_smarty_tpl->tpl_vars['lid']->value->getNaam();?>
</strong><br/>
										<small>Al 3 sezoenen actief</small>
									</td>
								</tr>
							<?php
$_smarty_tpl->tpl_vars['lid'] = $foreach_lid_Sav;
}
?> 
						</table>
					<?php } else { ?>
						<div class="alert alert-warning" role="alert">
							<i class="fa fa-exclamation-triangle"></i> Er zijn geen spelers in dit team
						</div>
					<?php }?>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="wedstrijden">
					<?php if ($_smarty_tpl->tpl_vars['model']->value->item->countTeamWedstrijds() > 0) {?>
					  <table class="table table-striped">
							<tr>
								<th>Datum</th>
								<th>Tijd</th>
								<th>Veld</th>
								<th>Tegen</th>
							</tr>
							<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->item->getTeamWedstrijds();
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['w'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['w']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['w']->value) {
$_smarty_tpl->tpl_vars['w']->_loop = true;
$foreach_w_Sav = $_smarty_tpl->tpl_vars['w'];
?>
								<tr>
									<td class="col-sm-2">
										<?php echo $_smarty_tpl->tpl_vars['w']->value->getDatum()->format('j F Y');?>
<br/>
									</td>
									<td class="col-sm-1">
										<?php echo $_smarty_tpl->tpl_vars['w']->value->getTijd()->format('G:i');?>
<br/>
									</td>
									<td class="col-sm-1">
										<?php echo $_smarty_tpl->tpl_vars['w']->value->getVeld();?>
<br/>
									</td>
									<td class="col-sm-10">
										<strong><?php echo $_smarty_tpl->tpl_vars['w']->value->getNaam();?>
</strong><br/>
									</td>
								</tr>
							<?php
$_smarty_tpl->tpl_vars['w'] = $foreach_w_Sav;
}
?> 
						</table>
					<?php } else { ?>
						<div class="alert alert-warning" role="alert">
							<i class="fa fa-exclamation-triangle"></i> Er zijn geen wedstrijden voor dit team
						</div>
					<?php }?>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="uitslagen">
					Hier komen de uitslagen
				</div>
				<div role="tabpanel" class="tab-pane fade" id="statistieken">
					<table class="table table-striped">
							<tr>
								<td>
									<strong>W</strong><br/>
									<small><i>Aantal gespeelde wedstrijden</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getW();?>
</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>P</strong><br/>
									<small><i>Competitiepunten (gewonnen sets min strafpunten)</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getP();?>
</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Sv</strong><br/>
									<small><i>Scores voor</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getSv();?>
</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>St</strong><br/>
									<small><i>Scores tegen</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getSt();?>
</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>S</strong><br/>
									<small><i>Score saldo (Sv - St)</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getS();?>
</strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Str.P</strong><br/>
									<small><i>Strafpunten</i></small>
								</td>
								<td>
									<strong><?php echo $_smarty_tpl->tpl_vars['model']->value->item->getStatistics()->getStrp();?>
</strong><br/>
								</td>
							</tr>
						</table>

				</div>
			</div>

		</div>

		</main>

		
		
		<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="loginform" id="detailsform" role="form" action="login.php"  method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Sluiten</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Inloggen</h4>
						</div>
		  				<div class="modal-body">
							<div class="form-group">
								 <label class="control-label" for="userid">Gebruikersnaam</label>
								 <input  type="text" class="form-control" name="userid"/>
							</div>		 
							<div class="form-group">
								 <label class="control-label" for="passwd">Wachtwoord</label>
								 <input  type="password" class="form-control" name="passwd"/>
							</div>
		  				</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			        		<button type="submit" class="btn btn-primary" id="btnSubmitDetails">Inloggen</button>
			      		</div>
			      	</form>
		      	</div>
			</div>
		</div>

		
	</div>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	
</body>
</html>
<?php }
}
?>