<?php /* Smarty version 3.1.27, created on 2015-10-07 12:36:36
         compiled from "D:\HZ\Blok 1\Deelopdracht03\HIBW\modules\users\edit\View.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:45665614f5b46fcd68_22015714%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1858697a7300343d58d3a259384d5fb348b0c8bb' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\modules\\users\\edit\\View.tpl',
      1 => 1444214191,
      2 => 'file',
    ),
    '3366103d9c3c5b5885bc0cc4670b4929f8eda843' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\templates\\site.tpl',
      1 => 1444201529,
      2 => 'file',
    ),
    'ad0649c7bc66b8a7688243d981ced7fbad9ae72d' => 
    array (
      0 => 'ad0649c7bc66b8a7688243d981ced7fbad9ae72d',
      1 => 0,
      2 => 'string',
    ),
    'd9e40db4b3613bf172c534bc452499ec666db382' => 
    array (
      0 => 'd9e40db4b3613bf172c534bc452499ec666db382',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '45665614f5b46fcd68_22015714',
  'variables' => 
  array (
    'model' => 0,
    'menu_item' => 0,
    'child_item' => 0,
    'msg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5614f5b47bdf79_78841066',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5614f5b47bdf79_78841066')) {
function content_5614f5b47bdf79_78841066 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '45665614f5b46fcd68_22015714';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  
  <title><?php
$_smarty_tpl->properties['nocache_hash'] = '45665614f5b46fcd68_22015714';
?>
Gebruikersbeheer</title>
  <meta name="description" content="">
  <meta name="author" content="waar0003">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/apple-touch-icon.png">
  
	<!-- Bootstrap -->
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
</head>

<body style="padding-top: 70px;">
	<div class="container">
		<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
">DBSTest</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      	<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->menu;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['menu_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['menu_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['menu_item']->value) {
$_smarty_tpl->tpl_vars['menu_item']->_loop = true;
$foreach_menu_item_Sav = $_smarty_tpl->tpl_vars['menu_item'];
?>
		      		<li <?php if ($_smarty_tpl->tpl_vars['model']->value->isCurrentModule($_smarty_tpl->tpl_vars['menu_item']->value->uri)) {?>class="active"<?php }?>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown"<?php }?> >
		      			<a href="<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->uri;?>
" <?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"<?php }?>>
		      				<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->readablename;?>

		      			</a>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>
		      				<ul class="dropdown-menu">
		      					<?php
$_from = $_smarty_tpl->tpl_vars['menu_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['child_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['child_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['child_item']->value) {
$_smarty_tpl->tpl_vars['child_item']->_loop = true;
$foreach_child_item_Sav = $_smarty_tpl->tpl_vars['child_item'];
?>
		      						<li><a href="<?php echo $_smarty_tpl->tpl_vars['child_item']->value->uri;?>
"><?php echo $_smarty_tpl->tpl_vars['child_item']->value->readablename;?>
</a></li>
		      					<?php
$_smarty_tpl->tpl_vars['child_item'] = $foreach_child_item_Sav;
}
?>
		      				</ul>
		      			<?php }?>
		      		</li>
		      	<?php
$_smarty_tpl->tpl_vars['menu_item'] = $foreach_menu_item_Sav;
}
?>
		      </ul>
				<ul class="nav navbar-nav navbar-right">
				    <?php if ($_smarty_tpl->tpl_vars['model']->value->hasAuthenticatedUser()) {?>
					    <li class="dropdown">
					        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['model']->value->current_user->getFullname();?>
 <span class="caret"></span></a>
					        <ul class="dropdown-menu" role="menu">
						        <li><a id="profile" href="#">Profiel</a></li>
						        <li class="divider"></li>
						        <li><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
?action=logoff">Afmelden</a></li>
					        </ul>
				    <?php } else { ?>
						 <!-- <li><a id="login" href="#">Log in</a></li> -->
				          <li class="dropdown" id="menuLogin">
				            <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
				            <div class="dropdown-menu" style="width:300px; padding:17px;">
				              <form class="form" id="formLogin" role="form" action="?action=login"  method="post"> 
										 <label>Login</label>
										 <input  type="text" class="form-control" name="userid" placeholder="Gebruikersnaam"/>
										 <input  type="password" class="form-control" name="passwd" placeholder="Wachtwoord"/>
										 <br/>
				        		<button type="submit" class="btn btn-primary">Inloggen</button>
				              </form>
				            </div>
				          </li>

				    <?php }?>
				    </li>
				</ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasErrors()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->errors;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-exclamation-sign"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasSuccess()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->success;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-saved"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		
		<?php
$_smarty_tpl->properties['nocache_hash'] = '45665614f5b46fcd68_22015714';
?>

<h3>Gebruikersdetails 
<a type="button" class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/users">
	  			Terug
	  		</a>
	  	</h3>
<form method="post">
<input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['model']->value->state;?>
"/>
<div class="form-group">
	 <label class="control-label" for="userid">Gebruikersnaam *</label>
	 <input  type="text" class="form-control" name="userid" 
	 		<?php if ($_smarty_tpl->tpl_vars['model']->value->state == 'update') {?>
	 			readonly
	 		<?php } else { ?>
	 			required minlength="2" 
	 		<?php }?>
	 		value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getUserid();?>
"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameFirst">Voornaam *</label>
	 <input  type="text" class="form-control" name="nameFirst" 
	 		required value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getNameFirst();?>
"/>
</div>		 
<div class="form-group">
	 <label class="control-label" for="nameLast">Achternaam *</label>
	 <input  type="text" class="form-control" name="nameLast" 
	 		required value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getNameLast();?>
"/>
</div>
<div class="form-group">
	 <label class="control-label" for="email">Email Adres *</label>
	 <input  type="text" class="form-control" name="email" 
	 		required value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getEmail();?>
"/>		 
</div>
<div class="form-group">
	 <label class="control-label" for="telephone">Telefoon Nummer</label>
	 <input  type="text" class="form-control" name="telephone" 
	 		value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getTelephone();?>
"/>
</div>
<div class="form-group">
	<?php if ($_smarty_tpl->tpl_vars['model']->value->state == 'update') {?>
		<label class="control-label" for="password">Wachtwoord</label>
		 <input  type="password" class="form-control" name="password" />
		 <label class="control-label" for="password_retype">Bevestig wachtwoord</label>
		 <input  type="password" class="form-control" name="password_retype" />
	<?php } else { ?>
		 <label class="control-label" for="password">Wachtwoord *</label>
		 <input  type="password" class="form-control" name="password" 
				required value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getPassword();?>
"/>
		 <label class="control-label" for="password_retype">Bevestig wachtwoord *</label>
		 <input  type="password" class="form-control" name="password_retype" 
				required value="<?php echo $_smarty_tpl->tpl_vars['model']->value->item->getPassword();?>
"/>
	<?php }?>
</div>
<div>
	<label class="control-label" for="groupid">Gebruikersgroep *</label>
	<select class="form-control" name="groupid" required <?php if ($_smarty_tpl->tpl_vars['model']->value->item->getUserid() == $_smarty_tpl->tpl_vars['model']->value->returnCurrentUserId()) {?>
				 disabled 
			<?php }?>>
		<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->getAllUserGroups();
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['userGroup'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['userGroup']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['userGroup']->value) {
$_smarty_tpl->tpl_vars['userGroup']->_loop = true;
$foreach_userGroup_Sav = $_smarty_tpl->tpl_vars['userGroup'];
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['userGroup']->value->getGroupid();?>
" <?php if ($_smarty_tpl->tpl_vars['userGroup']->value->getGroupid() == $_smarty_tpl->tpl_vars['model']->value->item->getGroupid() && $_smarty_tpl->tpl_vars['model']->value->state == 'update') {?>
				 selected 
			<?php } elseif ($_smarty_tpl->tpl_vars['model']->value->state != 'update' && $_smarty_tpl->tpl_vars['userGroup']->value->getGroupid() == 2) {?>
				 selected 
			<?php }?>
			><?php echo ucfirst($_smarty_tpl->tpl_vars['userGroup']->value->getGroupName());?>
</option>	
		<?php
$_smarty_tpl->tpl_vars['userGroup'] = $foreach_userGroup_Sav;
}
?>
	</select>
</div>
<br/><input type="submit" class="btn btn-primary" <?php if ($_smarty_tpl->tpl_vars['model']->value->returnCurrentUserGroupId() < 5) {?>
	 disabled 
<?php }?>></input>
</form>

		
		
		<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="loginform" id="detailsform" role="form" action="login.php"  method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Sluiten</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Inloggen</h4>
						</div>
		  				<div class="modal-body">
							<div class="form-group">
								 <label class="control-label" for="userid">Gebruikersnaam</label>
								 <input  type="text" class="form-control" name="userid"/>
							</div>		 
							<div class="form-group">
								 <label class="control-label" for="passwd">Wachtwoord</label>
								 <input  type="password" class="form-control" name="passwd"/>
							</div>
		  				</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			        		<button type="submit" class="btn btn-primary" id="btnSubmitDetails">Inloggen</button>
			      		</div>
			      	</form>
		      	</div>
			</div>
		</div>

		
	</div>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	
</body>
</html>
<?php }
}
?>