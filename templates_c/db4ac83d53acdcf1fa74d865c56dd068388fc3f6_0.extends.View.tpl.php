<?php /* Smarty version 3.1.27, created on 2015-10-07 09:07:34
         compiled from "D:\HZ\Blok 1\Deelopdracht03\HIBW\modules\users\View.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:26375614c4b635df13_42299356%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8d151bca432e4cb5cc5d380dc592813ffe703fa' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\modules\\users\\View.tpl',
      1 => 1442584372,
      2 => 'file',
    ),
    '3366103d9c3c5b5885bc0cc4670b4929f8eda843' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\templates\\site.tpl',
      1 => 1444201529,
      2 => 'file',
    ),
    '86ecbdafed93bdf80b8715372b4793c733a79921' => 
    array (
      0 => '86ecbdafed93bdf80b8715372b4793c733a79921',
      1 => 0,
      2 => 'string',
    ),
    '1d27adb1ba1a12fad45c19ca67fa375c35516a26' => 
    array (
      0 => '1d27adb1ba1a12fad45c19ca67fa375c35516a26',
      1 => 0,
      2 => 'string',
    ),
    '8cd210ab072b1205eeb558abb138b9aa451050a5' => 
    array (
      0 => 'D:\\HZ\\Blok 1\\Deelopdracht03\\HIBW\\templates\\tableview.tpl',
      1 => 1444201529,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26375614c4b635df13_42299356',
  'variables' => 
  array (
    'model' => 0,
    'menu_item' => 0,
    'child_item' => 0,
    'msg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5614c4b6417237_19647226',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5614c4b6417237_19647226')) {
function content_5614c4b6417237_19647226 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '26375614c4b635df13_42299356';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  
  <title><?php
$_smarty_tpl->properties['nocache_hash'] = '26375614c4b635df13_42299356';
?>
Gebruikersbeheer</title>
  <meta name="description" content="">
  <meta name="author" content="waar0003">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/apple-touch-icon.png">
  
	<!-- Bootstrap -->
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
</head>

<body style="padding-top: 70px;">
	<div class="container">
		<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
">DBSTest</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      	<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->menu;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['menu_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['menu_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['menu_item']->value) {
$_smarty_tpl->tpl_vars['menu_item']->_loop = true;
$foreach_menu_item_Sav = $_smarty_tpl->tpl_vars['menu_item'];
?>
		      		<li <?php if ($_smarty_tpl->tpl_vars['model']->value->isCurrentModule($_smarty_tpl->tpl_vars['menu_item']->value->uri)) {?>class="active"<?php }?>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown"<?php }?> >
		      			<a href="<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->uri;?>
" <?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"<?php }?>>
		      				<?php echo $_smarty_tpl->tpl_vars['menu_item']->value->readablename;?>

		      			</a>
		      			<?php if ($_smarty_tpl->tpl_vars['menu_item']->value->hasChildren()) {?>
		      				<ul class="dropdown-menu">
		      					<?php
$_from = $_smarty_tpl->tpl_vars['menu_item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['child_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['child_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['child_item']->value) {
$_smarty_tpl->tpl_vars['child_item']->_loop = true;
$foreach_child_item_Sav = $_smarty_tpl->tpl_vars['child_item'];
?>
		      						<li><a href="<?php echo $_smarty_tpl->tpl_vars['child_item']->value->uri;?>
"><?php echo $_smarty_tpl->tpl_vars['child_item']->value->readablename;?>
</a></li>
		      					<?php
$_smarty_tpl->tpl_vars['child_item'] = $foreach_child_item_Sav;
}
?>
		      				</ul>
		      			<?php }?>
		      		</li>
		      	<?php
$_smarty_tpl->tpl_vars['menu_item'] = $foreach_menu_item_Sav;
}
?>
		      </ul>
				<ul class="nav navbar-nav navbar-right">
				    <?php if ($_smarty_tpl->tpl_vars['model']->value->hasAuthenticatedUser()) {?>
					    <li class="dropdown">
					        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['model']->value->current_user->getFullname();?>
 <span class="caret"></span></a>
					        <ul class="dropdown-menu" role="menu">
						        <li><a id="profile" href="#">Profiel</a></li>
						        <li class="divider"></li>
						        <li><a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
?action=logoff">Afmelden</a></li>
					        </ul>
				    <?php } else { ?>
						 <!-- <li><a id="login" href="#">Log in</a></li> -->
				          <li class="dropdown" id="menuLogin">
				            <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
				            <div class="dropdown-menu" style="width:300px; padding:17px;">
				              <form class="form" id="formLogin" role="form" action="?action=login"  method="post"> 
										 <label>Login</label>
										 <input  type="text" class="form-control" name="userid" placeholder="Gebruikersnaam"/>
										 <input  type="password" class="form-control" name="passwd" placeholder="Wachtwoord"/>
										 <br/>
				        		<button type="submit" class="btn btn-primary">Inloggen</button>
				              </form>
				            </div>
				          </li>

				    <?php }?>
				    </li>
				</ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasErrors()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->errors;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-exclamation-sign"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasSuccess()) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->success;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['msg']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
$foreach_msg_Sav = $_smarty_tpl->tpl_vars['msg'];
?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Sluiten</span>
					</button>
					<span class="glyphicon glyphicon-saved"></span> <?php echo $_smarty_tpl->tpl_vars['msg']->value;?>

				</div>
			<?php
$_smarty_tpl->tpl_vars['msg'] = $foreach_msg_Sav;
}
?>
		<?php }?>
		
		<?php
$_smarty_tpl->properties['nocache_hash'] = '26375614c4b635df13_42299356';
?>

	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<h3>
	  		Gebruikers 
	  		<a type="button" class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['model']->value->uri;?>
/edit">
	  			Nieuwe gebruiker
	  		</a>
	  	</h3>
	  </div>
	  <div class="panel-body">
	  </div>
	  <?php /*  Call merged included template "tableview.tpl" */
echo $_smarty_tpl->getInlineSubTemplate("tableview.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, '323295614c4b63e1917_33138830', 'content_5614c4b63e0600_25685485');
/*  End of included template "tableview.tpl" */?>

	</div>

		
		
		<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="loginform" id="detailsform" role="form" action="login.php"  method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Sluiten</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Inloggen</h4>
						</div>
		  				<div class="modal-body">
							<div class="form-group">
								 <label class="control-label" for="userid">Gebruikersnaam</label>
								 <input  type="text" class="form-control" name="userid"/>
							</div>		 
							<div class="form-group">
								 <label class="control-label" for="passwd">Wachtwoord</label>
								 <input  type="password" class="form-control" name="passwd"/>
							</div>
		  				</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			        		<button type="submit" class="btn btn-primary" id="btnSubmitDetails">Inloggen</button>
			      		</div>
			      	</form>
		      	</div>
			</div>
		</div>

		
	</div>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['model']->value->home;?>
/lib/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	
</body>
</html>
<?php }
}
?><?php
/*%%SmartyHeaderCode:323295614c4b63e1917_33138830%%*/
if ($_valid && !is_callable('content_5614c4b63e0600_25685485')) {
function content_5614c4b63e0600_25685485 ($_smarty_tpl) {
?>
<?php
$_smarty_tpl->properties['nocache_hash'] = '323295614c4b63e1917_33138830';
?>
	  <table class="table table-striped table-hover">
	  	<tr>
	  		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasBulkActions()) {?><th/><?php }?>
	  		<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->columns;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['col'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['col']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['col']->value) {
$_smarty_tpl->tpl_vars['col']->_loop = true;
$foreach_col_Sav = $_smarty_tpl->tpl_vars['col'];
?>
	  			<th><?php echo $_smarty_tpl->tpl_vars['col']->value->headername;?>
</th>
	  		<?php
$_smarty_tpl->tpl_vars['col'] = $foreach_col_Sav;
}
?>
	  		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasButtons()) {?><th/><?php }?>

	  	</tr>
			<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->items;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
			<tr>
				
				<?php if ($_smarty_tpl->tpl_vars['model']->value->hasBulkActions()) {?>
					<td><input type="checkbox" class="cbDelete" id="<?php echo $_smarty_tpl->tpl_vars['item']->value->getUserid();?>
"></td>
				<?php }?>

				
	  		<?php
$_from = $_smarty_tpl->tpl_vars['model']->value->columns;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['col'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['col']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['col']->value) {
$_smarty_tpl->tpl_vars['col']->_loop = true;
$foreach_col_Sav = $_smarty_tpl->tpl_vars['col'];
?>
	  			<td>
	  				<?php echo $_smarty_tpl->tpl_vars['model']->value->renderCell($_smarty_tpl->tpl_vars['col']->value,$_smarty_tpl->tpl_vars['row']->value);?>

	  			</td>
	  		<?php
$_smarty_tpl->tpl_vars['col'] = $foreach_col_Sav;
}
?>

	  		
	  		<?php if ($_smarty_tpl->tpl_vars['model']->value->hasButtons()) {?>
	  			<td>
	  			<div class="btn-group" role="group" aria-label="edit buttons">
	  				<?php if ($_smarty_tpl->tpl_vars['model']->value->hasDetails()) {?>
	  					<a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->uri;?>
/details?<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdName();?>
=<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdValue($_smarty_tpl->tpl_vars['row']->value);?>
" class="btn btn-default btn-primary">
	  						<i class="fa fa-list"></i>
	  					</a>
	  				<?php }?>
	  				<?php if ($_smarty_tpl->tpl_vars['model']->value->canEdit()) {?>
	  					<a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->uri;?>
/edit?<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdName();?>
=<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdValue($_smarty_tpl->tpl_vars['row']->value);?>
" class="btn btn-default btn-success">
	  						<i class="fa fa-pencil"></i>
	  					</a>
	  				<?php }?>
	  				<?php if ($_smarty_tpl->tpl_vars['model']->value->canDelete()) {?>
	  					<a href="<?php echo $_smarty_tpl->tpl_vars['model']->value->uri;?>
/delete?<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdName();?>
=<?php echo $_smarty_tpl->tpl_vars['model']->value->getIdValue($_smarty_tpl->tpl_vars['row']->value);?>
" class="btn btn-default btn-danger">
	  						<i class="fa fa-remove"></i>
	  					</a>
	  				<?php }?>
	  			</div></td>
	  		<?php }?>
			</tr>
			<?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
	  </table>
<?php
/*/%%SmartyNocache:323295614c4b63e1917_33138830%%*/
}
}
?>