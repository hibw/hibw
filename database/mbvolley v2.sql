-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 01 okt 2015 om 15:11
-- Serverversie: 5.6.26
-- PHP-versie: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mbvolley`
--
DROP DATABASE `mbvolley`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `klas`
--

CREATE TABLE IF NOT EXISTS `klas` (
  `code` char(2) NOT NULL,
  `naam` varchar(50) NOT NULL DEFAULT '<naam>'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `klas`
--

INSERT INTO `klas` (`code`, `naam`) VALUES
('D1', 'Dames 1'),
('D2', 'Dames 2'),
('H1', 'Heren 1'),
('H2', 'Heren 2'),
('H3', 'Heren 3');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `lid`
--

CREATE TABLE IF NOT EXISTS `lid` (
  `id` int(11) NOT NULL,
  `naam` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `lid`
--

INSERT INTO `lid` (`id`, `naam`) VALUES
(1, 'Crista Aherm'),
(2, 'Lina Petrin'),
(3, 'Larae Smallwood 234'),
(4, 'Jackson Steger'),
(5, 'Darcel Mccarville'),
(6, 'Jenny Hyler'),
(7, 'Mable Harrod'),
(8, 'Aleida Buff'),
(9, 'Bailey Eisenhower'),
(10, 'Mose Campion'),
(11, 'Shae Pfannenstiel'),
(12, 'Reid Bugbee'),
(13, 'Emmitt Frye'),
(14, 'Danielle Digennaro'),
(15, 'Gertrud Schalk'),
(16, 'Ha Halpern'),
(17, 'Marica Bess'),
(18, 'Manuel Reader'),
(19, 'Zella Cloe'),
(20, 'Reinaldo Shuffler'),
(21, 'Jacco de Vissert'),
(22, 'Jesse van Splunder');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `speelweek`
--

CREATE TABLE IF NOT EXISTS `speelweek` (
  `id` int(11) NOT NULL,
  `datum` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `speelweek`
--

INSERT INTO `speelweek` (`id`, `datum`) VALUES
(1, '2014-09-09'),
(2, '2014-09-16'),
(3, '2014-09-23'),
(4, '2014-09-30'),
(5, '2014-10-07'),
(6, '2014-10-14'),
(7, '2014-10-21'),
(8, '2014-10-28'),
(9, '2014-11-04'),
(10, '2014-11-11'),
(11, '2014-11-18'),
(12, '2014-11-25'),
(13, '2014-12-02'),
(14, '2014-12-09'),
(15, '2014-12-16'),
(16, '2015-01-06'),
(17, '2015-01-13'),
(18, '2015-01-20'),
(19, '2015-01-27'),
(20, '2015-02-03'),
(21, '2015-02-10'),
(22, '2015-02-17'),
(23, '2015-02-24'),
(24, '2015-03-03'),
(25, '2015-03-10'),
(26, '2015-03-17'),
(27, '2015-03-24'),
(28, '2015-03-31'),
(29, '2015-04-07'),
(30, '2015-04-14'),
(31, '2015-04-21'),
(32, '2015-04-28'),
(33, '2015-05-05'),
(34, '2015-05-12'),
(35, '2015-05-19'),
(36, '2015-05-26');

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `statsview`
--
CREATE TABLE IF NOT EXISTS `statsview` (
`klassecode` char(2)
,`teamid` int(11)
,`naam` varchar(64)
,`W` bigint(21)
,`P` decimal(33,0)
,`Sv` decimal(32,0)
,`St` decimal(32,0)
,`S` decimal(33,0)
,`strp` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL,
  `klassecode` char(2) NOT NULL,
  `naam` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `team`
--

INSERT INTO `team` (`id`, `klassecode`, `naam`) VALUES
(1, 'H1', 'Munter Volley 1'),
(2, 'H1', 'De Graaf Assurantien'),
(3, 'H1', 'Sport2000 Mdb/''t Znd'),
(4, 'H3', 'SMA Zeeland'),
(5, 'H3', 'Bowling d Kruitmolen'),
(6, 'H2', 'Munter Volley 2'),
(7, 'D1', 'Vlijtig Liesje'),
(8, 'D1', 'Res. Valkenisse'),
(9, 'D1', 'The Tube');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `team_has_lid`
--

CREATE TABLE IF NOT EXISTS `team_has_lid` (
  `teamid` int(11) NOT NULL,
  `lidid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `team_has_lid`
--

INSERT INTO `team_has_lid` (`teamid`, `lidid`) VALUES
(1, 1),
(3, 1),
(6, 1),
(8, 1),
(1, 2),
(3, 2),
(6, 2),
(8, 2),
(1, 3),
(4, 3),
(6, 3),
(9, 3),
(1, 4),
(4, 4),
(6, 4),
(9, 4),
(1, 5),
(4, 5),
(6, 5),
(9, 5),
(1, 6),
(4, 6),
(6, 6),
(9, 6),
(1, 7),
(4, 7),
(7, 7),
(9, 7),
(2, 8),
(4, 8),
(7, 8),
(9, 8),
(2, 9),
(4, 9),
(7, 9),
(9, 9),
(2, 10),
(4, 10),
(7, 10),
(2, 11),
(4, 11),
(7, 11),
(2, 12),
(5, 12),
(7, 12),
(2, 13),
(5, 13),
(8, 13),
(2, 14),
(5, 14),
(8, 14),
(2, 15),
(5, 15),
(8, 15),
(3, 16),
(5, 16),
(8, 16),
(3, 17),
(5, 17),
(8, 17),
(3, 18),
(5, 18),
(8, 18),
(3, 19),
(6, 19),
(8, 19),
(3, 20),
(6, 20),
(8, 20);

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `team_strp`
--
CREATE TABLE IF NOT EXISTS `team_strp` (
`id` int(11)
,`klassecode` char(2)
,`naam` varchar(64)
,`Strp` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `team_uitslag_view`
--
CREATE TABLE IF NOT EXISTS `team_uitslag_view` (
`id` int(11)
,`klassecode` char(2)
,`naam` varchar(64)
,`Strp` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structuur voor view `teamwedstrijdview`
--
CREATE TABLE IF NOT EXISTS `teamwedstrijdview` (
`teamid` int(11)
,`datum` date
,`tijd` time
,`veld` int(11)
,`naam` varchar(64)
);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `uitslag_set_team`
--

CREATE TABLE IF NOT EXISTS `uitslag_set_team` (
  `wedstrijdid` int(11) NOT NULL,
  `set` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `punten` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `uitslag_set_team`
--

INSERT INTO `uitslag_set_team` (`wedstrijdid`, `set`, `teamid`, `score`, `punten`) VALUES
(1, 1, 7, 18, 0),
(1, 1, 8, 25, 2),
(1, 2, 7, 11, 0),
(1, 2, 8, 25, 2),
(1, 3, 7, 23, 0),
(1, 3, 8, 25, 2),
(1, 4, 7, 12, 2),
(1, 4, 8, 4, 0),
(2, 1, 1, 25, 2),
(2, 1, 2, 11, 0),
(2, 2, 1, 25, 2),
(2, 2, 2, 3, 0),
(2, 3, 1, 25, 2),
(2, 3, 2, 23, 0),
(2, 4, 1, 12, 0),
(2, 4, 2, 25, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Userid` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Name_first` varchar(50) NOT NULL,
  `Name_last` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telephone` varchar(50) DEFAULT NULL,
  `groupid` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`Userid`, `Password`, `Name_first`, `Name_last`, `Email`, `Telephone`, `groupid`) VALUES
('admin', 'fi4jJJNfbkvVk', 'Daan', 'de Waard', 'daanwaard@gmail.com', '1232344', 1),
('asd', 'fionu3giiS71.', 'Piet', 'Pietersen', 'sdfsd@sfjdlksdfl.nl', '', 1),
('asd2', 'figsoZwws4Zu6', 'Hansje', 'asdas', 'a@b.nl', '0344232334', 1),
('tester', 'fionu3giiS71.', 'sdlfjsdflk', 'sdlkjlgkjfkl', 'a@b.nl', '0344232334', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_groups`
--

INSERT INTO `user_groups` (`id`, `naam`, `niveau`) VALUES
(1, 'gast', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `w_team`
--

CREATE TABLE IF NOT EXISTS `w_team` (
  `wedstrijdid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `strafpunten` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `w_team`
--

INSERT INTO `w_team` (`wedstrijdid`, `teamid`, `rol`, `strafpunten`) VALUES
(1, 7, 1, 0),
(1, 8, 2, 0),
(1, 9, 3, 0),
(2, 1, 1, 0),
(2, 2, 2, 0),
(2, 3, 3, 0),
(19, 7, 3, 8),
(19, 8, 1, 0),
(19, 9, 2, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wedstrijd`
--

CREATE TABLE IF NOT EXISTS `wedstrijd` (
  `id` int(11) NOT NULL,
  `speelweekid` int(11) NOT NULL,
  `tijd` time NOT NULL,
  `veld` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `wedstrijd`
--

INSERT INTO `wedstrijd` (`id`, `speelweekid`, `tijd`, `veld`) VALUES
(1, 1, '19:15:00', 2),
(2, 1, '19:15:00', 3),
(3, 1, '19:15:00', 4),
(4, 1, '19:15:00', 5),
(5, 1, '19:15:00', 6),
(6, 1, '20:15:00', 1),
(7, 1, '20:15:00', 2),
(8, 1, '20:15:00', 3),
(9, 1, '20:15:00', 4),
(10, 1, '20:15:00', 5),
(11, 1, '20:15:00', 6),
(12, 1, '21:15:00', 1),
(13, 1, '21:15:00', 2),
(14, 1, '21:15:00', 3),
(15, 1, '21:15:00', 4),
(16, 1, '21:15:00', 5),
(17, 2, '21:15:00', 6),
(18, 2, '19:15:00', 1),
(19, 2, '19:15:00', 2),
(20, 2, '19:15:00', 3),
(21, 2, '19:15:00', 4),
(22, 2, '19:15:00', 5),
(23, 2, '19:15:00', 6),
(24, 2, '20:15:00', 1),
(25, 2, '20:15:00', 2),
(26, 2, '20:15:00', 3),
(27, 2, '20:15:00', 4),
(28, 2, '20:15:00', 5),
(29, 2, '20:15:00', 6),
(30, 2, '21:15:00', 1),
(31, 2, '21:15:00', 2),
(32, 2, '21:15:00', 3),
(33, 2, '21:15:00', 4),
(34, 2, '21:15:00', 5),
(35, 2, '21:15:00', 6),
(36, 3, '19:15:00', 1),
(37, 3, '19:15:00', 2),
(38, 3, '19:15:00', 3),
(39, 3, '19:15:00', 4),
(40, 3, '19:15:00', 5),
(41, 3, '19:15:00', 6),
(42, 3, '20:15:00', 1),
(43, 3, '20:15:00', 2),
(44, 3, '20:15:00', 3),
(45, 3, '20:15:00', 4),
(46, 3, '20:15:00', 5),
(47, 3, '20:15:00', 6),
(48, 3, '21:15:00', 1),
(49, 3, '21:15:00', 2),
(50, 3, '21:15:00', 3),
(51, 3, '21:15:00', 4),
(52, 3, '21:15:00', 5),
(53, 3, '21:15:00', 6),
(54, 4, '19:15:00', 1),
(55, 4, '19:15:00', 2),
(56, 4, '19:15:00', 3),
(57, 4, '19:15:00', 4),
(58, 4, '19:15:00', 5),
(59, 4, '19:15:00', 6),
(60, 4, '20:15:00', 1),
(61, 4, '20:15:00', 2),
(62, 4, '20:15:00', 3),
(63, 4, '20:15:00', 4),
(64, 4, '20:15:00', 5),
(65, 4, '20:15:00', 6),
(66, 4, '21:15:00', 1),
(67, 4, '21:15:00', 2),
(68, 4, '21:15:00', 3),
(69, 4, '21:15:00', 4),
(70, 4, '21:15:00', 5),
(71, 4, '21:15:00', 6),
(72, 5, '19:15:00', 1),
(73, 5, '19:15:00', 2),
(74, 5, '19:15:00', 3),
(75, 5, '19:15:00', 4),
(76, 5, '19:15:00', 5),
(77, 5, '19:15:00', 6),
(78, 5, '20:15:00', 1),
(79, 5, '20:15:00', 2),
(80, 5, '20:15:00', 3),
(81, 5, '20:15:00', 4),
(82, 5, '20:15:00', 5),
(83, 5, '20:15:00', 6),
(84, 5, '21:15:00', 1),
(85, 5, '21:15:00', 2),
(86, 5, '21:15:00', 3),
(87, 5, '21:15:00', 4),
(88, 5, '21:15:00', 5),
(89, 5, '21:15:00', 6),
(90, 6, '19:15:00', 1),
(91, 6, '19:15:00', 2),
(92, 6, '19:15:00', 3),
(93, 6, '19:15:00', 4),
(94, 6, '19:15:00', 5),
(95, 6, '19:15:00', 6),
(96, 6, '20:15:00', 1),
(97, 6, '20:15:00', 2),
(98, 6, '20:15:00', 3),
(99, 6, '20:15:00', 4),
(100, 6, '20:15:00', 5),
(101, 6, '20:15:00', 6),
(102, 6, '21:15:00', 1),
(103, 6, '21:15:00', 2),
(104, 6, '21:15:00', 3),
(105, 6, '21:15:00', 4),
(106, 6, '21:15:00', 5),
(107, 6, '21:15:00', 6),
(108, 7, '19:15:00', 1),
(109, 7, '19:15:00', 2),
(110, 7, '19:15:00', 3),
(111, 7, '19:15:00', 4),
(112, 7, '19:15:00', 5),
(113, 7, '19:15:00', 6),
(114, 7, '20:15:00', 1),
(115, 7, '20:15:00', 2),
(116, 7, '20:15:00', 3),
(117, 7, '20:15:00', 4),
(118, 7, '20:15:00', 5),
(119, 7, '20:15:00', 6),
(120, 7, '21:15:00', 1),
(121, 7, '21:15:00', 2),
(122, 7, '21:15:00', 3),
(123, 7, '21:15:00', 4),
(124, 7, '21:15:00', 5),
(125, 7, '21:15:00', 6),
(126, 8, '19:15:00', 1),
(127, 8, '19:15:00', 2),
(128, 8, '19:15:00', 3),
(129, 8, '19:15:00', 4),
(130, 8, '19:15:00', 5),
(131, 8, '19:15:00', 6),
(132, 8, '20:15:00', 1),
(133, 8, '20:15:00', 2),
(134, 8, '20:15:00', 3),
(135, 8, '20:15:00', 4),
(136, 8, '20:15:00', 5),
(137, 8, '20:15:00', 6),
(138, 8, '21:15:00', 1),
(139, 8, '21:15:00', 2),
(140, 8, '21:15:00', 3),
(141, 8, '21:15:00', 4),
(142, 8, '21:15:00', 5),
(143, 8, '21:15:00', 6),
(144, 9, '19:15:00', 1),
(145, 9, '19:15:00', 2),
(146, 9, '19:15:00', 3),
(147, 9, '19:15:00', 4),
(148, 9, '19:15:00', 5),
(149, 9, '19:15:00', 6),
(150, 9, '20:15:00', 1),
(151, 9, '20:15:00', 2),
(152, 9, '20:15:00', 3),
(153, 9, '20:15:00', 4),
(154, 9, '20:15:00', 5),
(155, 9, '20:15:00', 6),
(156, 9, '21:15:00', 1),
(157, 9, '21:15:00', 2),
(158, 9, '21:15:00', 3),
(159, 9, '21:15:00', 4),
(160, 9, '21:15:00', 5),
(161, 9, '21:15:00', 6),
(162, 9, '19:15:00', 1),
(163, 10, '19:15:00', 2),
(164, 10, '19:15:00', 3),
(165, 10, '19:15:00', 4),
(166, 10, '19:15:00', 5),
(167, 10, '19:15:00', 6),
(168, 10, '20:15:00', 1),
(169, 10, '20:15:00', 2),
(170, 10, '20:15:00', 3),
(171, 10, '20:15:00', 4),
(172, 10, '20:15:00', 5),
(173, 10, '20:15:00', 6),
(174, 10, '21:15:00', 1),
(175, 10, '21:15:00', 2),
(176, 10, '21:15:00', 3),
(177, 10, '21:15:00', 4),
(178, 10, '21:15:00', 5),
(179, 10, '21:15:00', 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wedstrijdschema_view`
--

CREATE TABLE IF NOT EXISTS `wedstrijdschema_view` (
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structuur voor de view `statsview`
--
DROP TABLE IF EXISTS `statsview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `statsview` AS select `t`.`klassecode` AS `klassecode`,`u`.`teamid` AS `teamid`,`t`.`naam` AS `naam`,count(distinct `u`.`wedstrijdid`) AS `W`,(sum(`u`.`punten`) - `t`.`Strp`) AS `P`,sum(`u`.`score`) AS `Sv`,sum(`ut`.`score`) AS `St`,(sum(`u`.`score`) - sum(`ut`.`score`)) AS `S`,`t`.`Strp` AS `strp` from (((`uitslag_set_team` `u` join `uitslag_set_team` `ut`) join `w_team` `wt`) join `team_strp` `t`) where ((`u`.`wedstrijdid` = `ut`.`wedstrijdid`) and (`u`.`set` = `ut`.`set`) and (`u`.`teamid` <> `ut`.`teamid`) and (`wt`.`wedstrijdid` = `u`.`wedstrijdid`) and (`wt`.`teamid` = `u`.`teamid`) and (`t`.`id` = `u`.`teamid`)) group by `u`.`teamid` order by `t`.`klassecode`,`P` desc,`W`,`S` desc;

-- --------------------------------------------------------

--
-- Structuur voor de view `team_strp`
--
DROP TABLE IF EXISTS `team_strp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `team_strp` AS select `t`.`id` AS `id`,`t`.`klassecode` AS `klassecode`,`t`.`naam` AS `naam`,sum(`wt`.`strafpunten`) AS `Strp` from (`team` `t` join `w_team` `wt`) where (`t`.`id` = `wt`.`teamid`) group by `t`.`id` union select `t`.`id` AS `id`,`t`.`klassecode` AS `klassecode`,`t`.`naam` AS `naam`,0 AS `Strp` from `team` `t` where (not(`t`.`id` in (select distinct `w_team`.`teamid` from `w_team`))) order by `id`;

-- --------------------------------------------------------

--
-- Structuur voor de view `team_uitslag_view`
--
DROP TABLE IF EXISTS `team_uitslag_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `team_uitslag_view` AS select `t`.`id` AS `id`,`t`.`klassecode` AS `klassecode`,`t`.`naam` AS `naam`,`t`.`Strp` AS `Strp` from `team_strp` `t`;

-- --------------------------------------------------------

--
-- Structuur voor de view `teamwedstrijdview`
--
DROP TABLE IF EXISTS `teamwedstrijdview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teamwedstrijdview` AS select `wta`.`teamid` AS `teamid`,`s`.`datum` AS `datum`,`w`.`tijd` AS `tijd`,`w`.`veld` AS `veld`,`tb`.`naam` AS `naam` from ((((`w_team` `wta` join `wedstrijd` `w`) join `w_team` `wtb`) join `team` `tb`) join `speelweek` `s`) where ((`wta`.`rol` = 1) and (`wta`.`wedstrijdid` = `w`.`id`) and (`wtb`.`wedstrijdid` = `w`.`id`) and (`wtb`.`rol` = 2) and (`wtb`.`teamid` = `tb`.`id`) and (`s`.`id` = `w`.`speelweekid`)) union select `wtb`.`teamid` AS `teamid`,`s`.`datum` AS `datum`,`w`.`tijd` AS `tijd`,`w`.`veld` AS `veld`,`ta`.`naam` AS `naam` from ((((`w_team` `wtb` join `wedstrijd` `w`) join `w_team` `wta`) join `team` `ta`) join `speelweek` `s`) where ((`wtb`.`rol` = 2) and (`wtb`.`wedstrijdid` = `w`.`id`) and (`wta`.`wedstrijdid` = `w`.`id`) and (`wta`.`rol` = 1) and (`wta`.`teamid` = `ta`.`id`) and (`s`.`id` = `w`.`speelweekid`)) union select `wts`.`teamid` AS `teamid`,`s`.`datum` AS `datum`,`w`.`tijd` AS `tijd`,`w`.`veld` AS `veld`,'<i>Scheidsrechter</i>' AS `naam` from ((`w_team` `wts` join `wedstrijd` `w`) join `speelweek` `s`) where ((`wts`.`rol` = 3) and (`wts`.`wedstrijdid` = `w`.`id`) and (`s`.`id` = `w`.`speelweekid`)) order by `datum`,`tijd`,`veld`;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `klas`
--
ALTER TABLE `klas`
  ADD PRIMARY KEY (`code`);

--
-- Indexen voor tabel `lid`
--
ALTER TABLE `lid`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `speelweek`
--
ALTER TABLE `speelweek`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_team_klas_idx` (`klassecode`);

--
-- Indexen voor tabel `team_has_lid`
--
ALTER TABLE `team_has_lid`
  ADD PRIMARY KEY (`teamid`,`lidid`),
  ADD KEY `fk_team_has_lid_lid1_idx` (`lidid`),
  ADD KEY `fk_team_has_lid_team1_idx` (`teamid`);

--
-- Indexen voor tabel `uitslag_set_team`
--
ALTER TABLE `uitslag_set_team`
  ADD PRIMARY KEY (`wedstrijdid`,`set`,`teamid`),
  ADD KEY `fk_SET_has_WEDSTRIJD_has_team_WEDSTRIJD_has_team1_idx` (`wedstrijdid`,`teamid`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Userid`),
  ADD KEY `groupid` (`groupid`);

--
-- Indexen voor tabel `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `w_team`
--
ALTER TABLE `w_team`
  ADD PRIMARY KEY (`wedstrijdid`,`teamid`),
  ADD KEY `fk_WEDSTRIJD_has_team_team1_idx` (`teamid`),
  ADD KEY `fk_WEDSTRIJD_has_team_WEDSTRIJD1_idx` (`wedstrijdid`);

--
-- Indexen voor tabel `wedstrijd`
--
ALTER TABLE `wedstrijd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_wedstrijd_speelweek` (`speelweekid`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `lid`
--
ALTER TABLE `lid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT voor een tabel `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT voor een tabel `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `wedstrijd`
--
ALTER TABLE `wedstrijd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=180;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `fk_team_klas` FOREIGN KEY (`klassecode`) REFERENCES `klas` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `team_has_lid`
--
ALTER TABLE `team_has_lid`
  ADD CONSTRAINT `fk_team_has_lid_lid1` FOREIGN KEY (`lidid`) REFERENCES `lid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_team_has_lid_team1` FOREIGN KEY (`teamid`) REFERENCES `team` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `uitslag_set_team`
--
ALTER TABLE `uitslag_set_team`
  ADD CONSTRAINT `fk_SET_has_WEDSTRIJD_has_team_WEDSTRIJD_has_team1` FOREIGN KEY (`wedstrijdid`, `teamid`) REFERENCES `w_team` (`wedstrijdid`, `teamid`);

--
-- Beperkingen voor tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`groupid`) REFERENCES `user_groups` (`id`);

--
-- Beperkingen voor tabel `w_team`
--
ALTER TABLE `w_team`
  ADD CONSTRAINT `fk_WEDSTRIJD_has_team_WEDSTRIJD1` FOREIGN KEY (`wedstrijdid`) REFERENCES `wedstrijd` (`id`),
  ADD CONSTRAINT `fk_WEDSTRIJD_has_team_team1` FOREIGN KEY (`teamid`) REFERENCES `team` (`id`);

--
-- Beperkingen voor tabel `wedstrijd`
--
ALTER TABLE `wedstrijd`
  ADD CONSTRAINT `FK_wedstrijd_speelweek` FOREIGN KEY (`speelweekid`) REFERENCES `speelweek` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
