<?php
namespace Model;

/**
 * Represents Useful information of a page or menu-item referring to that page
 */
class PageMeta {

	public $home;

	public $module_path;

	public $uri;

	private $config;

	public function __construct($home, $uri, $module_path) {
		$this -> home = $home;
		$this -> uri = $uri;
		$this -> module_path = $module_path;
		$filename = $this -> module_path . "/config.json";
		if (file_exists($filename)) {
			$string = file_get_contents($filename);
			$this -> config = json_decode($string);

		} else {
			$this -> config = new \stdClass();
		}
	}

	public function getName() {
		if (property_exists($this -> config, "readablename")) {
			return $this -> config -> readablename;
		} else {
			return $this -> uri;
		}
	}

	public function isAuthorized($userLevel) {
		$accessLevel = 0;
		if (property_exists($this -> config, "accesslevel")) {
			$accessLevel = $this -> config -> accesslevel;
		}
		return $userLevel >= $accessLevel;
	 }

	public function isMenuitem($roles) {
		if (property_exists($this -> config, "display")) {
			return $this -> config -> display && $this -> isAuthorized($roles);
		} else {
			return $this -> isAuthorized($roles);
		}
	}

}
?>