<?php

namespace Model;

class DefaultTableModel extends AbstractPageModel
{

		public $items = array();

    public $columns = array();

    public function __construct($home, $uri, $module_path, $current_user, $entityName) {
    	parent::__construct($home, $uri, $module_path, $current_user);
			$entityQueryClass = 'Data\\'.$entityName.'Query';
			$this->items = $entityQueryClass::create()->find();
   	}

   	public function hasItems() {
   		return $this->items!=null || sizeof($this->items)>0;
   	}

   	public function renderCell($col, $row) {
   		$method = $col->getMethodName();
   		if (method_exists($this, $method)) {
   			return $this->$method($row);
   		} else {
	   		if (method_exists($row, $method)) {
	   			return $row->$method($row);
	   		} else {
	   			return "ERROR: method $method not found!";
	   		}
   		}
   	}

    public function getIdName() {
      return 'id';
    }

    public function getIdValue($row) {
      return $row->getId();
    }

    public function hasBulkActions() {
      return false;
    }

    public function hasButtons() {
      return $this->hasDetails() || $this->canEdit() || $this->canDelete();
    }

    public function hasDetails() {
      return $this->hasView('details');
    }

    public function canEdit() {
      return $this->hasView('edit');
    }

    public function canDelete() {
      return $this->hasView('delete');
    }

    private function hasView($v) {
      return file_exists("$this->module_path/$v/View.tpl");
    }

}
