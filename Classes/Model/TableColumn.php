<?php

namespace Model;

class TableColumn {
	
	public $name;

	public $headername;

	/**
	 * @param $name Unique name of this column. Is used to construct method name to retreive the content of a specific item
	 * @param $headername Readable name for the table header.
	 */
	public function __construct($name, $headername) {
		$this->name = $name;
		$this->headername = $headername;
	}

	public function getMethodName() {
		return "get" . $this->name;
	}

}
?>