<?php

namespace Model;

abstract class DefaultFormModel extends AbstractPageModel
{

		public $state = 'insert';

		public $item;

    public function __construct($home, $uri, $module_path, $current_user, $entityName, $pkQueryvarName = 'id') {
    	parent::__construct($home, $uri, $module_path, $current_user);

			if (isset($_GET[ $pkQueryvarName ])) {
					$entityQueryClass = 'Data\\'.$entityName.'Query';
					$this->item = $entityQueryClass::create()->requirePk($_GET[ $pkQueryvarName ]);
					$this->state = 'update';
			} else {
				$entityClass = 'Data\\'.$entityName;
				$this->item = new $entityClass();
			}
   	}

   	public abstract function insert();

   	public abstract function update();


}
