<?php

namespace Model;

class AbstractPageModel extends PageMeta {
	public $menu;

	public $current_user;

	public $redirect = '';

	public $errors = array();
	public $success = array();

	public function __construct($home, $uri, $module_path, $current_user) {
		parent::__construct($home, $uri, $module_path);
		$this -> current_user = $current_user;
		//Find the roles for the user
		$roles = array();
		if (isset($current_user))
			$roles = $current_user -> getUser_Groups();
		//Initialize the messages
		if (isset($_REQUEST['success']))
			$this -> success = unserialize(urldecode($_REQUEST['success']));
		if (isset($_REQUEST['error']))
			$this -> errors = unserialize(urldecode($_REQUEST['error']));
		//Initialize the menu
		$this -> menu = new MenuItem($home, 'modules', $roles);
	}

	public function isCurrentModule($uri) {
		return $this -> uri === $uri;
	}

	public function requestsRedirect() {
		return strlen($this -> redirect) > 0;
	}

	public function login() {
		$userid = $_REQUEST['userid'];
		$userquery = new \Data\UserQuery();
		$user = $userquery -> findOneByUserid($_REQUEST['userid']);

		if ($user) {
			if ($user -> isCorrectPassword($_REQUEST['passwd'])) {
				$this -> current_user = $user;
				$_SESSION['username'] = $user -> getUserid();
				$this -> success[] = 'Succesvol ingelogd. Welkom ' . $user -> getFullName() . '.';
			} else {
				$this -> errors[] = 'Ongeldig wachtwoord.';
			}
		} else {
			$this -> errors[] = 'Ongeldige gebruikersnaam.';
		}
		if (strlen($this -> uri) > 0) {
			$this -> redirect = $this -> uri;
		} else {
			$this -> redirect = $this -> home;
		}

	}

	public function logoff() {
		unset($_SESSION['username']);
		$this -> success[] = 'Succesvol uitgelogd. Tot ziens.';
		$this -> redirect = 'index.php';
	}

	public function getMaxUserAuth() {
		$result = 0;
		if (isset($this -> current_user)) {
			foreach($this -> current_user -> getUser_Groups() as $group) {
				if($group > $result) {
					$result = $group;
				}
			}
		}
		return $result;
	}

	public function isAuthorized() {
		return parent::isAuthorized($this->getMaxUserAuth());
	}

	public function hasAuthenticatedUser() {
		return $this -> current_user;
	}

	public function hasErrors() {
		return count($this -> errors) > 0;
	}

	public function hasSuccess() {
		return count($this -> success) > 0;
	}

}
