<?php

namespace Model;

use ArrayObject;

class MenuItem extends ArrayObject {

	public $path;

	public $uri;

	public $readablename;

	public $display = false;

	private $roles;

	public function __construct($uri, $path, $roles = []) {
		$this -> uri = $uri;
		$this -> path = $path;
		$filename = $this -> path . "/config.json";
		if (file_exists($filename)) {
			$string = file_get_contents($filename);
			$json = json_decode($string);
			if (property_exists($json, "readablename")) {
				$this -> readablename = $json -> readablename;
			} else {
				$this -> readablename = "Property 'readablename' is not defined in " . $this -> getPath() . "/config.json";
			}

			if (property_exists($json, "accesslevel")) {
				$this -> accesslevel = $json -> accesslevel;
			} else {
				$this -> accesslevel = 0;
			}

			if (isset($roles->niveau)) {
				$this->niveau = $roles->niveau;
			} else {
				$this->niveau = 0;
			}
			
			if (property_exists($json, "display")) {
				$this -> display = $json -> display && $this -> isAuthorized($this->niveau,$this -> accesslevel);
			} else {
				$this -> display = $this -> isAuthorized($this->niveau,$this -> accesslevel);
			}

		} else {
			$this -> readablename = $path;
		}
		$this -> initChildren($roles);
	}

	public function isAuthorized($userLevel,$accessLevel) {
		if (!$userLevel) {
			$userLevel=0;
		}
		return $userLevel >= $accessLevel;
	}
	// public function isAuthorized($roles) {
		// return empty($this -> roles) || sizeof(array_intersect($this -> roles, $roles)) > 0;
	// }

	public function hasChildren() {
		return $this -> count() > 0;
	}

	private function initChildren($roles) {
		$directories = glob($this -> path . '/*', GLOB_ONLYDIR);
		foreach ($directories as $directory) {
			$name = basename($directory);
			$path = $this -> path . '/' . $name;
			$uri = $this -> uri . '/' . $name;
			$item = new MenuItem($uri, $path, $roles);
			if ($item -> display)
				$this -> append($item);
		}
	}

}
?>