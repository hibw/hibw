<?php

namespace Data;

use Data\Base\Wedstrijd as BaseWedstrijd;

/**
 * Skeleton subclass for representing a row from the 'wedstrijd' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Wedstrijd extends BaseWedstrijd
{

}
