<?php

namespace Data;

use Data\Base\Teamwedstrijd as BaseTeamwedstrijd;

/**
 * Skeleton subclass for representing a row from the 'teamwedstrijdview' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Teamwedstrijd extends BaseTeamwedstrijd
{

}
