<?php

namespace Data;

use Data\Base\User as BaseUser;

/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{

    public function getFullname() {
        return $this->getNameFirst() . ' ' . $this->getNameLast();
    }

    /**
     * Set the value of [password] column.
     * 
     * @param string $v new value
     * @return $this|\User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
    		return parent::setPassword($this->crypt($v));
    } // setPassword()

    /**
     * Set the value of [password] column.
     * 
     * @param string $v new value
     * @return $this|\User The current object (for fluent API support)
     */
    public function setPasswordWithConfirm($v, $v2)
    {
        if(!($v === $v2))
            throw new \Exception('Wachtwoorden zijn niet gelijk');
        return parent::setPassword($this->crypt($v));
    } // setPassword()


    /**
     * Checks if the password matches the password of this user
     *
     * @param string $pwd the password to be checked
     * @return true if the passwords are equal
     */
    public function isCorrectPassword($pwd) {
    		return $this->getPassword() === $this->crypt($pwd);
    }


    /*
     * Helper method that hashes a string as it is supposed to be in this application
     *
     * @param string $pwd to be hashed
     * @return the hashed value
     */
    private function crypt($pwd) {
    		return crypt($pwd, 'fietsbel');
    }


    public function getRoles() {
		return "";
    }
}
