<?php

namespace Data\Map;

use Data\Teamwedstrijd;
use Data\TeamwedstrijdQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'teamwedstrijdview' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TeamwedstrijdTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Data.Map.TeamwedstrijdTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'mbvolley';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'teamwedstrijdview';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Data\\Teamwedstrijd';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Data.Teamwedstrijd';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the teamid field
     */
    const COL_TEAMID = 'teamwedstrijdview.teamid';

    /**
     * the column name for the datum field
     */
    const COL_DATUM = 'teamwedstrijdview.datum';

    /**
     * the column name for the tijd field
     */
    const COL_TIJD = 'teamwedstrijdview.tijd';

    /**
     * the column name for the veld field
     */
    const COL_VELD = 'teamwedstrijdview.veld';

    /**
     * the column name for the naam field
     */
    const COL_NAAM = 'teamwedstrijdview.naam';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Teamid', 'Datum', 'Tijd', 'Veld', 'Naam', ),
        self::TYPE_CAMELNAME     => array('teamid', 'datum', 'tijd', 'veld', 'naam', ),
        self::TYPE_COLNAME       => array(TeamwedstrijdTableMap::COL_TEAMID, TeamwedstrijdTableMap::COL_DATUM, TeamwedstrijdTableMap::COL_TIJD, TeamwedstrijdTableMap::COL_VELD, TeamwedstrijdTableMap::COL_NAAM, ),
        self::TYPE_FIELDNAME     => array('teamid', 'datum', 'tijd', 'veld', 'naam', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Teamid' => 0, 'Datum' => 1, 'Tijd' => 2, 'Veld' => 3, 'Naam' => 4, ),
        self::TYPE_CAMELNAME     => array('teamid' => 0, 'datum' => 1, 'tijd' => 2, 'veld' => 3, 'naam' => 4, ),
        self::TYPE_COLNAME       => array(TeamwedstrijdTableMap::COL_TEAMID => 0, TeamwedstrijdTableMap::COL_DATUM => 1, TeamwedstrijdTableMap::COL_TIJD => 2, TeamwedstrijdTableMap::COL_VELD => 3, TeamwedstrijdTableMap::COL_NAAM => 4, ),
        self::TYPE_FIELDNAME     => array('teamid' => 0, 'datum' => 1, 'tijd' => 2, 'veld' => 3, 'naam' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('teamwedstrijdview');
        $this->setPhpName('Teamwedstrijd');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Data\\Teamwedstrijd');
        $this->setPackage('Data');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('teamid', 'Teamid', 'INTEGER', 'team', 'id', false, null, null);
        $this->addColumn('datum', 'Datum', 'DATE', false, null, null);
        $this->addColumn('tijd', 'Tijd', 'TIME', false, null, null);
        $this->addColumn('veld', 'Veld', 'VARCHAR', false, 255, null);
        $this->addColumn('naam', 'Naam', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Team', '\\Data\\Team', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':teamid',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return null;
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return '';
    }
    
    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TeamwedstrijdTableMap::CLASS_DEFAULT : TeamwedstrijdTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Teamwedstrijd object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TeamwedstrijdTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TeamwedstrijdTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TeamwedstrijdTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TeamwedstrijdTableMap::OM_CLASS;
            /** @var Teamwedstrijd $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TeamwedstrijdTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TeamwedstrijdTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TeamwedstrijdTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Teamwedstrijd $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TeamwedstrijdTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TeamwedstrijdTableMap::COL_TEAMID);
            $criteria->addSelectColumn(TeamwedstrijdTableMap::COL_DATUM);
            $criteria->addSelectColumn(TeamwedstrijdTableMap::COL_TIJD);
            $criteria->addSelectColumn(TeamwedstrijdTableMap::COL_VELD);
            $criteria->addSelectColumn(TeamwedstrijdTableMap::COL_NAAM);
        } else {
            $criteria->addSelectColumn($alias . '.teamid');
            $criteria->addSelectColumn($alias . '.datum');
            $criteria->addSelectColumn($alias . '.tijd');
            $criteria->addSelectColumn($alias . '.veld');
            $criteria->addSelectColumn($alias . '.naam');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TeamwedstrijdTableMap::DATABASE_NAME)->getTable(TeamwedstrijdTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TeamwedstrijdTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TeamwedstrijdTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TeamwedstrijdTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Teamwedstrijd or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Teamwedstrijd object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamwedstrijdTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Data\Teamwedstrijd) { // it's a model object
            // create criteria based on pk value
            $criteria = $values->buildCriteria();
        } else { // it's a primary key, or an array of pks
            throw new LogicException('The Teamwedstrijd object has no primary key');
        }

        $query = TeamwedstrijdQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TeamwedstrijdTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TeamwedstrijdTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the teamwedstrijdview table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TeamwedstrijdQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Teamwedstrijd or Criteria object.
     *
     * @param mixed               $criteria Criteria or Teamwedstrijd object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamwedstrijdTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Teamwedstrijd object
        }


        // Set the correct dbName
        $query = TeamwedstrijdQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TeamwedstrijdTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TeamwedstrijdTableMap::buildTableMap();
