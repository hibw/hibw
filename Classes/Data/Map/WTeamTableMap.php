<?php

namespace Data\Map;

use Data\WTeam;
use Data\WTeamQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'w_team' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class WTeamTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Data.Map.WTeamTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'mbvolley';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'w_team';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Data\\WTeam';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Data.WTeam';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the wedstrijdid field
     */
    const COL_WEDSTRIJDID = 'w_team.wedstrijdid';

    /**
     * the column name for the teamid field
     */
    const COL_TEAMID = 'w_team.teamid';

    /**
     * the column name for the rol field
     */
    const COL_ROL = 'w_team.rol';

    /**
     * the column name for the strafpunten field
     */
    const COL_STRAFPUNTEN = 'w_team.strafpunten';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Wedstrijdid', 'Teamid', 'Rol', 'Strafpunten', ),
        self::TYPE_CAMELNAME     => array('wedstrijdid', 'teamid', 'rol', 'strafpunten', ),
        self::TYPE_COLNAME       => array(WTeamTableMap::COL_WEDSTRIJDID, WTeamTableMap::COL_TEAMID, WTeamTableMap::COL_ROL, WTeamTableMap::COL_STRAFPUNTEN, ),
        self::TYPE_FIELDNAME     => array('wedstrijdid', 'teamid', 'rol', 'strafpunten', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Wedstrijdid' => 0, 'Teamid' => 1, 'Rol' => 2, 'Strafpunten' => 3, ),
        self::TYPE_CAMELNAME     => array('wedstrijdid' => 0, 'teamid' => 1, 'rol' => 2, 'strafpunten' => 3, ),
        self::TYPE_COLNAME       => array(WTeamTableMap::COL_WEDSTRIJDID => 0, WTeamTableMap::COL_TEAMID => 1, WTeamTableMap::COL_ROL => 2, WTeamTableMap::COL_STRAFPUNTEN => 3, ),
        self::TYPE_FIELDNAME     => array('wedstrijdid' => 0, 'teamid' => 1, 'rol' => 2, 'strafpunten' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('w_team');
        $this->setPhpName('WTeam');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Data\\WTeam');
        $this->setPackage('Data');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('wedstrijdid', 'Wedstrijdid', 'INTEGER' , 'wedstrijd', 'id', true, null, null);
        $this->addForeignPrimaryKey('teamid', 'Teamid', 'INTEGER' , 'team', 'id', true, null, null);
        $this->addColumn('rol', 'Rol', 'INTEGER', true, null, null);
        $this->addColumn('strafpunten', 'Strafpunten', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Wedstrijd', '\\Data\\Wedstrijd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':wedstrijdid',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Team', '\\Data\\Team', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':teamid',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UitslagSetTeam', '\\Data\\UitslagSetTeam', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':wedstrijdid',
    1 => ':wedstrijdid',
  ),
  1 =>
  array (
    0 => ':teamid',
    1 => ':teamid',
  ),
), null, null, 'UitslagSetTeams', false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Data\WTeam $obj A \Data\WTeam object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize(array((string) $obj->getWedstrijdid(), (string) $obj->getTeamid()));
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Data\WTeam object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Data\WTeam) {
                $key = serialize(array((string) $value->getWedstrijdid(), (string) $value->getTeamid()));

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Data\WTeam object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Wedstrijdid', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Teamid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize(array((string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Wedstrijdid', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Teamid', TableMap::TYPE_PHPNAME, $indexType)]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];
            
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Wedstrijdid', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('Teamid', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }
    
    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? WTeamTableMap::CLASS_DEFAULT : WTeamTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (WTeam object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = WTeamTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = WTeamTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + WTeamTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = WTeamTableMap::OM_CLASS;
            /** @var WTeam $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            WTeamTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = WTeamTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = WTeamTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var WTeam $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                WTeamTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(WTeamTableMap::COL_WEDSTRIJDID);
            $criteria->addSelectColumn(WTeamTableMap::COL_TEAMID);
            $criteria->addSelectColumn(WTeamTableMap::COL_ROL);
            $criteria->addSelectColumn(WTeamTableMap::COL_STRAFPUNTEN);
        } else {
            $criteria->addSelectColumn($alias . '.wedstrijdid');
            $criteria->addSelectColumn($alias . '.teamid');
            $criteria->addSelectColumn($alias . '.rol');
            $criteria->addSelectColumn($alias . '.strafpunten');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(WTeamTableMap::DATABASE_NAME)->getTable(WTeamTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(WTeamTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(WTeamTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new WTeamTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a WTeam or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or WTeam object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WTeamTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Data\WTeam) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(WTeamTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(WTeamTableMap::COL_WEDSTRIJDID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(WTeamTableMap::COL_TEAMID, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = WTeamQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            WTeamTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                WTeamTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the w_team table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return WTeamQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a WTeam or Criteria object.
     *
     * @param mixed               $criteria Criteria or WTeam object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WTeamTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from WTeam object
        }


        // Set the correct dbName
        $query = WTeamQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // WTeamTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
WTeamTableMap::buildTableMap();
