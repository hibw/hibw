<?php

namespace Data;

use Data\Base\WTeam as BaseWTeam;

/**
 * Skeleton subclass for representing a row from the 'w_team' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class WTeam extends BaseWTeam
{

}
