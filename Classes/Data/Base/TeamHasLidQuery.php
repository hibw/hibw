<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\TeamHasLid as ChildTeamHasLid;
use Data\TeamHasLidQuery as ChildTeamHasLidQuery;
use Data\Map\TeamHasLidTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'team_has_lid' table.
 *
 * 
 *
 * @method     ChildTeamHasLidQuery orderByTeamid($order = Criteria::ASC) Order by the teamid column
 * @method     ChildTeamHasLidQuery orderByLidid($order = Criteria::ASC) Order by the lidid column
 *
 * @method     ChildTeamHasLidQuery groupByTeamid() Group by the teamid column
 * @method     ChildTeamHasLidQuery groupByLidid() Group by the lidid column
 *
 * @method     ChildTeamHasLidQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTeamHasLidQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTeamHasLidQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTeamHasLidQuery leftJoinLid($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lid relation
 * @method     ChildTeamHasLidQuery rightJoinLid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lid relation
 * @method     ChildTeamHasLidQuery innerJoinLid($relationAlias = null) Adds a INNER JOIN clause to the query using the Lid relation
 *
 * @method     ChildTeamHasLidQuery leftJoinTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the Team relation
 * @method     ChildTeamHasLidQuery rightJoinTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Team relation
 * @method     ChildTeamHasLidQuery innerJoinTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the Team relation
 *
 * @method     \Data\LidQuery|\Data\TeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTeamHasLid findOne(ConnectionInterface $con = null) Return the first ChildTeamHasLid matching the query
 * @method     ChildTeamHasLid findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTeamHasLid matching the query, or a new ChildTeamHasLid object populated from the query conditions when no match is found
 *
 * @method     ChildTeamHasLid findOneByTeamid(int $teamid) Return the first ChildTeamHasLid filtered by the teamid column
 * @method     ChildTeamHasLid findOneByLidid(int $lidid) Return the first ChildTeamHasLid filtered by the lidid column *

 * @method     ChildTeamHasLid requirePk($key, ConnectionInterface $con = null) Return the ChildTeamHasLid by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamHasLid requireOne(ConnectionInterface $con = null) Return the first ChildTeamHasLid matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeamHasLid requireOneByTeamid(int $teamid) Return the first ChildTeamHasLid filtered by the teamid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamHasLid requireOneByLidid(int $lidid) Return the first ChildTeamHasLid filtered by the lidid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeamHasLid[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTeamHasLid objects based on current ModelCriteria
 * @method     ChildTeamHasLid[]|ObjectCollection findByTeamid(int $teamid) Return ChildTeamHasLid objects filtered by the teamid column
 * @method     ChildTeamHasLid[]|ObjectCollection findByLidid(int $lidid) Return ChildTeamHasLid objects filtered by the lidid column
 * @method     ChildTeamHasLid[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TeamHasLidQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\TeamHasLidQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\TeamHasLid', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTeamHasLidQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTeamHasLidQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTeamHasLidQuery) {
            return $criteria;
        }
        $query = new ChildTeamHasLidQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$teamid, $lidid] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTeamHasLid|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TeamHasLidTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TeamHasLidTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeamHasLid A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT teamid, lidid FROM team_has_lid WHERE teamid = :p0 AND lidid = :p1';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);            
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTeamHasLid $obj */
            $obj = new ChildTeamHasLid();
            $obj->hydrate($row);
            TeamHasLidTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTeamHasLid|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(TeamHasLidTableMap::COL_TEAMID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(TeamHasLidTableMap::COL_LIDID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the teamid column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamid(1234); // WHERE teamid = 1234
     * $query->filterByTeamid(array(12, 34)); // WHERE teamid IN (12, 34)
     * $query->filterByTeamid(array('min' => 12)); // WHERE teamid > 12
     * </code>
     *
     * @see       filterByTeam()
     *
     * @param     mixed $teamid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByTeamid($teamid = null, $comparison = null)
    {
        if (is_array($teamid)) {
            $useMinMax = false;
            if (isset($teamid['min'])) {
                $this->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $teamid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamid['max'])) {
                $this->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $teamid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $teamid, $comparison);
    }

    /**
     * Filter the query on the lidid column
     *
     * Example usage:
     * <code>
     * $query->filterByLidid(1234); // WHERE lidid = 1234
     * $query->filterByLidid(array(12, 34)); // WHERE lidid IN (12, 34)
     * $query->filterByLidid(array('min' => 12)); // WHERE lidid > 12
     * </code>
     *
     * @see       filterByLid()
     *
     * @param     mixed $lidid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByLidid($lidid = null, $comparison = null)
    {
        if (is_array($lidid)) {
            $useMinMax = false;
            if (isset($lidid['min'])) {
                $this->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $lidid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lidid['max'])) {
                $this->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $lidid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $lidid, $comparison);
    }

    /**
     * Filter the query by a related \Data\Lid object
     *
     * @param \Data\Lid|ObjectCollection $lid The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByLid($lid, $comparison = null)
    {
        if ($lid instanceof \Data\Lid) {
            return $this
                ->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $lid->getId(), $comparison);
        } elseif ($lid instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TeamHasLidTableMap::COL_LIDID, $lid->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLid() only accepts arguments of type \Data\Lid or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function joinLid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lid');
        }

        return $this;
    }

    /**
     * Use the Lid relation Lid object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\LidQuery A secondary query class using the current class as primary query
     */
    public function useLidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lid', '\Data\LidQuery');
    }

    /**
     * Filter the query by a related \Data\Team object
     *
     * @param \Data\Team|ObjectCollection $team The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function filterByTeam($team, $comparison = null)
    {
        if ($team instanceof \Data\Team) {
            return $this
                ->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $team->getId(), $comparison);
        } elseif ($team instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TeamHasLidTableMap::COL_TEAMID, $team->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTeam() only accepts arguments of type \Data\Team or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Team relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function joinTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Team');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Team');
        }

        return $this;
    }

    /**
     * Use the Team relation Team object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamQuery A secondary query class using the current class as primary query
     */
    public function useTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Team', '\Data\TeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTeamHasLid $teamHasLid Object to remove from the list of results
     *
     * @return $this|ChildTeamHasLidQuery The current query, for fluid interface
     */
    public function prune($teamHasLid = null)
    {
        if ($teamHasLid) {
            $this->addCond('pruneCond0', $this->getAliasedColName(TeamHasLidTableMap::COL_TEAMID), $teamHasLid->getTeamid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(TeamHasLidTableMap::COL_LIDID), $teamHasLid->getLidid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the team_has_lid table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamHasLidTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TeamHasLidTableMap::clearInstancePool();
            TeamHasLidTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamHasLidTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TeamHasLidTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            TeamHasLidTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            TeamHasLidTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TeamHasLidQuery
