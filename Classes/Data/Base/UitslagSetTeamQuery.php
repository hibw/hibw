<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\UitslagSetTeam as ChildUitslagSetTeam;
use Data\UitslagSetTeamQuery as ChildUitslagSetTeamQuery;
use Data\Map\UitslagSetTeamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'uitslag_set_team' table.
 *
 * 
 *
 * @method     ChildUitslagSetTeamQuery orderByWedstrijdid($order = Criteria::ASC) Order by the wedstrijdid column
 * @method     ChildUitslagSetTeamQuery orderBySet($order = Criteria::ASC) Order by the set column
 * @method     ChildUitslagSetTeamQuery orderByTeamid($order = Criteria::ASC) Order by the teamid column
 * @method     ChildUitslagSetTeamQuery orderByScore($order = Criteria::ASC) Order by the score column
 * @method     ChildUitslagSetTeamQuery orderByPunten($order = Criteria::ASC) Order by the punten column
 *
 * @method     ChildUitslagSetTeamQuery groupByWedstrijdid() Group by the wedstrijdid column
 * @method     ChildUitslagSetTeamQuery groupBySet() Group by the set column
 * @method     ChildUitslagSetTeamQuery groupByTeamid() Group by the teamid column
 * @method     ChildUitslagSetTeamQuery groupByScore() Group by the score column
 * @method     ChildUitslagSetTeamQuery groupByPunten() Group by the punten column
 *
 * @method     ChildUitslagSetTeamQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUitslagSetTeamQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUitslagSetTeamQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUitslagSetTeamQuery leftJoinWTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the WTeam relation
 * @method     ChildUitslagSetTeamQuery rightJoinWTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WTeam relation
 * @method     ChildUitslagSetTeamQuery innerJoinWTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the WTeam relation
 *
 * @method     \Data\WTeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUitslagSetTeam findOne(ConnectionInterface $con = null) Return the first ChildUitslagSetTeam matching the query
 * @method     ChildUitslagSetTeam findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUitslagSetTeam matching the query, or a new ChildUitslagSetTeam object populated from the query conditions when no match is found
 *
 * @method     ChildUitslagSetTeam findOneByWedstrijdid(int $wedstrijdid) Return the first ChildUitslagSetTeam filtered by the wedstrijdid column
 * @method     ChildUitslagSetTeam findOneBySet(int $set) Return the first ChildUitslagSetTeam filtered by the set column
 * @method     ChildUitslagSetTeam findOneByTeamid(int $teamid) Return the first ChildUitslagSetTeam filtered by the teamid column
 * @method     ChildUitslagSetTeam findOneByScore(int $score) Return the first ChildUitslagSetTeam filtered by the score column
 * @method     ChildUitslagSetTeam findOneByPunten(int $punten) Return the first ChildUitslagSetTeam filtered by the punten column *

 * @method     ChildUitslagSetTeam requirePk($key, ConnectionInterface $con = null) Return the ChildUitslagSetTeam by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUitslagSetTeam requireOne(ConnectionInterface $con = null) Return the first ChildUitslagSetTeam matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUitslagSetTeam requireOneByWedstrijdid(int $wedstrijdid) Return the first ChildUitslagSetTeam filtered by the wedstrijdid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUitslagSetTeam requireOneBySet(int $set) Return the first ChildUitslagSetTeam filtered by the set column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUitslagSetTeam requireOneByTeamid(int $teamid) Return the first ChildUitslagSetTeam filtered by the teamid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUitslagSetTeam requireOneByScore(int $score) Return the first ChildUitslagSetTeam filtered by the score column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUitslagSetTeam requireOneByPunten(int $punten) Return the first ChildUitslagSetTeam filtered by the punten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUitslagSetTeam[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUitslagSetTeam objects based on current ModelCriteria
 * @method     ChildUitslagSetTeam[]|ObjectCollection findByWedstrijdid(int $wedstrijdid) Return ChildUitslagSetTeam objects filtered by the wedstrijdid column
 * @method     ChildUitslagSetTeam[]|ObjectCollection findBySet(int $set) Return ChildUitslagSetTeam objects filtered by the set column
 * @method     ChildUitslagSetTeam[]|ObjectCollection findByTeamid(int $teamid) Return ChildUitslagSetTeam objects filtered by the teamid column
 * @method     ChildUitslagSetTeam[]|ObjectCollection findByScore(int $score) Return ChildUitslagSetTeam objects filtered by the score column
 * @method     ChildUitslagSetTeam[]|ObjectCollection findByPunten(int $punten) Return ChildUitslagSetTeam objects filtered by the punten column
 * @method     ChildUitslagSetTeam[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UitslagSetTeamQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\UitslagSetTeamQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\UitslagSetTeam', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUitslagSetTeamQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUitslagSetTeamQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUitslagSetTeamQuery) {
            return $criteria;
        }
        $query = new ChildUitslagSetTeamQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$wedstrijdid, $set, $teamid] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUitslagSetTeam|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UitslagSetTeamTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UitslagSetTeamTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUitslagSetTeam A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT wedstrijdid, set, teamid, score, punten FROM uitslag_set_team WHERE wedstrijdid = :p0 AND set = :p1 AND teamid = :p2';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);            
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);            
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUitslagSetTeam $obj */
            $obj = new ChildUitslagSetTeam();
            $obj->hydrate($row);
            UitslagSetTeamTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUitslagSetTeam|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(UitslagSetTeamTableMap::COL_SET, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(UitslagSetTeamTableMap::COL_TEAMID, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(UitslagSetTeamTableMap::COL_SET, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(UitslagSetTeamTableMap::COL_TEAMID, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the wedstrijdid column
     *
     * Example usage:
     * <code>
     * $query->filterByWedstrijdid(1234); // WHERE wedstrijdid = 1234
     * $query->filterByWedstrijdid(array(12, 34)); // WHERE wedstrijdid IN (12, 34)
     * $query->filterByWedstrijdid(array('min' => 12)); // WHERE wedstrijdid > 12
     * </code>
     *
     * @see       filterByWTeam()
     *
     * @param     mixed $wedstrijdid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByWedstrijdid($wedstrijdid = null, $comparison = null)
    {
        if (is_array($wedstrijdid)) {
            $useMinMax = false;
            if (isset($wedstrijdid['min'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wedstrijdid['max'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid, $comparison);
    }

    /**
     * Filter the query on the set column
     *
     * Example usage:
     * <code>
     * $query->filterBySet(1234); // WHERE set = 1234
     * $query->filterBySet(array(12, 34)); // WHERE set IN (12, 34)
     * $query->filterBySet(array('min' => 12)); // WHERE set > 12
     * </code>
     *
     * @param     mixed $set The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterBySet($set = null, $comparison = null)
    {
        if (is_array($set)) {
            $useMinMax = false;
            if (isset($set['min'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_SET, $set['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($set['max'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_SET, $set['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UitslagSetTeamTableMap::COL_SET, $set, $comparison);
    }

    /**
     * Filter the query on the teamid column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamid(1234); // WHERE teamid = 1234
     * $query->filterByTeamid(array(12, 34)); // WHERE teamid IN (12, 34)
     * $query->filterByTeamid(array('min' => 12)); // WHERE teamid > 12
     * </code>
     *
     * @see       filterByWTeam()
     *
     * @param     mixed $teamid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByTeamid($teamid = null, $comparison = null)
    {
        if (is_array($teamid)) {
            $useMinMax = false;
            if (isset($teamid['min'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_TEAMID, $teamid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamid['max'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_TEAMID, $teamid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UitslagSetTeamTableMap::COL_TEAMID, $teamid, $comparison);
    }

    /**
     * Filter the query on the score column
     *
     * Example usage:
     * <code>
     * $query->filterByScore(1234); // WHERE score = 1234
     * $query->filterByScore(array(12, 34)); // WHERE score IN (12, 34)
     * $query->filterByScore(array('min' => 12)); // WHERE score > 12
     * </code>
     *
     * @param     mixed $score The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByScore($score = null, $comparison = null)
    {
        if (is_array($score)) {
            $useMinMax = false;
            if (isset($score['min'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_SCORE, $score['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($score['max'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_SCORE, $score['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UitslagSetTeamTableMap::COL_SCORE, $score, $comparison);
    }

    /**
     * Filter the query on the punten column
     *
     * Example usage:
     * <code>
     * $query->filterByPunten(1234); // WHERE punten = 1234
     * $query->filterByPunten(array(12, 34)); // WHERE punten IN (12, 34)
     * $query->filterByPunten(array('min' => 12)); // WHERE punten > 12
     * </code>
     *
     * @param     mixed $punten The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByPunten($punten = null, $comparison = null)
    {
        if (is_array($punten)) {
            $useMinMax = false;
            if (isset($punten['min'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_PUNTEN, $punten['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($punten['max'])) {
                $this->addUsingAlias(UitslagSetTeamTableMap::COL_PUNTEN, $punten['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UitslagSetTeamTableMap::COL_PUNTEN, $punten, $comparison);
    }

    /**
     * Filter the query by a related \Data\WTeam object
     *
     * @param \Data\WTeam $wTeam The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function filterByWTeam($wTeam, $comparison = null)
    {
        if ($wTeam instanceof \Data\WTeam) {
            return $this
                ->addUsingAlias(UitslagSetTeamTableMap::COL_WEDSTRIJDID, $wTeam->getWedstrijdid(), $comparison)
                ->addUsingAlias(UitslagSetTeamTableMap::COL_TEAMID, $wTeam->getTeamid(), $comparison);
        } else {
            throw new PropelException('filterByWTeam() only accepts arguments of type \Data\WTeam');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WTeam relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function joinWTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WTeam');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WTeam');
        }

        return $this;
    }

    /**
     * Use the WTeam relation WTeam object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\WTeamQuery A secondary query class using the current class as primary query
     */
    public function useWTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WTeam', '\Data\WTeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUitslagSetTeam $uitslagSetTeam Object to remove from the list of results
     *
     * @return $this|ChildUitslagSetTeamQuery The current query, for fluid interface
     */
    public function prune($uitslagSetTeam = null)
    {
        if ($uitslagSetTeam) {
            $this->addCond('pruneCond0', $this->getAliasedColName(UitslagSetTeamTableMap::COL_WEDSTRIJDID), $uitslagSetTeam->getWedstrijdid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(UitslagSetTeamTableMap::COL_SET), $uitslagSetTeam->getSet(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(UitslagSetTeamTableMap::COL_TEAMID), $uitslagSetTeam->getTeamid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the uitslag_set_team table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UitslagSetTeamTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UitslagSetTeamTableMap::clearInstancePool();
            UitslagSetTeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UitslagSetTeamTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UitslagSetTeamTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            UitslagSetTeamTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            UitslagSetTeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UitslagSetTeamQuery
