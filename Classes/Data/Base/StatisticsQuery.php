<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\Statistics as ChildStatistics;
use Data\StatisticsQuery as ChildStatisticsQuery;
use Data\Map\StatisticsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'statsview' table.
 *
 * 
 *
 * @method     ChildStatisticsQuery orderByTeamid($order = Criteria::ASC) Order by the teamid column
 * @method     ChildStatisticsQuery orderByKlassecode($order = Criteria::ASC) Order by the klassecode column
 * @method     ChildStatisticsQuery orderByNaam($order = Criteria::ASC) Order by the naam column
 * @method     ChildStatisticsQuery orderByW($order = Criteria::ASC) Order by the w column
 * @method     ChildStatisticsQuery orderByP($order = Criteria::ASC) Order by the p column
 * @method     ChildStatisticsQuery orderBySv($order = Criteria::ASC) Order by the sv column
 * @method     ChildStatisticsQuery orderBySt($order = Criteria::ASC) Order by the st column
 * @method     ChildStatisticsQuery orderByS($order = Criteria::ASC) Order by the s column
 * @method     ChildStatisticsQuery orderByStrp($order = Criteria::ASC) Order by the strp column
 *
 * @method     ChildStatisticsQuery groupByTeamid() Group by the teamid column
 * @method     ChildStatisticsQuery groupByKlassecode() Group by the klassecode column
 * @method     ChildStatisticsQuery groupByNaam() Group by the naam column
 * @method     ChildStatisticsQuery groupByW() Group by the w column
 * @method     ChildStatisticsQuery groupByP() Group by the p column
 * @method     ChildStatisticsQuery groupBySv() Group by the sv column
 * @method     ChildStatisticsQuery groupBySt() Group by the st column
 * @method     ChildStatisticsQuery groupByS() Group by the s column
 * @method     ChildStatisticsQuery groupByStrp() Group by the strp column
 *
 * @method     ChildStatisticsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStatisticsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStatisticsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStatisticsQuery leftJoinTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the Team relation
 * @method     ChildStatisticsQuery rightJoinTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Team relation
 * @method     ChildStatisticsQuery innerJoinTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the Team relation
 *
 * @method     \Data\TeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStatistics findOne(ConnectionInterface $con = null) Return the first ChildStatistics matching the query
 * @method     ChildStatistics findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStatistics matching the query, or a new ChildStatistics object populated from the query conditions when no match is found
 *
 * @method     ChildStatistics findOneByTeamid(int $teamid) Return the first ChildStatistics filtered by the teamid column
 * @method     ChildStatistics findOneByKlassecode(string $klassecode) Return the first ChildStatistics filtered by the klassecode column
 * @method     ChildStatistics findOneByNaam(string $naam) Return the first ChildStatistics filtered by the naam column
 * @method     ChildStatistics findOneByW(int $w) Return the first ChildStatistics filtered by the w column
 * @method     ChildStatistics findOneByP(int $p) Return the first ChildStatistics filtered by the p column
 * @method     ChildStatistics findOneBySv(int $sv) Return the first ChildStatistics filtered by the sv column
 * @method     ChildStatistics findOneBySt(int $st) Return the first ChildStatistics filtered by the st column
 * @method     ChildStatistics findOneByS(int $s) Return the first ChildStatistics filtered by the s column
 * @method     ChildStatistics findOneByStrp(int $strp) Return the first ChildStatistics filtered by the strp column *

 * @method     ChildStatistics requirePk($key, ConnectionInterface $con = null) Return the ChildStatistics by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOne(ConnectionInterface $con = null) Return the first ChildStatistics matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStatistics requireOneByTeamid(int $teamid) Return the first ChildStatistics filtered by the teamid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByKlassecode(string $klassecode) Return the first ChildStatistics filtered by the klassecode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByNaam(string $naam) Return the first ChildStatistics filtered by the naam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByW(int $w) Return the first ChildStatistics filtered by the w column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByP(int $p) Return the first ChildStatistics filtered by the p column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneBySv(int $sv) Return the first ChildStatistics filtered by the sv column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneBySt(int $st) Return the first ChildStatistics filtered by the st column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByS(int $s) Return the first ChildStatistics filtered by the s column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStatistics requireOneByStrp(int $strp) Return the first ChildStatistics filtered by the strp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStatistics[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStatistics objects based on current ModelCriteria
 * @method     ChildStatistics[]|ObjectCollection findByTeamid(int $teamid) Return ChildStatistics objects filtered by the teamid column
 * @method     ChildStatistics[]|ObjectCollection findByKlassecode(string $klassecode) Return ChildStatistics objects filtered by the klassecode column
 * @method     ChildStatistics[]|ObjectCollection findByNaam(string $naam) Return ChildStatistics objects filtered by the naam column
 * @method     ChildStatistics[]|ObjectCollection findByW(int $w) Return ChildStatistics objects filtered by the w column
 * @method     ChildStatistics[]|ObjectCollection findByP(int $p) Return ChildStatistics objects filtered by the p column
 * @method     ChildStatistics[]|ObjectCollection findBySv(int $sv) Return ChildStatistics objects filtered by the sv column
 * @method     ChildStatistics[]|ObjectCollection findBySt(int $st) Return ChildStatistics objects filtered by the st column
 * @method     ChildStatistics[]|ObjectCollection findByS(int $s) Return ChildStatistics objects filtered by the s column
 * @method     ChildStatistics[]|ObjectCollection findByStrp(int $strp) Return ChildStatistics objects filtered by the strp column
 * @method     ChildStatistics[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StatisticsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\StatisticsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\Statistics', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStatisticsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStatisticsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStatisticsQuery) {
            return $criteria;
        }
        $query = new ChildStatisticsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStatistics|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = StatisticsTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StatisticsTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStatistics A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT teamid, klassecode, naam, w, p, sv, st, s, strp FROM statsview WHERE teamid = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStatistics $obj */
            $obj = new ChildStatistics();
            $obj->hydrate($row);
            StatisticsTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStatistics|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the teamid column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamid(1234); // WHERE teamid = 1234
     * $query->filterByTeamid(array(12, 34)); // WHERE teamid IN (12, 34)
     * $query->filterByTeamid(array('min' => 12)); // WHERE teamid > 12
     * </code>
     *
     * @see       filterByTeam()
     *
     * @param     mixed $teamid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByTeamid($teamid = null, $comparison = null)
    {
        if (is_array($teamid)) {
            $useMinMax = false;
            if (isset($teamid['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $teamid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamid['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $teamid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $teamid, $comparison);
    }

    /**
     * Filter the query on the klassecode column
     *
     * Example usage:
     * <code>
     * $query->filterByKlassecode('fooValue');   // WHERE klassecode = 'fooValue'
     * $query->filterByKlassecode('%fooValue%'); // WHERE klassecode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $klassecode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByKlassecode($klassecode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($klassecode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $klassecode)) {
                $klassecode = str_replace('*', '%', $klassecode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_KLASSECODE, $klassecode, $comparison);
    }

    /**
     * Filter the query on the naam column
     *
     * Example usage:
     * <code>
     * $query->filterByNaam('fooValue');   // WHERE naam = 'fooValue'
     * $query->filterByNaam('%fooValue%'); // WHERE naam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $naam The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByNaam($naam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($naam)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $naam)) {
                $naam = str_replace('*', '%', $naam);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_NAAM, $naam, $comparison);
    }

    /**
     * Filter the query on the w column
     *
     * Example usage:
     * <code>
     * $query->filterByW(1234); // WHERE w = 1234
     * $query->filterByW(array(12, 34)); // WHERE w IN (12, 34)
     * $query->filterByW(array('min' => 12)); // WHERE w > 12
     * </code>
     *
     * @param     mixed $w The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByW($w = null, $comparison = null)
    {
        if (is_array($w)) {
            $useMinMax = false;
            if (isset($w['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_W, $w['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($w['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_W, $w['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_W, $w, $comparison);
    }

    /**
     * Filter the query on the p column
     *
     * Example usage:
     * <code>
     * $query->filterByP(1234); // WHERE p = 1234
     * $query->filterByP(array(12, 34)); // WHERE p IN (12, 34)
     * $query->filterByP(array('min' => 12)); // WHERE p > 12
     * </code>
     *
     * @param     mixed $p The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByP($p = null, $comparison = null)
    {
        if (is_array($p)) {
            $useMinMax = false;
            if (isset($p['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_P, $p['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($p['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_P, $p['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_P, $p, $comparison);
    }

    /**
     * Filter the query on the sv column
     *
     * Example usage:
     * <code>
     * $query->filterBySv(1234); // WHERE sv = 1234
     * $query->filterBySv(array(12, 34)); // WHERE sv IN (12, 34)
     * $query->filterBySv(array('min' => 12)); // WHERE sv > 12
     * </code>
     *
     * @param     mixed $sv The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterBySv($sv = null, $comparison = null)
    {
        if (is_array($sv)) {
            $useMinMax = false;
            if (isset($sv['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_SV, $sv['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sv['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_SV, $sv['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_SV, $sv, $comparison);
    }

    /**
     * Filter the query on the st column
     *
     * Example usage:
     * <code>
     * $query->filterBySt(1234); // WHERE st = 1234
     * $query->filterBySt(array(12, 34)); // WHERE st IN (12, 34)
     * $query->filterBySt(array('min' => 12)); // WHERE st > 12
     * </code>
     *
     * @param     mixed $st The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterBySt($st = null, $comparison = null)
    {
        if (is_array($st)) {
            $useMinMax = false;
            if (isset($st['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_ST, $st['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($st['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_ST, $st['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_ST, $st, $comparison);
    }

    /**
     * Filter the query on the s column
     *
     * Example usage:
     * <code>
     * $query->filterByS(1234); // WHERE s = 1234
     * $query->filterByS(array(12, 34)); // WHERE s IN (12, 34)
     * $query->filterByS(array('min' => 12)); // WHERE s > 12
     * </code>
     *
     * @param     mixed $s The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByS($s = null, $comparison = null)
    {
        if (is_array($s)) {
            $useMinMax = false;
            if (isset($s['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_S, $s['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($s['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_S, $s['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_S, $s, $comparison);
    }

    /**
     * Filter the query on the strp column
     *
     * Example usage:
     * <code>
     * $query->filterByStrp(1234); // WHERE strp = 1234
     * $query->filterByStrp(array(12, 34)); // WHERE strp IN (12, 34)
     * $query->filterByStrp(array('min' => 12)); // WHERE strp > 12
     * </code>
     *
     * @param     mixed $strp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByStrp($strp = null, $comparison = null)
    {
        if (is_array($strp)) {
            $useMinMax = false;
            if (isset($strp['min'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_STRP, $strp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($strp['max'])) {
                $this->addUsingAlias(StatisticsTableMap::COL_STRP, $strp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatisticsTableMap::COL_STRP, $strp, $comparison);
    }

    /**
     * Filter the query by a related \Data\Team object
     *
     * @param \Data\Team|ObjectCollection $team The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStatisticsQuery The current query, for fluid interface
     */
    public function filterByTeam($team, $comparison = null)
    {
        if ($team instanceof \Data\Team) {
            return $this
                ->addUsingAlias(StatisticsTableMap::COL_TEAMID, $team->getId(), $comparison);
        } elseif ($team instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StatisticsTableMap::COL_TEAMID, $team->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTeam() only accepts arguments of type \Data\Team or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Team relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function joinTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Team');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Team');
        }

        return $this;
    }

    /**
     * Use the Team relation Team object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamQuery A secondary query class using the current class as primary query
     */
    public function useTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Team', '\Data\TeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStatistics $statistics Object to remove from the list of results
     *
     * @return $this|ChildStatisticsQuery The current query, for fluid interface
     */
    public function prune($statistics = null)
    {
        if ($statistics) {
            $this->addUsingAlias(StatisticsTableMap::COL_TEAMID, $statistics->getTeamid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

} // StatisticsQuery
