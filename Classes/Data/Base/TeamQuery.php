<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\Team as ChildTeam;
use Data\TeamQuery as ChildTeamQuery;
use Data\Map\TeamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'team' table.
 *
 * 
 *
 * @method     ChildTeamQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTeamQuery orderByKlassecode($order = Criteria::ASC) Order by the klassecode column
 * @method     ChildTeamQuery orderByNaam($order = Criteria::ASC) Order by the naam column
 *
 * @method     ChildTeamQuery groupById() Group by the id column
 * @method     ChildTeamQuery groupByKlassecode() Group by the klassecode column
 * @method     ChildTeamQuery groupByNaam() Group by the naam column
 *
 * @method     ChildTeamQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTeamQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTeamQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTeamQuery leftJoinKlas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Klas relation
 * @method     ChildTeamQuery rightJoinKlas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Klas relation
 * @method     ChildTeamQuery innerJoinKlas($relationAlias = null) Adds a INNER JOIN clause to the query using the Klas relation
 *
 * @method     ChildTeamQuery leftJoinTeamHasLid($relationAlias = null) Adds a LEFT JOIN clause to the query using the TeamHasLid relation
 * @method     ChildTeamQuery rightJoinTeamHasLid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TeamHasLid relation
 * @method     ChildTeamQuery innerJoinTeamHasLid($relationAlias = null) Adds a INNER JOIN clause to the query using the TeamHasLid relation
 *
 * @method     ChildTeamQuery leftJoinTeamwedstrijd($relationAlias = null) Adds a LEFT JOIN clause to the query using the Teamwedstrijd relation
 * @method     ChildTeamQuery rightJoinTeamwedstrijd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Teamwedstrijd relation
 * @method     ChildTeamQuery innerJoinTeamwedstrijd($relationAlias = null) Adds a INNER JOIN clause to the query using the Teamwedstrijd relation
 *
 * @method     ChildTeamQuery leftJoinStatistics($relationAlias = null) Adds a LEFT JOIN clause to the query using the Statistics relation
 * @method     ChildTeamQuery rightJoinStatistics($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Statistics relation
 * @method     ChildTeamQuery innerJoinStatistics($relationAlias = null) Adds a INNER JOIN clause to the query using the Statistics relation
 *
 * @method     ChildTeamQuery leftJoinWTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the WTeam relation
 * @method     ChildTeamQuery rightJoinWTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WTeam relation
 * @method     ChildTeamQuery innerJoinWTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the WTeam relation
 *
 * @method     \Data\KlasQuery|\Data\TeamHasLidQuery|\Data\TeamwedstrijdQuery|\Data\StatisticsQuery|\Data\WTeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTeam findOne(ConnectionInterface $con = null) Return the first ChildTeam matching the query
 * @method     ChildTeam findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTeam matching the query, or a new ChildTeam object populated from the query conditions when no match is found
 *
 * @method     ChildTeam findOneById(int $id) Return the first ChildTeam filtered by the id column
 * @method     ChildTeam findOneByKlassecode(string $klassecode) Return the first ChildTeam filtered by the klassecode column
 * @method     ChildTeam findOneByNaam(string $naam) Return the first ChildTeam filtered by the naam column *

 * @method     ChildTeam requirePk($key, ConnectionInterface $con = null) Return the ChildTeam by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeam requireOne(ConnectionInterface $con = null) Return the first ChildTeam matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeam requireOneById(int $id) Return the first ChildTeam filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeam requireOneByKlassecode(string $klassecode) Return the first ChildTeam filtered by the klassecode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeam requireOneByNaam(string $naam) Return the first ChildTeam filtered by the naam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeam[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTeam objects based on current ModelCriteria
 * @method     ChildTeam[]|ObjectCollection findById(int $id) Return ChildTeam objects filtered by the id column
 * @method     ChildTeam[]|ObjectCollection findByKlassecode(string $klassecode) Return ChildTeam objects filtered by the klassecode column
 * @method     ChildTeam[]|ObjectCollection findByNaam(string $naam) Return ChildTeam objects filtered by the naam column
 * @method     ChildTeam[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TeamQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\TeamQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\Team', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTeamQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTeamQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTeamQuery) {
            return $criteria;
        }
        $query = new ChildTeamQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTeam|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TeamTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TeamTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeam A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, klassecode, naam FROM team WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTeam $obj */
            $obj = new ChildTeam();
            $obj->hydrate($row);
            TeamTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTeam|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TeamTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TeamTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TeamTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TeamTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the klassecode column
     *
     * Example usage:
     * <code>
     * $query->filterByKlassecode('fooValue');   // WHERE klassecode = 'fooValue'
     * $query->filterByKlassecode('%fooValue%'); // WHERE klassecode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $klassecode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function filterByKlassecode($klassecode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($klassecode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $klassecode)) {
                $klassecode = str_replace('*', '%', $klassecode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TeamTableMap::COL_KLASSECODE, $klassecode, $comparison);
    }

    /**
     * Filter the query on the naam column
     *
     * Example usage:
     * <code>
     * $query->filterByNaam('fooValue');   // WHERE naam = 'fooValue'
     * $query->filterByNaam('%fooValue%'); // WHERE naam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $naam The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function filterByNaam($naam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($naam)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $naam)) {
                $naam = str_replace('*', '%', $naam);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TeamTableMap::COL_NAAM, $naam, $comparison);
    }

    /**
     * Filter the query by a related \Data\Klas object
     *
     * @param \Data\Klas|ObjectCollection $klas The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByKlas($klas, $comparison = null)
    {
        if ($klas instanceof \Data\Klas) {
            return $this
                ->addUsingAlias(TeamTableMap::COL_KLASSECODE, $klas->getCode(), $comparison);
        } elseif ($klas instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TeamTableMap::COL_KLASSECODE, $klas->toKeyValue('PrimaryKey', 'Code'), $comparison);
        } else {
            throw new PropelException('filterByKlas() only accepts arguments of type \Data\Klas or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Klas relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function joinKlas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Klas');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Klas');
        }

        return $this;
    }

    /**
     * Use the Klas relation Klas object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\KlasQuery A secondary query class using the current class as primary query
     */
    public function useKlasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinKlas($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Klas', '\Data\KlasQuery');
    }

    /**
     * Filter the query by a related \Data\TeamHasLid object
     *
     * @param \Data\TeamHasLid|ObjectCollection $teamHasLid the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByTeamHasLid($teamHasLid, $comparison = null)
    {
        if ($teamHasLid instanceof \Data\TeamHasLid) {
            return $this
                ->addUsingAlias(TeamTableMap::COL_ID, $teamHasLid->getTeamid(), $comparison);
        } elseif ($teamHasLid instanceof ObjectCollection) {
            return $this
                ->useTeamHasLidQuery()
                ->filterByPrimaryKeys($teamHasLid->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTeamHasLid() only accepts arguments of type \Data\TeamHasLid or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TeamHasLid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function joinTeamHasLid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TeamHasLid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TeamHasLid');
        }

        return $this;
    }

    /**
     * Use the TeamHasLid relation TeamHasLid object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamHasLidQuery A secondary query class using the current class as primary query
     */
    public function useTeamHasLidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTeamHasLid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TeamHasLid', '\Data\TeamHasLidQuery');
    }

    /**
     * Filter the query by a related \Data\Teamwedstrijd object
     *
     * @param \Data\Teamwedstrijd|ObjectCollection $teamwedstrijd the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByTeamwedstrijd($teamwedstrijd, $comparison = null)
    {
        if ($teamwedstrijd instanceof \Data\Teamwedstrijd) {
            return $this
                ->addUsingAlias(TeamTableMap::COL_ID, $teamwedstrijd->getTeamid(), $comparison);
        } elseif ($teamwedstrijd instanceof ObjectCollection) {
            return $this
                ->useTeamwedstrijdQuery()
                ->filterByPrimaryKeys($teamwedstrijd->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTeamwedstrijd() only accepts arguments of type \Data\Teamwedstrijd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Teamwedstrijd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function joinTeamwedstrijd($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Teamwedstrijd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Teamwedstrijd');
        }

        return $this;
    }

    /**
     * Use the Teamwedstrijd relation Teamwedstrijd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamwedstrijdQuery A secondary query class using the current class as primary query
     */
    public function useTeamwedstrijdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTeamwedstrijd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Teamwedstrijd', '\Data\TeamwedstrijdQuery');
    }

    /**
     * Filter the query by a related \Data\Statistics object
     *
     * @param \Data\Statistics|ObjectCollection $statistics the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByStatistics($statistics, $comparison = null)
    {
        if ($statistics instanceof \Data\Statistics) {
            return $this
                ->addUsingAlias(TeamTableMap::COL_ID, $statistics->getTeamid(), $comparison);
        } elseif ($statistics instanceof ObjectCollection) {
            return $this
                ->useStatisticsQuery()
                ->filterByPrimaryKeys($statistics->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStatistics() only accepts arguments of type \Data\Statistics or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Statistics relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function joinStatistics($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Statistics');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Statistics');
        }

        return $this;
    }

    /**
     * Use the Statistics relation Statistics object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\StatisticsQuery A secondary query class using the current class as primary query
     */
    public function useStatisticsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatistics($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Statistics', '\Data\StatisticsQuery');
    }

    /**
     * Filter the query by a related \Data\WTeam object
     *
     * @param \Data\WTeam|ObjectCollection $wTeam the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByWTeam($wTeam, $comparison = null)
    {
        if ($wTeam instanceof \Data\WTeam) {
            return $this
                ->addUsingAlias(TeamTableMap::COL_ID, $wTeam->getTeamid(), $comparison);
        } elseif ($wTeam instanceof ObjectCollection) {
            return $this
                ->useWTeamQuery()
                ->filterByPrimaryKeys($wTeam->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWTeam() only accepts arguments of type \Data\WTeam or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WTeam relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function joinWTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WTeam');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WTeam');
        }

        return $this;
    }

    /**
     * Use the WTeam relation WTeam object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\WTeamQuery A secondary query class using the current class as primary query
     */
    public function useWTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WTeam', '\Data\WTeamQuery');
    }

    /**
     * Filter the query by a related Lid object
     * using the team_has_lid table as cross reference
     *
     * @param Lid $lid the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTeamQuery The current query, for fluid interface
     */
    public function filterByLid($lid, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useTeamHasLidQuery()
            ->filterByLid($lid, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTeam $team Object to remove from the list of results
     *
     * @return $this|ChildTeamQuery The current query, for fluid interface
     */
    public function prune($team = null)
    {
        if ($team) {
            $this->addUsingAlias(TeamTableMap::COL_ID, $team->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the team table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TeamTableMap::clearInstancePool();
            TeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TeamTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            TeamTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            TeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TeamQuery
