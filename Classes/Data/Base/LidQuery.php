<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\Lid as ChildLid;
use Data\LidQuery as ChildLidQuery;
use Data\Map\LidTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'lid' table.
 *
 * 
 *
 * @method     ChildLidQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildLidQuery orderByNaam($order = Criteria::ASC) Order by the naam column
 *
 * @method     ChildLidQuery groupById() Group by the id column
 * @method     ChildLidQuery groupByNaam() Group by the naam column
 *
 * @method     ChildLidQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLidQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLidQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLidQuery leftJoinTeamHasLid($relationAlias = null) Adds a LEFT JOIN clause to the query using the TeamHasLid relation
 * @method     ChildLidQuery rightJoinTeamHasLid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TeamHasLid relation
 * @method     ChildLidQuery innerJoinTeamHasLid($relationAlias = null) Adds a INNER JOIN clause to the query using the TeamHasLid relation
 *
 * @method     \Data\TeamHasLidQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLid findOne(ConnectionInterface $con = null) Return the first ChildLid matching the query
 * @method     ChildLid findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLid matching the query, or a new ChildLid object populated from the query conditions when no match is found
 *
 * @method     ChildLid findOneById(int $id) Return the first ChildLid filtered by the id column
 * @method     ChildLid findOneByNaam(string $naam) Return the first ChildLid filtered by the naam column *

 * @method     ChildLid requirePk($key, ConnectionInterface $con = null) Return the ChildLid by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLid requireOne(ConnectionInterface $con = null) Return the first ChildLid matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLid requireOneById(int $id) Return the first ChildLid filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLid requireOneByNaam(string $naam) Return the first ChildLid filtered by the naam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLid[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLid objects based on current ModelCriteria
 * @method     ChildLid[]|ObjectCollection findById(int $id) Return ChildLid objects filtered by the id column
 * @method     ChildLid[]|ObjectCollection findByNaam(string $naam) Return ChildLid objects filtered by the naam column
 * @method     ChildLid[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LidQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\LidQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\Lid', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLidQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLidQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLidQuery) {
            return $criteria;
        }
        $query = new ChildLidQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLid|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LidTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LidTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLid A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, naam FROM lid WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLid $obj */
            $obj = new ChildLid();
            $obj->hydrate($row);
            LidTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLid|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LidTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LidTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LidTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LidTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LidTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the naam column
     *
     * Example usage:
     * <code>
     * $query->filterByNaam('fooValue');   // WHERE naam = 'fooValue'
     * $query->filterByNaam('%fooValue%'); // WHERE naam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $naam The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function filterByNaam($naam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($naam)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $naam)) {
                $naam = str_replace('*', '%', $naam);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LidTableMap::COL_NAAM, $naam, $comparison);
    }

    /**
     * Filter the query by a related \Data\TeamHasLid object
     *
     * @param \Data\TeamHasLid|ObjectCollection $teamHasLid the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLidQuery The current query, for fluid interface
     */
    public function filterByTeamHasLid($teamHasLid, $comparison = null)
    {
        if ($teamHasLid instanceof \Data\TeamHasLid) {
            return $this
                ->addUsingAlias(LidTableMap::COL_ID, $teamHasLid->getLidid(), $comparison);
        } elseif ($teamHasLid instanceof ObjectCollection) {
            return $this
                ->useTeamHasLidQuery()
                ->filterByPrimaryKeys($teamHasLid->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTeamHasLid() only accepts arguments of type \Data\TeamHasLid or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TeamHasLid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function joinTeamHasLid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TeamHasLid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TeamHasLid');
        }

        return $this;
    }

    /**
     * Use the TeamHasLid relation TeamHasLid object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamHasLidQuery A secondary query class using the current class as primary query
     */
    public function useTeamHasLidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTeamHasLid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TeamHasLid', '\Data\TeamHasLidQuery');
    }

    /**
     * Filter the query by a related Team object
     * using the team_has_lid table as cross reference
     *
     * @param Team $team the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLidQuery The current query, for fluid interface
     */
    public function filterByTeam($team, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useTeamHasLidQuery()
            ->filterByTeam($team, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLid $lid Object to remove from the list of results
     *
     * @return $this|ChildLidQuery The current query, for fluid interface
     */
    public function prune($lid = null)
    {
        if ($lid) {
            $this->addUsingAlias(LidTableMap::COL_ID, $lid->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lid table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LidTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LidTableMap::clearInstancePool();
            LidTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LidTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LidTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            LidTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            LidTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LidQuery
