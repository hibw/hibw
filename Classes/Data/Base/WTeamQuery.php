<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\WTeam as ChildWTeam;
use Data\WTeamQuery as ChildWTeamQuery;
use Data\Map\WTeamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'w_team' table.
 *
 * 
 *
 * @method     ChildWTeamQuery orderByWedstrijdid($order = Criteria::ASC) Order by the wedstrijdid column
 * @method     ChildWTeamQuery orderByTeamid($order = Criteria::ASC) Order by the teamid column
 * @method     ChildWTeamQuery orderByRol($order = Criteria::ASC) Order by the rol column
 * @method     ChildWTeamQuery orderByStrafpunten($order = Criteria::ASC) Order by the strafpunten column
 *
 * @method     ChildWTeamQuery groupByWedstrijdid() Group by the wedstrijdid column
 * @method     ChildWTeamQuery groupByTeamid() Group by the teamid column
 * @method     ChildWTeamQuery groupByRol() Group by the rol column
 * @method     ChildWTeamQuery groupByStrafpunten() Group by the strafpunten column
 *
 * @method     ChildWTeamQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWTeamQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWTeamQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWTeamQuery leftJoinWedstrijd($relationAlias = null) Adds a LEFT JOIN clause to the query using the Wedstrijd relation
 * @method     ChildWTeamQuery rightJoinWedstrijd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Wedstrijd relation
 * @method     ChildWTeamQuery innerJoinWedstrijd($relationAlias = null) Adds a INNER JOIN clause to the query using the Wedstrijd relation
 *
 * @method     ChildWTeamQuery leftJoinTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the Team relation
 * @method     ChildWTeamQuery rightJoinTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Team relation
 * @method     ChildWTeamQuery innerJoinTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the Team relation
 *
 * @method     ChildWTeamQuery leftJoinUitslagSetTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the UitslagSetTeam relation
 * @method     ChildWTeamQuery rightJoinUitslagSetTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UitslagSetTeam relation
 * @method     ChildWTeamQuery innerJoinUitslagSetTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the UitslagSetTeam relation
 *
 * @method     \Data\WedstrijdQuery|\Data\TeamQuery|\Data\UitslagSetTeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWTeam findOne(ConnectionInterface $con = null) Return the first ChildWTeam matching the query
 * @method     ChildWTeam findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWTeam matching the query, or a new ChildWTeam object populated from the query conditions when no match is found
 *
 * @method     ChildWTeam findOneByWedstrijdid(int $wedstrijdid) Return the first ChildWTeam filtered by the wedstrijdid column
 * @method     ChildWTeam findOneByTeamid(int $teamid) Return the first ChildWTeam filtered by the teamid column
 * @method     ChildWTeam findOneByRol(int $rol) Return the first ChildWTeam filtered by the rol column
 * @method     ChildWTeam findOneByStrafpunten(int $strafpunten) Return the first ChildWTeam filtered by the strafpunten column *

 * @method     ChildWTeam requirePk($key, ConnectionInterface $con = null) Return the ChildWTeam by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWTeam requireOne(ConnectionInterface $con = null) Return the first ChildWTeam matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWTeam requireOneByWedstrijdid(int $wedstrijdid) Return the first ChildWTeam filtered by the wedstrijdid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWTeam requireOneByTeamid(int $teamid) Return the first ChildWTeam filtered by the teamid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWTeam requireOneByRol(int $rol) Return the first ChildWTeam filtered by the rol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWTeam requireOneByStrafpunten(int $strafpunten) Return the first ChildWTeam filtered by the strafpunten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWTeam[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWTeam objects based on current ModelCriteria
 * @method     ChildWTeam[]|ObjectCollection findByWedstrijdid(int $wedstrijdid) Return ChildWTeam objects filtered by the wedstrijdid column
 * @method     ChildWTeam[]|ObjectCollection findByTeamid(int $teamid) Return ChildWTeam objects filtered by the teamid column
 * @method     ChildWTeam[]|ObjectCollection findByRol(int $rol) Return ChildWTeam objects filtered by the rol column
 * @method     ChildWTeam[]|ObjectCollection findByStrafpunten(int $strafpunten) Return ChildWTeam objects filtered by the strafpunten column
 * @method     ChildWTeam[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WTeamQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\WTeamQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\WTeam', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWTeamQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWTeamQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWTeamQuery) {
            return $criteria;
        }
        $query = new ChildWTeamQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$wedstrijdid, $teamid] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWTeam|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WTeamTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WTeamTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWTeam A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT wedstrijdid, teamid, rol, strafpunten FROM w_team WHERE wedstrijdid = :p0 AND teamid = :p1';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);            
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWTeam $obj */
            $obj = new ChildWTeam();
            $obj->hydrate($row);
            WTeamTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWTeam|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(WTeamTableMap::COL_TEAMID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(WTeamTableMap::COL_WEDSTRIJDID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(WTeamTableMap::COL_TEAMID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the wedstrijdid column
     *
     * Example usage:
     * <code>
     * $query->filterByWedstrijdid(1234); // WHERE wedstrijdid = 1234
     * $query->filterByWedstrijdid(array(12, 34)); // WHERE wedstrijdid IN (12, 34)
     * $query->filterByWedstrijdid(array('min' => 12)); // WHERE wedstrijdid > 12
     * </code>
     *
     * @see       filterByWedstrijd()
     *
     * @param     mixed $wedstrijdid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByWedstrijdid($wedstrijdid = null, $comparison = null)
    {
        if (is_array($wedstrijdid)) {
            $useMinMax = false;
            if (isset($wedstrijdid['min'])) {
                $this->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wedstrijdid['max'])) {
                $this->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $wedstrijdid, $comparison);
    }

    /**
     * Filter the query on the teamid column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamid(1234); // WHERE teamid = 1234
     * $query->filterByTeamid(array(12, 34)); // WHERE teamid IN (12, 34)
     * $query->filterByTeamid(array('min' => 12)); // WHERE teamid > 12
     * </code>
     *
     * @see       filterByTeam()
     *
     * @param     mixed $teamid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByTeamid($teamid = null, $comparison = null)
    {
        if (is_array($teamid)) {
            $useMinMax = false;
            if (isset($teamid['min'])) {
                $this->addUsingAlias(WTeamTableMap::COL_TEAMID, $teamid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamid['max'])) {
                $this->addUsingAlias(WTeamTableMap::COL_TEAMID, $teamid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WTeamTableMap::COL_TEAMID, $teamid, $comparison);
    }

    /**
     * Filter the query on the rol column
     *
     * Example usage:
     * <code>
     * $query->filterByRol(1234); // WHERE rol = 1234
     * $query->filterByRol(array(12, 34)); // WHERE rol IN (12, 34)
     * $query->filterByRol(array('min' => 12)); // WHERE rol > 12
     * </code>
     *
     * @param     mixed $rol The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByRol($rol = null, $comparison = null)
    {
        if (is_array($rol)) {
            $useMinMax = false;
            if (isset($rol['min'])) {
                $this->addUsingAlias(WTeamTableMap::COL_ROL, $rol['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rol['max'])) {
                $this->addUsingAlias(WTeamTableMap::COL_ROL, $rol['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WTeamTableMap::COL_ROL, $rol, $comparison);
    }

    /**
     * Filter the query on the strafpunten column
     *
     * Example usage:
     * <code>
     * $query->filterByStrafpunten(1234); // WHERE strafpunten = 1234
     * $query->filterByStrafpunten(array(12, 34)); // WHERE strafpunten IN (12, 34)
     * $query->filterByStrafpunten(array('min' => 12)); // WHERE strafpunten > 12
     * </code>
     *
     * @param     mixed $strafpunten The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByStrafpunten($strafpunten = null, $comparison = null)
    {
        if (is_array($strafpunten)) {
            $useMinMax = false;
            if (isset($strafpunten['min'])) {
                $this->addUsingAlias(WTeamTableMap::COL_STRAFPUNTEN, $strafpunten['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($strafpunten['max'])) {
                $this->addUsingAlias(WTeamTableMap::COL_STRAFPUNTEN, $strafpunten['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WTeamTableMap::COL_STRAFPUNTEN, $strafpunten, $comparison);
    }

    /**
     * Filter the query by a related \Data\Wedstrijd object
     *
     * @param \Data\Wedstrijd|ObjectCollection $wedstrijd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByWedstrijd($wedstrijd, $comparison = null)
    {
        if ($wedstrijd instanceof \Data\Wedstrijd) {
            return $this
                ->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $wedstrijd->getId(), $comparison);
        } elseif ($wedstrijd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $wedstrijd->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByWedstrijd() only accepts arguments of type \Data\Wedstrijd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Wedstrijd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function joinWedstrijd($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Wedstrijd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Wedstrijd');
        }

        return $this;
    }

    /**
     * Use the Wedstrijd relation Wedstrijd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\WedstrijdQuery A secondary query class using the current class as primary query
     */
    public function useWedstrijdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWedstrijd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Wedstrijd', '\Data\WedstrijdQuery');
    }

    /**
     * Filter the query by a related \Data\Team object
     *
     * @param \Data\Team|ObjectCollection $team The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByTeam($team, $comparison = null)
    {
        if ($team instanceof \Data\Team) {
            return $this
                ->addUsingAlias(WTeamTableMap::COL_TEAMID, $team->getId(), $comparison);
        } elseif ($team instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WTeamTableMap::COL_TEAMID, $team->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTeam() only accepts arguments of type \Data\Team or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Team relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function joinTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Team');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Team');
        }

        return $this;
    }

    /**
     * Use the Team relation Team object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamQuery A secondary query class using the current class as primary query
     */
    public function useTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Team', '\Data\TeamQuery');
    }

    /**
     * Filter the query by a related \Data\UitslagSetTeam object
     *
     * @param \Data\UitslagSetTeam|ObjectCollection $uitslagSetTeam the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildWTeamQuery The current query, for fluid interface
     */
    public function filterByUitslagSetTeam($uitslagSetTeam, $comparison = null)
    {
        if ($uitslagSetTeam instanceof \Data\UitslagSetTeam) {
            return $this
                ->addUsingAlias(WTeamTableMap::COL_WEDSTRIJDID, $uitslagSetTeam->getWedstrijdid(), $comparison)
                ->addUsingAlias(WTeamTableMap::COL_TEAMID, $uitslagSetTeam->getTeamid(), $comparison);
        } else {
            throw new PropelException('filterByUitslagSetTeam() only accepts arguments of type \Data\UitslagSetTeam');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UitslagSetTeam relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function joinUitslagSetTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UitslagSetTeam');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UitslagSetTeam');
        }

        return $this;
    }

    /**
     * Use the UitslagSetTeam relation UitslagSetTeam object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\UitslagSetTeamQuery A secondary query class using the current class as primary query
     */
    public function useUitslagSetTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUitslagSetTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UitslagSetTeam', '\Data\UitslagSetTeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWTeam $wTeam Object to remove from the list of results
     *
     * @return $this|ChildWTeamQuery The current query, for fluid interface
     */
    public function prune($wTeam = null)
    {
        if ($wTeam) {
            $this->addCond('pruneCond0', $this->getAliasedColName(WTeamTableMap::COL_WEDSTRIJDID), $wTeam->getWedstrijdid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(WTeamTableMap::COL_TEAMID), $wTeam->getTeamid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the w_team table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WTeamTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WTeamTableMap::clearInstancePool();
            WTeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WTeamTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WTeamTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            WTeamTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            WTeamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // WTeamQuery
