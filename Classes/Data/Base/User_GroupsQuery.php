<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\User_Groups as ChildUser_Groups;
use Data\User_GroupsQuery as ChildUser_GroupsQuery;
use Data\Map\User_GroupsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_groups' table.
 *
 * 
 *
 * @method     ChildUser_GroupsQuery orderByGroupid($order = Criteria::ASC) Order by the id column
 * @method     ChildUser_GroupsQuery orderByGroupName($order = Criteria::ASC) Order by the naam column
 * @method     ChildUser_GroupsQuery orderByGroupNiveau($order = Criteria::ASC) Order by the niveau column
 *
 * @method     ChildUser_GroupsQuery groupByGroupid() Group by the id column
 * @method     ChildUser_GroupsQuery groupByGroupName() Group by the naam column
 * @method     ChildUser_GroupsQuery groupByGroupNiveau() Group by the niveau column
 *
 * @method     ChildUser_GroupsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUser_GroupsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUser_GroupsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUser_GroupsQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildUser_GroupsQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildUser_GroupsQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     \Data\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUser_Groups findOne(ConnectionInterface $con = null) Return the first ChildUser_Groups matching the query
 * @method     ChildUser_Groups findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUser_Groups matching the query, or a new ChildUser_Groups object populated from the query conditions when no match is found
 *
 * @method     ChildUser_Groups findOneByGroupid(int $id) Return the first ChildUser_Groups filtered by the id column
 * @method     ChildUser_Groups findOneByGroupName(string $naam) Return the first ChildUser_Groups filtered by the naam column
 * @method     ChildUser_Groups findOneByGroupNiveau(int $niveau) Return the first ChildUser_Groups filtered by the niveau column *

 * @method     ChildUser_Groups requirePk($key, ConnectionInterface $con = null) Return the ChildUser_Groups by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser_Groups requireOne(ConnectionInterface $con = null) Return the first ChildUser_Groups matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser_Groups requireOneByGroupid(int $id) Return the first ChildUser_Groups filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser_Groups requireOneByGroupName(string $naam) Return the first ChildUser_Groups filtered by the naam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser_Groups requireOneByGroupNiveau(int $niveau) Return the first ChildUser_Groups filtered by the niveau column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser_Groups[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUser_Groups objects based on current ModelCriteria
 * @method     ChildUser_Groups[]|ObjectCollection findByGroupid(int $id) Return ChildUser_Groups objects filtered by the id column
 * @method     ChildUser_Groups[]|ObjectCollection findByGroupName(string $naam) Return ChildUser_Groups objects filtered by the naam column
 * @method     ChildUser_Groups[]|ObjectCollection findByGroupNiveau(int $niveau) Return ChildUser_Groups objects filtered by the niveau column
 * @method     ChildUser_Groups[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class User_GroupsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\User_GroupsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\User_Groups', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUser_GroupsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUser_GroupsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUser_GroupsQuery) {
            return $criteria;
        }
        $query = new ChildUser_GroupsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUser_Groups|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = User_GroupsTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(User_GroupsTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUser_Groups A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, naam, niveau FROM user_groups WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUser_Groups $obj */
            $obj = new ChildUser_Groups();
            $obj->hydrate($row);
            User_GroupsTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUser_Groups|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(User_GroupsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(User_GroupsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupid(1234); // WHERE id = 1234
     * $query->filterByGroupid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByGroupid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $groupid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByGroupid($groupid = null, $comparison = null)
    {
        if (is_array($groupid)) {
            $useMinMax = false;
            if (isset($groupid['min'])) {
                $this->addUsingAlias(User_GroupsTableMap::COL_ID, $groupid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($groupid['max'])) {
                $this->addUsingAlias(User_GroupsTableMap::COL_ID, $groupid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(User_GroupsTableMap::COL_ID, $groupid, $comparison);
    }

    /**
     * Filter the query on the naam column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupName('fooValue');   // WHERE naam = 'fooValue'
     * $query->filterByGroupName('%fooValue%'); // WHERE naam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $groupName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByGroupName($groupName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($groupName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $groupName)) {
                $groupName = str_replace('*', '%', $groupName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(User_GroupsTableMap::COL_NAAM, $groupName, $comparison);
    }

    /**
     * Filter the query on the niveau column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupNiveau(1234); // WHERE niveau = 1234
     * $query->filterByGroupNiveau(array(12, 34)); // WHERE niveau IN (12, 34)
     * $query->filterByGroupNiveau(array('min' => 12)); // WHERE niveau > 12
     * </code>
     *
     * @param     mixed $groupNiveau The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByGroupNiveau($groupNiveau = null, $comparison = null)
    {
        if (is_array($groupNiveau)) {
            $useMinMax = false;
            if (isset($groupNiveau['min'])) {
                $this->addUsingAlias(User_GroupsTableMap::COL_NIVEAU, $groupNiveau['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($groupNiveau['max'])) {
                $this->addUsingAlias(User_GroupsTableMap::COL_NIVEAU, $groupNiveau['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(User_GroupsTableMap::COL_NIVEAU, $groupNiveau, $comparison);
    }

    /**
     * Filter the query by a related \Data\User object
     *
     * @param \Data\User|ObjectCollection $user the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Data\User) {
            return $this
                ->addUsingAlias(User_GroupsTableMap::COL_ID, $user->getGroupid(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            return $this
                ->useUserQuery()
                ->filterByPrimaryKeys($user->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Data\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Data\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUser_Groups $user_Groups Object to remove from the list of results
     *
     * @return $this|ChildUser_GroupsQuery The current query, for fluid interface
     */
    public function prune($user_Groups = null)
    {
        if ($user_Groups) {
            $this->addUsingAlias(User_GroupsTableMap::COL_ID, $user_Groups->getGroupid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_groups table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(User_GroupsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            User_GroupsTableMap::clearInstancePool();
            User_GroupsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(User_GroupsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(User_GroupsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            User_GroupsTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            User_GroupsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // User_GroupsQuery
