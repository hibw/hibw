<?php

namespace Data\Base;

use \Exception;
use Data\Teamwedstrijd as ChildTeamwedstrijd;
use Data\TeamwedstrijdQuery as ChildTeamwedstrijdQuery;
use Data\Map\TeamwedstrijdTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'teamwedstrijdview' table.
 *
 * 
 *
 * @method     ChildTeamwedstrijdQuery orderByTeamid($order = Criteria::ASC) Order by the teamid column
 * @method     ChildTeamwedstrijdQuery orderByDatum($order = Criteria::ASC) Order by the datum column
 * @method     ChildTeamwedstrijdQuery orderByTijd($order = Criteria::ASC) Order by the tijd column
 * @method     ChildTeamwedstrijdQuery orderByVeld($order = Criteria::ASC) Order by the veld column
 * @method     ChildTeamwedstrijdQuery orderByNaam($order = Criteria::ASC) Order by the naam column
 *
 * @method     ChildTeamwedstrijdQuery groupByTeamid() Group by the teamid column
 * @method     ChildTeamwedstrijdQuery groupByDatum() Group by the datum column
 * @method     ChildTeamwedstrijdQuery groupByTijd() Group by the tijd column
 * @method     ChildTeamwedstrijdQuery groupByVeld() Group by the veld column
 * @method     ChildTeamwedstrijdQuery groupByNaam() Group by the naam column
 *
 * @method     ChildTeamwedstrijdQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTeamwedstrijdQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTeamwedstrijdQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTeamwedstrijdQuery leftJoinTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the Team relation
 * @method     ChildTeamwedstrijdQuery rightJoinTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Team relation
 * @method     ChildTeamwedstrijdQuery innerJoinTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the Team relation
 *
 * @method     \Data\TeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTeamwedstrijd findOne(ConnectionInterface $con = null) Return the first ChildTeamwedstrijd matching the query
 * @method     ChildTeamwedstrijd findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTeamwedstrijd matching the query, or a new ChildTeamwedstrijd object populated from the query conditions when no match is found
 *
 * @method     ChildTeamwedstrijd findOneByTeamid(int $teamid) Return the first ChildTeamwedstrijd filtered by the teamid column
 * @method     ChildTeamwedstrijd findOneByDatum(string $datum) Return the first ChildTeamwedstrijd filtered by the datum column
 * @method     ChildTeamwedstrijd findOneByTijd(string $tijd) Return the first ChildTeamwedstrijd filtered by the tijd column
 * @method     ChildTeamwedstrijd findOneByVeld(string $veld) Return the first ChildTeamwedstrijd filtered by the veld column
 * @method     ChildTeamwedstrijd findOneByNaam(string $naam) Return the first ChildTeamwedstrijd filtered by the naam column *

 * @method     ChildTeamwedstrijd requirePk($key, ConnectionInterface $con = null) Return the ChildTeamwedstrijd by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamwedstrijd requireOne(ConnectionInterface $con = null) Return the first ChildTeamwedstrijd matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeamwedstrijd requireOneByTeamid(int $teamid) Return the first ChildTeamwedstrijd filtered by the teamid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamwedstrijd requireOneByDatum(string $datum) Return the first ChildTeamwedstrijd filtered by the datum column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamwedstrijd requireOneByTijd(string $tijd) Return the first ChildTeamwedstrijd filtered by the tijd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamwedstrijd requireOneByVeld(string $veld) Return the first ChildTeamwedstrijd filtered by the veld column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeamwedstrijd requireOneByNaam(string $naam) Return the first ChildTeamwedstrijd filtered by the naam column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeamwedstrijd[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTeamwedstrijd objects based on current ModelCriteria
 * @method     ChildTeamwedstrijd[]|ObjectCollection findByTeamid(int $teamid) Return ChildTeamwedstrijd objects filtered by the teamid column
 * @method     ChildTeamwedstrijd[]|ObjectCollection findByDatum(string $datum) Return ChildTeamwedstrijd objects filtered by the datum column
 * @method     ChildTeamwedstrijd[]|ObjectCollection findByTijd(string $tijd) Return ChildTeamwedstrijd objects filtered by the tijd column
 * @method     ChildTeamwedstrijd[]|ObjectCollection findByVeld(string $veld) Return ChildTeamwedstrijd objects filtered by the veld column
 * @method     ChildTeamwedstrijd[]|ObjectCollection findByNaam(string $naam) Return ChildTeamwedstrijd objects filtered by the naam column
 * @method     ChildTeamwedstrijd[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TeamwedstrijdQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\TeamwedstrijdQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\Teamwedstrijd', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTeamwedstrijdQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTeamwedstrijdQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTeamwedstrijdQuery) {
            return $criteria;
        }
        $query = new ChildTeamwedstrijdQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTeamwedstrijd|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The Teamwedstrijd object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The Teamwedstrijd object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The Teamwedstrijd object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The Teamwedstrijd object has no primary key');
    }

    /**
     * Filter the query on the teamid column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamid(1234); // WHERE teamid = 1234
     * $query->filterByTeamid(array(12, 34)); // WHERE teamid IN (12, 34)
     * $query->filterByTeamid(array('min' => 12)); // WHERE teamid > 12
     * </code>
     *
     * @see       filterByTeam()
     *
     * @param     mixed $teamid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByTeamid($teamid = null, $comparison = null)
    {
        if (is_array($teamid)) {
            $useMinMax = false;
            if (isset($teamid['min'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_TEAMID, $teamid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamid['max'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_TEAMID, $teamid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamwedstrijdTableMap::COL_TEAMID, $teamid, $comparison);
    }

    /**
     * Filter the query on the datum column
     *
     * Example usage:
     * <code>
     * $query->filterByDatum('2011-03-14'); // WHERE datum = '2011-03-14'
     * $query->filterByDatum('now'); // WHERE datum = '2011-03-14'
     * $query->filterByDatum(array('max' => 'yesterday')); // WHERE datum > '2011-03-13'
     * </code>
     *
     * @param     mixed $datum The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByDatum($datum = null, $comparison = null)
    {
        if (is_array($datum)) {
            $useMinMax = false;
            if (isset($datum['min'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_DATUM, $datum['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datum['max'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_DATUM, $datum['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamwedstrijdTableMap::COL_DATUM, $datum, $comparison);
    }

    /**
     * Filter the query on the tijd column
     *
     * Example usage:
     * <code>
     * $query->filterByTijd('2011-03-14'); // WHERE tijd = '2011-03-14'
     * $query->filterByTijd('now'); // WHERE tijd = '2011-03-14'
     * $query->filterByTijd(array('max' => 'yesterday')); // WHERE tijd > '2011-03-13'
     * </code>
     *
     * @param     mixed $tijd The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByTijd($tijd = null, $comparison = null)
    {
        if (is_array($tijd)) {
            $useMinMax = false;
            if (isset($tijd['min'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_TIJD, $tijd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tijd['max'])) {
                $this->addUsingAlias(TeamwedstrijdTableMap::COL_TIJD, $tijd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamwedstrijdTableMap::COL_TIJD, $tijd, $comparison);
    }

    /**
     * Filter the query on the veld column
     *
     * Example usage:
     * <code>
     * $query->filterByVeld('fooValue');   // WHERE veld = 'fooValue'
     * $query->filterByVeld('%fooValue%'); // WHERE veld LIKE '%fooValue%'
     * </code>
     *
     * @param     string $veld The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByVeld($veld = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($veld)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $veld)) {
                $veld = str_replace('*', '%', $veld);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TeamwedstrijdTableMap::COL_VELD, $veld, $comparison);
    }

    /**
     * Filter the query on the naam column
     *
     * Example usage:
     * <code>
     * $query->filterByNaam('fooValue');   // WHERE naam = 'fooValue'
     * $query->filterByNaam('%fooValue%'); // WHERE naam LIKE '%fooValue%'
     * </code>
     *
     * @param     string $naam The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByNaam($naam = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($naam)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $naam)) {
                $naam = str_replace('*', '%', $naam);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TeamwedstrijdTableMap::COL_NAAM, $naam, $comparison);
    }

    /**
     * Filter the query by a related \Data\Team object
     *
     * @param \Data\Team|ObjectCollection $team The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function filterByTeam($team, $comparison = null)
    {
        if ($team instanceof \Data\Team) {
            return $this
                ->addUsingAlias(TeamwedstrijdTableMap::COL_TEAMID, $team->getId(), $comparison);
        } elseif ($team instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TeamwedstrijdTableMap::COL_TEAMID, $team->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTeam() only accepts arguments of type \Data\Team or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Team relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function joinTeam($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Team');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Team');
        }

        return $this;
    }

    /**
     * Use the Team relation Team object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\TeamQuery A secondary query class using the current class as primary query
     */
    public function useTeamQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Team', '\Data\TeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTeamwedstrijd $teamwedstrijd Object to remove from the list of results
     *
     * @return $this|ChildTeamwedstrijdQuery The current query, for fluid interface
     */
    public function prune($teamwedstrijd = null)
    {
        if ($teamwedstrijd) {
            throw new LogicException('Teamwedstrijd object has no primary key');

        }

        return $this;
    }

} // TeamwedstrijdQuery
