<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\Wedstrijd as ChildWedstrijd;
use Data\WedstrijdQuery as ChildWedstrijdQuery;
use Data\Map\WedstrijdTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'wedstrijd' table.
 *
 * 
 *
 * @method     ChildWedstrijdQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildWedstrijdQuery orderBySpeelweekid($order = Criteria::ASC) Order by the speelweekid column
 * @method     ChildWedstrijdQuery orderByTijd($order = Criteria::ASC) Order by the tijd column
 * @method     ChildWedstrijdQuery orderByVeld($order = Criteria::ASC) Order by the veld column
 *
 * @method     ChildWedstrijdQuery groupById() Group by the id column
 * @method     ChildWedstrijdQuery groupBySpeelweekid() Group by the speelweekid column
 * @method     ChildWedstrijdQuery groupByTijd() Group by the tijd column
 * @method     ChildWedstrijdQuery groupByVeld() Group by the veld column
 *
 * @method     ChildWedstrijdQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWedstrijdQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWedstrijdQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWedstrijdQuery leftJoinSpeelweek($relationAlias = null) Adds a LEFT JOIN clause to the query using the Speelweek relation
 * @method     ChildWedstrijdQuery rightJoinSpeelweek($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Speelweek relation
 * @method     ChildWedstrijdQuery innerJoinSpeelweek($relationAlias = null) Adds a INNER JOIN clause to the query using the Speelweek relation
 *
 * @method     ChildWedstrijdQuery leftJoinWTeam($relationAlias = null) Adds a LEFT JOIN clause to the query using the WTeam relation
 * @method     ChildWedstrijdQuery rightJoinWTeam($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WTeam relation
 * @method     ChildWedstrijdQuery innerJoinWTeam($relationAlias = null) Adds a INNER JOIN clause to the query using the WTeam relation
 *
 * @method     \Data\SpeelweekQuery|\Data\WTeamQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWedstrijd findOne(ConnectionInterface $con = null) Return the first ChildWedstrijd matching the query
 * @method     ChildWedstrijd findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWedstrijd matching the query, or a new ChildWedstrijd object populated from the query conditions when no match is found
 *
 * @method     ChildWedstrijd findOneById(int $id) Return the first ChildWedstrijd filtered by the id column
 * @method     ChildWedstrijd findOneBySpeelweekid(int $speelweekid) Return the first ChildWedstrijd filtered by the speelweekid column
 * @method     ChildWedstrijd findOneByTijd(string $tijd) Return the first ChildWedstrijd filtered by the tijd column
 * @method     ChildWedstrijd findOneByVeld(int $veld) Return the first ChildWedstrijd filtered by the veld column *

 * @method     ChildWedstrijd requirePk($key, ConnectionInterface $con = null) Return the ChildWedstrijd by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWedstrijd requireOne(ConnectionInterface $con = null) Return the first ChildWedstrijd matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWedstrijd requireOneById(int $id) Return the first ChildWedstrijd filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWedstrijd requireOneBySpeelweekid(int $speelweekid) Return the first ChildWedstrijd filtered by the speelweekid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWedstrijd requireOneByTijd(string $tijd) Return the first ChildWedstrijd filtered by the tijd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWedstrijd requireOneByVeld(int $veld) Return the first ChildWedstrijd filtered by the veld column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWedstrijd[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWedstrijd objects based on current ModelCriteria
 * @method     ChildWedstrijd[]|ObjectCollection findById(int $id) Return ChildWedstrijd objects filtered by the id column
 * @method     ChildWedstrijd[]|ObjectCollection findBySpeelweekid(int $speelweekid) Return ChildWedstrijd objects filtered by the speelweekid column
 * @method     ChildWedstrijd[]|ObjectCollection findByTijd(string $tijd) Return ChildWedstrijd objects filtered by the tijd column
 * @method     ChildWedstrijd[]|ObjectCollection findByVeld(int $veld) Return ChildWedstrijd objects filtered by the veld column
 * @method     ChildWedstrijd[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WedstrijdQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Data\Base\WedstrijdQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mbvolley', $modelName = '\\Data\\Wedstrijd', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWedstrijdQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWedstrijdQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWedstrijdQuery) {
            return $criteria;
        }
        $query = new ChildWedstrijdQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWedstrijd|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WedstrijdTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WedstrijdTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWedstrijd A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, speelweekid, tijd, veld FROM wedstrijd WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWedstrijd $obj */
            $obj = new ChildWedstrijd();
            $obj->hydrate($row);
            WedstrijdTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWedstrijd|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WedstrijdTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WedstrijdTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WedstrijdTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the speelweekid column
     *
     * Example usage:
     * <code>
     * $query->filterBySpeelweekid(1234); // WHERE speelweekid = 1234
     * $query->filterBySpeelweekid(array(12, 34)); // WHERE speelweekid IN (12, 34)
     * $query->filterBySpeelweekid(array('min' => 12)); // WHERE speelweekid > 12
     * </code>
     *
     * @see       filterBySpeelweek()
     *
     * @param     mixed $speelweekid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterBySpeelweekid($speelweekid = null, $comparison = null)
    {
        if (is_array($speelweekid)) {
            $useMinMax = false;
            if (isset($speelweekid['min'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_SPEELWEEKID, $speelweekid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($speelweekid['max'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_SPEELWEEKID, $speelweekid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WedstrijdTableMap::COL_SPEELWEEKID, $speelweekid, $comparison);
    }

    /**
     * Filter the query on the tijd column
     *
     * Example usage:
     * <code>
     * $query->filterByTijd('2011-03-14'); // WHERE tijd = '2011-03-14'
     * $query->filterByTijd('now'); // WHERE tijd = '2011-03-14'
     * $query->filterByTijd(array('max' => 'yesterday')); // WHERE tijd > '2011-03-13'
     * </code>
     *
     * @param     mixed $tijd The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterByTijd($tijd = null, $comparison = null)
    {
        if (is_array($tijd)) {
            $useMinMax = false;
            if (isset($tijd['min'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_TIJD, $tijd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tijd['max'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_TIJD, $tijd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WedstrijdTableMap::COL_TIJD, $tijd, $comparison);
    }

    /**
     * Filter the query on the veld column
     *
     * Example usage:
     * <code>
     * $query->filterByVeld(1234); // WHERE veld = 1234
     * $query->filterByVeld(array(12, 34)); // WHERE veld IN (12, 34)
     * $query->filterByVeld(array('min' => 12)); // WHERE veld > 12
     * </code>
     *
     * @param     mixed $veld The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterByVeld($veld = null, $comparison = null)
    {
        if (is_array($veld)) {
            $useMinMax = false;
            if (isset($veld['min'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_VELD, $veld['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($veld['max'])) {
                $this->addUsingAlias(WedstrijdTableMap::COL_VELD, $veld['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WedstrijdTableMap::COL_VELD, $veld, $comparison);
    }

    /**
     * Filter the query by a related \Data\Speelweek object
     *
     * @param \Data\Speelweek|ObjectCollection $speelweek The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterBySpeelweek($speelweek, $comparison = null)
    {
        if ($speelweek instanceof \Data\Speelweek) {
            return $this
                ->addUsingAlias(WedstrijdTableMap::COL_SPEELWEEKID, $speelweek->getId(), $comparison);
        } elseif ($speelweek instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WedstrijdTableMap::COL_SPEELWEEKID, $speelweek->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySpeelweek() only accepts arguments of type \Data\Speelweek or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Speelweek relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function joinSpeelweek($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Speelweek');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Speelweek');
        }

        return $this;
    }

    /**
     * Use the Speelweek relation Speelweek object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\SpeelweekQuery A secondary query class using the current class as primary query
     */
    public function useSpeelweekQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSpeelweek($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Speelweek', '\Data\SpeelweekQuery');
    }

    /**
     * Filter the query by a related \Data\WTeam object
     *
     * @param \Data\WTeam|ObjectCollection $wTeam the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildWedstrijdQuery The current query, for fluid interface
     */
    public function filterByWTeam($wTeam, $comparison = null)
    {
        if ($wTeam instanceof \Data\WTeam) {
            return $this
                ->addUsingAlias(WedstrijdTableMap::COL_ID, $wTeam->getWedstrijdid(), $comparison);
        } elseif ($wTeam instanceof ObjectCollection) {
            return $this
                ->useWTeamQuery()
                ->filterByPrimaryKeys($wTeam->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWTeam() only accepts arguments of type \Data\WTeam or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WTeam relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function joinWTeam($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WTeam');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WTeam');
        }

        return $this;
    }

    /**
     * Use the WTeam relation WTeam object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Data\WTeamQuery A secondary query class using the current class as primary query
     */
    public function useWTeamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWTeam($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WTeam', '\Data\WTeamQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWedstrijd $wedstrijd Object to remove from the list of results
     *
     * @return $this|ChildWedstrijdQuery The current query, for fluid interface
     */
    public function prune($wedstrijd = null)
    {
        if ($wedstrijd) {
            $this->addUsingAlias(WedstrijdTableMap::COL_ID, $wedstrijd->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the wedstrijd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WedstrijdTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WedstrijdTableMap::clearInstancePool();
            WedstrijdTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WedstrijdTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WedstrijdTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            WedstrijdTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            WedstrijdTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // WedstrijdQuery
