<?php

namespace Data\Base;

use \Exception;
use \PDO;
use Data\Klas as ChildKlas;
use Data\KlasQuery as ChildKlasQuery;
use Data\Lid as ChildLid;
use Data\LidQuery as ChildLidQuery;
use Data\Statistics as ChildStatistics;
use Data\StatisticsQuery as ChildStatisticsQuery;
use Data\Team as ChildTeam;
use Data\TeamHasLid as ChildTeamHasLid;
use Data\TeamHasLidQuery as ChildTeamHasLidQuery;
use Data\TeamQuery as ChildTeamQuery;
use Data\Teamwedstrijd as ChildTeamwedstrijd;
use Data\TeamwedstrijdQuery as ChildTeamwedstrijdQuery;
use Data\WTeam as ChildWTeam;
use Data\WTeamQuery as ChildWTeamQuery;
use Data\Map\TeamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'team' table.
 *
 * 
 *
* @package    propel.generator.Data.Base
*/
abstract class Team implements ActiveRecordInterface 
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Data\\Map\\TeamTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the klassecode field.
     * @var        string
     */
    protected $klassecode;

    /**
     * The value for the naam field.
     * @var        string
     */
    protected $naam;

    /**
     * @var        ChildKlas
     */
    protected $aKlas;

    /**
     * @var        ObjectCollection|ChildTeamHasLid[] Collection to store aggregation of ChildTeamHasLid objects.
     */
    protected $collTeamHasLids;
    protected $collTeamHasLidsPartial;

    /**
     * @var        ObjectCollection|ChildTeamwedstrijd[] Collection to store aggregation of ChildTeamwedstrijd objects.
     */
    protected $collTeamwedstrijds;
    protected $collTeamwedstrijdsPartial;

    /**
     * @var        ChildStatistics one-to-one related ChildStatistics object
     */
    protected $singleStatistics;

    /**
     * @var        ObjectCollection|ChildWTeam[] Collection to store aggregation of ChildWTeam objects.
     */
    protected $collWTeams;
    protected $collWTeamsPartial;

    /**
     * @var        ObjectCollection|ChildLid[] Cross Collection to store aggregation of ChildLid objects.
     */
    protected $collLids;

    /**
     * @var bool
     */
    protected $collLidsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLid[]
     */
    protected $lidsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTeamHasLid[]
     */
    protected $teamHasLidsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTeamwedstrijd[]
     */
    protected $teamwedstrijdsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWTeam[]
     */
    protected $wTeamsScheduledForDeletion = null;

    /**
     * Initializes internal state of Data\Base\Team object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Team</code> instance.  If
     * <code>obj</code> is an instance of <code>Team</code>, delegates to
     * <code>equals(Team)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Team The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        return array_keys(get_object_vars($this));
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [klassecode] column value.
     * 
     * @return string
     */
    public function getKlassecode()
    {
        return $this->klassecode;
    }

    /**
     * Get the [naam] column value.
     * 
     * @return string
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[TeamTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [klassecode] column.
     * 
     * @param string $v new value
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function setKlassecode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->klassecode !== $v) {
            $this->klassecode = $v;
            $this->modifiedColumns[TeamTableMap::COL_KLASSECODE] = true;
        }

        if ($this->aKlas !== null && $this->aKlas->getCode() !== $v) {
            $this->aKlas = null;
        }

        return $this;
    } // setKlassecode()

    /**
     * Set the value of [naam] column.
     * 
     * @param string $v new value
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function setNaam($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->naam !== $v) {
            $this->naam = $v;
            $this->modifiedColumns[TeamTableMap::COL_NAAM] = true;
        }

        return $this;
    } // setNaam()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : TeamTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : TeamTableMap::translateFieldName('Klassecode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->klassecode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : TeamTableMap::translateFieldName('Naam', TableMap::TYPE_PHPNAME, $indexType)];
            $this->naam = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = TeamTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Data\\Team'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aKlas !== null && $this->klassecode !== $this->aKlas->getCode()) {
            $this->aKlas = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TeamTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildTeamQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aKlas = null;
            $this->collTeamHasLids = null;

            $this->collTeamwedstrijds = null;

            $this->singleStatistics = null;

            $this->collWTeams = null;

            $this->collLids = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Team::setDeleted()
     * @see Team::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildTeamQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TeamTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aKlas !== null) {
                if ($this->aKlas->isModified() || $this->aKlas->isNew()) {
                    $affectedRows += $this->aKlas->save($con);
                }
                $this->setKlas($this->aKlas);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->lidsScheduledForDeletion !== null) {
                if (!$this->lidsScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    foreach ($this->lidsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getId();
                        $entryPk[1] = $entry->getId();
                        $pks[] = $entryPk;
                    }

                    \Data\TeamHasLidQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->lidsScheduledForDeletion = null;
                }

            }

            if ($this->collLids) {
                foreach ($this->collLids as $lid) {
                    if (!$lid->isDeleted() && ($lid->isNew() || $lid->isModified())) {
                        $lid->save($con);
                    }
                }
            }


            if ($this->teamHasLidsScheduledForDeletion !== null) {
                if (!$this->teamHasLidsScheduledForDeletion->isEmpty()) {
                    \Data\TeamHasLidQuery::create()
                        ->filterByPrimaryKeys($this->teamHasLidsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->teamHasLidsScheduledForDeletion = null;
                }
            }

            if ($this->collTeamHasLids !== null) {
                foreach ($this->collTeamHasLids as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->teamwedstrijdsScheduledForDeletion !== null) {
                if (!$this->teamwedstrijdsScheduledForDeletion->isEmpty()) {
                    foreach ($this->teamwedstrijdsScheduledForDeletion as $teamwedstrijd) {
                        // need to save related object because we set the relation to null
                        $teamwedstrijd->save($con);
                    }
                    $this->teamwedstrijdsScheduledForDeletion = null;
                }
            }

            if ($this->collTeamwedstrijds !== null) {
                foreach ($this->collTeamwedstrijds as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->singleStatistics !== null) {
                if (!$this->singleStatistics->isDeleted() && ($this->singleStatistics->isNew() || $this->singleStatistics->isModified())) {
                    $affectedRows += $this->singleStatistics->save($con);
                }
            }

            if ($this->wTeamsScheduledForDeletion !== null) {
                if (!$this->wTeamsScheduledForDeletion->isEmpty()) {
                    \Data\WTeamQuery::create()
                        ->filterByPrimaryKeys($this->wTeamsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wTeamsScheduledForDeletion = null;
                }
            }

            if ($this->collWTeams !== null) {
                foreach ($this->collWTeams as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[TeamTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TeamTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TeamTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(TeamTableMap::COL_KLASSECODE)) {
            $modifiedColumns[':p' . $index++]  = 'klassecode';
        }
        if ($this->isColumnModified(TeamTableMap::COL_NAAM)) {
            $modifiedColumns[':p' . $index++]  = 'naam';
        }

        $sql = sprintf(
            'INSERT INTO team (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':                        
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'klassecode':                        
                        $stmt->bindValue($identifier, $this->klassecode, PDO::PARAM_STR);
                        break;
                    case 'naam':                        
                        $stmt->bindValue($identifier, $this->naam, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TeamTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getKlassecode();
                break;
            case 2:
                return $this->getNaam();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Team'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Team'][$this->hashCode()] = true;
        $keys = TeamTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getKlassecode(),
            $keys[2] => $this->getNaam(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aKlas) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'klas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'klas';
                        break;
                    default:
                        $key = 'Klas';
                }
        
                $result[$key] = $this->aKlas->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collTeamHasLids) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'teamHasLids';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'team_has_lids';
                        break;
                    default:
                        $key = 'TeamHasLids';
                }
        
                $result[$key] = $this->collTeamHasLids->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTeamwedstrijds) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'teamwedstrijds';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'teamwedstrijdviews';
                        break;
                    default:
                        $key = 'Teamwedstrijds';
                }
        
                $result[$key] = $this->collTeamwedstrijds->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleStatistics) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'statistics';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'statsview';
                        break;
                    default:
                        $key = 'Statistics';
                }
        
                $result[$key] = $this->singleStatistics->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collWTeams) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'wTeams';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'w_teams';
                        break;
                    default:
                        $key = 'WTeams';
                }
        
                $result[$key] = $this->collWTeams->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Data\Team
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TeamTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Data\Team
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setKlassecode($value);
                break;
            case 2:
                $this->setNaam($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = TeamTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setKlassecode($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNaam($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Data\Team The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TeamTableMap::DATABASE_NAME);

        if ($this->isColumnModified(TeamTableMap::COL_ID)) {
            $criteria->add(TeamTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(TeamTableMap::COL_KLASSECODE)) {
            $criteria->add(TeamTableMap::COL_KLASSECODE, $this->klassecode);
        }
        if ($this->isColumnModified(TeamTableMap::COL_NAAM)) {
            $criteria->add(TeamTableMap::COL_NAAM, $this->naam);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildTeamQuery::create();
        $criteria->add(TeamTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }
        
    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Data\Team (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKlassecode($this->getKlassecode());
        $copyObj->setNaam($this->getNaam());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getTeamHasLids() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTeamHasLid($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTeamwedstrijds() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTeamwedstrijd($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getStatistics();
            if ($relObj) {
                $copyObj->setStatistics($relObj->copy($deepCopy));
            }

            foreach ($this->getWTeams() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWTeam($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Data\Team Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildKlas object.
     *
     * @param  ChildKlas $v
     * @return $this|\Data\Team The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKlas(ChildKlas $v = null)
    {
        if ($v === null) {
            $this->setKlassecode(NULL);
        } else {
            $this->setKlassecode($v->getCode());
        }

        $this->aKlas = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildKlas object, it will not be re-added.
        if ($v !== null) {
            $v->addTeam($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildKlas object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildKlas The associated ChildKlas object.
     * @throws PropelException
     */
    public function getKlas(ConnectionInterface $con = null)
    {
        if ($this->aKlas === null && (($this->klassecode !== "" && $this->klassecode !== null))) {
            $this->aKlas = ChildKlasQuery::create()->findPk($this->klassecode, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKlas->addTeams($this);
             */
        }

        return $this->aKlas;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('TeamHasLid' == $relationName) {
            return $this->initTeamHasLids();
        }
        if ('Teamwedstrijd' == $relationName) {
            return $this->initTeamwedstrijds();
        }
        if ('WTeam' == $relationName) {
            return $this->initWTeams();
        }
    }

    /**
     * Clears out the collTeamHasLids collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTeamHasLids()
     */
    public function clearTeamHasLids()
    {
        $this->collTeamHasLids = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTeamHasLids collection loaded partially.
     */
    public function resetPartialTeamHasLids($v = true)
    {
        $this->collTeamHasLidsPartial = $v;
    }

    /**
     * Initializes the collTeamHasLids collection.
     *
     * By default this just sets the collTeamHasLids collection to an empty array (like clearcollTeamHasLids());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTeamHasLids($overrideExisting = true)
    {
        if (null !== $this->collTeamHasLids && !$overrideExisting) {
            return;
        }
        $this->collTeamHasLids = new ObjectCollection();
        $this->collTeamHasLids->setModel('\Data\TeamHasLid');
    }

    /**
     * Gets an array of ChildTeamHasLid objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTeam is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTeamHasLid[] List of ChildTeamHasLid objects
     * @throws PropelException
     */
    public function getTeamHasLids(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTeamHasLidsPartial && !$this->isNew();
        if (null === $this->collTeamHasLids || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTeamHasLids) {
                // return empty collection
                $this->initTeamHasLids();
            } else {
                $collTeamHasLids = ChildTeamHasLidQuery::create(null, $criteria)
                    ->filterByTeam($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTeamHasLidsPartial && count($collTeamHasLids)) {
                        $this->initTeamHasLids(false);

                        foreach ($collTeamHasLids as $obj) {
                            if (false == $this->collTeamHasLids->contains($obj)) {
                                $this->collTeamHasLids->append($obj);
                            }
                        }

                        $this->collTeamHasLidsPartial = true;
                    }

                    return $collTeamHasLids;
                }

                if ($partial && $this->collTeamHasLids) {
                    foreach ($this->collTeamHasLids as $obj) {
                        if ($obj->isNew()) {
                            $collTeamHasLids[] = $obj;
                        }
                    }
                }

                $this->collTeamHasLids = $collTeamHasLids;
                $this->collTeamHasLidsPartial = false;
            }
        }

        return $this->collTeamHasLids;
    }

    /**
     * Sets a collection of ChildTeamHasLid objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $teamHasLids A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function setTeamHasLids(Collection $teamHasLids, ConnectionInterface $con = null)
    {
        /** @var ChildTeamHasLid[] $teamHasLidsToDelete */
        $teamHasLidsToDelete = $this->getTeamHasLids(new Criteria(), $con)->diff($teamHasLids);

        
        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->teamHasLidsScheduledForDeletion = clone $teamHasLidsToDelete;

        foreach ($teamHasLidsToDelete as $teamHasLidRemoved) {
            $teamHasLidRemoved->setTeam(null);
        }

        $this->collTeamHasLids = null;
        foreach ($teamHasLids as $teamHasLid) {
            $this->addTeamHasLid($teamHasLid);
        }

        $this->collTeamHasLids = $teamHasLids;
        $this->collTeamHasLidsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TeamHasLid objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related TeamHasLid objects.
     * @throws PropelException
     */
    public function countTeamHasLids(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTeamHasLidsPartial && !$this->isNew();
        if (null === $this->collTeamHasLids || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTeamHasLids) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTeamHasLids());
            }

            $query = ChildTeamHasLidQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTeam($this)
                ->count($con);
        }

        return count($this->collTeamHasLids);
    }

    /**
     * Method called to associate a ChildTeamHasLid object to this object
     * through the ChildTeamHasLid foreign key attribute.
     *
     * @param  ChildTeamHasLid $l ChildTeamHasLid
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function addTeamHasLid(ChildTeamHasLid $l)
    {
        if ($this->collTeamHasLids === null) {
            $this->initTeamHasLids();
            $this->collTeamHasLidsPartial = true;
        }

        if (!$this->collTeamHasLids->contains($l)) {
            $this->doAddTeamHasLid($l);
        }

        return $this;
    }

    /**
     * @param ChildTeamHasLid $teamHasLid The ChildTeamHasLid object to add.
     */
    protected function doAddTeamHasLid(ChildTeamHasLid $teamHasLid)
    {
        $this->collTeamHasLids[]= $teamHasLid;
        $teamHasLid->setTeam($this);
    }

    /**
     * @param  ChildTeamHasLid $teamHasLid The ChildTeamHasLid object to remove.
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function removeTeamHasLid(ChildTeamHasLid $teamHasLid)
    {
        if ($this->getTeamHasLids()->contains($teamHasLid)) {
            $pos = $this->collTeamHasLids->search($teamHasLid);
            $this->collTeamHasLids->remove($pos);
            if (null === $this->teamHasLidsScheduledForDeletion) {
                $this->teamHasLidsScheduledForDeletion = clone $this->collTeamHasLids;
                $this->teamHasLidsScheduledForDeletion->clear();
            }
            $this->teamHasLidsScheduledForDeletion[]= clone $teamHasLid;
            $teamHasLid->setTeam(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Team is new, it will return
     * an empty collection; or if this Team has previously
     * been saved, it will retrieve related TeamHasLids from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Team.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTeamHasLid[] List of ChildTeamHasLid objects
     */
    public function getTeamHasLidsJoinLid(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTeamHasLidQuery::create(null, $criteria);
        $query->joinWith('Lid', $joinBehavior);

        return $this->getTeamHasLids($query, $con);
    }

    /**
     * Clears out the collTeamwedstrijds collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTeamwedstrijds()
     */
    public function clearTeamwedstrijds()
    {
        $this->collTeamwedstrijds = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTeamwedstrijds collection loaded partially.
     */
    public function resetPartialTeamwedstrijds($v = true)
    {
        $this->collTeamwedstrijdsPartial = $v;
    }

    /**
     * Initializes the collTeamwedstrijds collection.
     *
     * By default this just sets the collTeamwedstrijds collection to an empty array (like clearcollTeamwedstrijds());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTeamwedstrijds($overrideExisting = true)
    {
        if (null !== $this->collTeamwedstrijds && !$overrideExisting) {
            return;
        }
        $this->collTeamwedstrijds = new ObjectCollection();
        $this->collTeamwedstrijds->setModel('\Data\Teamwedstrijd');
    }

    /**
     * Gets an array of ChildTeamwedstrijd objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTeam is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTeamwedstrijd[] List of ChildTeamwedstrijd objects
     * @throws PropelException
     */
    public function getTeamwedstrijds(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTeamwedstrijdsPartial && !$this->isNew();
        if (null === $this->collTeamwedstrijds || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTeamwedstrijds) {
                // return empty collection
                $this->initTeamwedstrijds();
            } else {
                $collTeamwedstrijds = ChildTeamwedstrijdQuery::create(null, $criteria)
                    ->filterByTeam($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTeamwedstrijdsPartial && count($collTeamwedstrijds)) {
                        $this->initTeamwedstrijds(false);

                        foreach ($collTeamwedstrijds as $obj) {
                            if (false == $this->collTeamwedstrijds->contains($obj)) {
                                $this->collTeamwedstrijds->append($obj);
                            }
                        }

                        $this->collTeamwedstrijdsPartial = true;
                    }

                    return $collTeamwedstrijds;
                }

                if ($partial && $this->collTeamwedstrijds) {
                    foreach ($this->collTeamwedstrijds as $obj) {
                        if ($obj->isNew()) {
                            $collTeamwedstrijds[] = $obj;
                        }
                    }
                }

                $this->collTeamwedstrijds = $collTeamwedstrijds;
                $this->collTeamwedstrijdsPartial = false;
            }
        }

        return $this->collTeamwedstrijds;
    }

    /**
     * Sets a collection of ChildTeamwedstrijd objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $teamwedstrijds A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function setTeamwedstrijds(Collection $teamwedstrijds, ConnectionInterface $con = null)
    {
        /** @var ChildTeamwedstrijd[] $teamwedstrijdsToDelete */
        $teamwedstrijdsToDelete = $this->getTeamwedstrijds(new Criteria(), $con)->diff($teamwedstrijds);

        
        $this->teamwedstrijdsScheduledForDeletion = $teamwedstrijdsToDelete;

        foreach ($teamwedstrijdsToDelete as $teamwedstrijdRemoved) {
            $teamwedstrijdRemoved->setTeam(null);
        }

        $this->collTeamwedstrijds = null;
        foreach ($teamwedstrijds as $teamwedstrijd) {
            $this->addTeamwedstrijd($teamwedstrijd);
        }

        $this->collTeamwedstrijds = $teamwedstrijds;
        $this->collTeamwedstrijdsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Teamwedstrijd objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Teamwedstrijd objects.
     * @throws PropelException
     */
    public function countTeamwedstrijds(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTeamwedstrijdsPartial && !$this->isNew();
        if (null === $this->collTeamwedstrijds || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTeamwedstrijds) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTeamwedstrijds());
            }

            $query = ChildTeamwedstrijdQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTeam($this)
                ->count($con);
        }

        return count($this->collTeamwedstrijds);
    }

    /**
     * Method called to associate a ChildTeamwedstrijd object to this object
     * through the ChildTeamwedstrijd foreign key attribute.
     *
     * @param  ChildTeamwedstrijd $l ChildTeamwedstrijd
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function addTeamwedstrijd(ChildTeamwedstrijd $l)
    {
        if ($this->collTeamwedstrijds === null) {
            $this->initTeamwedstrijds();
            $this->collTeamwedstrijdsPartial = true;
        }

        if (!$this->collTeamwedstrijds->contains($l)) {
            $this->doAddTeamwedstrijd($l);
        }

        return $this;
    }

    /**
     * @param ChildTeamwedstrijd $teamwedstrijd The ChildTeamwedstrijd object to add.
     */
    protected function doAddTeamwedstrijd(ChildTeamwedstrijd $teamwedstrijd)
    {
        $this->collTeamwedstrijds[]= $teamwedstrijd;
        $teamwedstrijd->setTeam($this);
    }

    /**
     * @param  ChildTeamwedstrijd $teamwedstrijd The ChildTeamwedstrijd object to remove.
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function removeTeamwedstrijd(ChildTeamwedstrijd $teamwedstrijd)
    {
        if ($this->getTeamwedstrijds()->contains($teamwedstrijd)) {
            $pos = $this->collTeamwedstrijds->search($teamwedstrijd);
            $this->collTeamwedstrijds->remove($pos);
            if (null === $this->teamwedstrijdsScheduledForDeletion) {
                $this->teamwedstrijdsScheduledForDeletion = clone $this->collTeamwedstrijds;
                $this->teamwedstrijdsScheduledForDeletion->clear();
            }
            $this->teamwedstrijdsScheduledForDeletion[]= $teamwedstrijd;
            $teamwedstrijd->setTeam(null);
        }

        return $this;
    }

    /**
     * Gets a single ChildStatistics object, which is related to this object by a one-to-one relationship.
     *
     * @param  ConnectionInterface $con optional connection object
     * @return ChildStatistics
     * @throws PropelException
     */
    public function getStatistics(ConnectionInterface $con = null)
    {

        if ($this->singleStatistics === null && !$this->isNew()) {
            $this->singleStatistics = ChildStatisticsQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleStatistics;
    }

    /**
     * Sets a single ChildStatistics object as related to this object by a one-to-one relationship.
     *
     * @param  ChildStatistics $v ChildStatistics
     * @return $this|\Data\Team The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatistics(ChildStatistics $v = null)
    {
        $this->singleStatistics = $v;

        // Make sure that that the passed-in ChildStatistics isn't already associated with this object
        if ($v !== null && $v->getTeam(null, false) === null) {
            $v->setTeam($this);
        }

        return $this;
    }

    /**
     * Clears out the collWTeams collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWTeams()
     */
    public function clearWTeams()
    {
        $this->collWTeams = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWTeams collection loaded partially.
     */
    public function resetPartialWTeams($v = true)
    {
        $this->collWTeamsPartial = $v;
    }

    /**
     * Initializes the collWTeams collection.
     *
     * By default this just sets the collWTeams collection to an empty array (like clearcollWTeams());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWTeams($overrideExisting = true)
    {
        if (null !== $this->collWTeams && !$overrideExisting) {
            return;
        }
        $this->collWTeams = new ObjectCollection();
        $this->collWTeams->setModel('\Data\WTeam');
    }

    /**
     * Gets an array of ChildWTeam objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTeam is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWTeam[] List of ChildWTeam objects
     * @throws PropelException
     */
    public function getWTeams(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWTeamsPartial && !$this->isNew();
        if (null === $this->collWTeams || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWTeams) {
                // return empty collection
                $this->initWTeams();
            } else {
                $collWTeams = ChildWTeamQuery::create(null, $criteria)
                    ->filterByTeam($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWTeamsPartial && count($collWTeams)) {
                        $this->initWTeams(false);

                        foreach ($collWTeams as $obj) {
                            if (false == $this->collWTeams->contains($obj)) {
                                $this->collWTeams->append($obj);
                            }
                        }

                        $this->collWTeamsPartial = true;
                    }

                    return $collWTeams;
                }

                if ($partial && $this->collWTeams) {
                    foreach ($this->collWTeams as $obj) {
                        if ($obj->isNew()) {
                            $collWTeams[] = $obj;
                        }
                    }
                }

                $this->collWTeams = $collWTeams;
                $this->collWTeamsPartial = false;
            }
        }

        return $this->collWTeams;
    }

    /**
     * Sets a collection of ChildWTeam objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $wTeams A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function setWTeams(Collection $wTeams, ConnectionInterface $con = null)
    {
        /** @var ChildWTeam[] $wTeamsToDelete */
        $wTeamsToDelete = $this->getWTeams(new Criteria(), $con)->diff($wTeams);

        
        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->wTeamsScheduledForDeletion = clone $wTeamsToDelete;

        foreach ($wTeamsToDelete as $wTeamRemoved) {
            $wTeamRemoved->setTeam(null);
        }

        $this->collWTeams = null;
        foreach ($wTeams as $wTeam) {
            $this->addWTeam($wTeam);
        }

        $this->collWTeams = $wTeams;
        $this->collWTeamsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WTeam objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related WTeam objects.
     * @throws PropelException
     */
    public function countWTeams(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWTeamsPartial && !$this->isNew();
        if (null === $this->collWTeams || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWTeams) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWTeams());
            }

            $query = ChildWTeamQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTeam($this)
                ->count($con);
        }

        return count($this->collWTeams);
    }

    /**
     * Method called to associate a ChildWTeam object to this object
     * through the ChildWTeam foreign key attribute.
     *
     * @param  ChildWTeam $l ChildWTeam
     * @return $this|\Data\Team The current object (for fluent API support)
     */
    public function addWTeam(ChildWTeam $l)
    {
        if ($this->collWTeams === null) {
            $this->initWTeams();
            $this->collWTeamsPartial = true;
        }

        if (!$this->collWTeams->contains($l)) {
            $this->doAddWTeam($l);
        }

        return $this;
    }

    /**
     * @param ChildWTeam $wTeam The ChildWTeam object to add.
     */
    protected function doAddWTeam(ChildWTeam $wTeam)
    {
        $this->collWTeams[]= $wTeam;
        $wTeam->setTeam($this);
    }

    /**
     * @param  ChildWTeam $wTeam The ChildWTeam object to remove.
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function removeWTeam(ChildWTeam $wTeam)
    {
        if ($this->getWTeams()->contains($wTeam)) {
            $pos = $this->collWTeams->search($wTeam);
            $this->collWTeams->remove($pos);
            if (null === $this->wTeamsScheduledForDeletion) {
                $this->wTeamsScheduledForDeletion = clone $this->collWTeams;
                $this->wTeamsScheduledForDeletion->clear();
            }
            $this->wTeamsScheduledForDeletion[]= clone $wTeam;
            $wTeam->setTeam(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Team is new, it will return
     * an empty collection; or if this Team has previously
     * been saved, it will retrieve related WTeams from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Team.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWTeam[] List of ChildWTeam objects
     */
    public function getWTeamsJoinWedstrijd(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWTeamQuery::create(null, $criteria);
        $query->joinWith('Wedstrijd', $joinBehavior);

        return $this->getWTeams($query, $con);
    }

    /**
     * Clears out the collLids collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLids()
     */
    public function clearLids()
    {
        $this->collLids = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collLids crossRef collection.
     *
     * By default this just sets the collLids collection to an empty collection (like clearLids());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initLids()
    {
        $this->collLids = new ObjectCollection();
        $this->collLidsPartial = true;

        $this->collLids->setModel('\Data\Lid');
    }

    /**
     * Checks if the collLids collection is loaded.
     *
     * @return bool
     */
    public function isLidsLoaded()
    {
        return null !== $this->collLids;
    }

    /**
     * Gets a collection of ChildLid objects related by a many-to-many relationship
     * to the current object by way of the team_has_lid cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTeam is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildLid[] List of ChildLid objects
     */
    public function getLids(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLidsPartial && !$this->isNew();
        if (null === $this->collLids || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collLids) {
                    $this->initLids();
                }
            } else {

                $query = ChildLidQuery::create(null, $criteria)
                    ->filterByTeam($this);
                $collLids = $query->find($con);
                if (null !== $criteria) {
                    return $collLids;
                }

                if ($partial && $this->collLids) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collLids as $obj) {
                        if (!$collLids->contains($obj)) {
                            $collLids[] = $obj;
                        }
                    }
                }

                $this->collLids = $collLids;
                $this->collLidsPartial = false;
            }
        }

        return $this->collLids;
    }

    /**
     * Sets a collection of Lid objects related by a many-to-many relationship
     * to the current object by way of the team_has_lid cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param  Collection $lids A Propel collection.
     * @param  ConnectionInterface $con Optional connection object
     * @return $this|ChildTeam The current object (for fluent API support)
     */
    public function setLids(Collection $lids, ConnectionInterface $con = null)
    {
        $this->clearLids();
        $currentLids = $this->getLids();

        $lidsScheduledForDeletion = $currentLids->diff($lids);

        foreach ($lidsScheduledForDeletion as $toDelete) {
            $this->removeLid($toDelete);
        }

        foreach ($lids as $lid) {
            if (!$currentLids->contains($lid)) {
                $this->doAddLid($lid);
            }
        }

        $this->collLidsPartial = false;
        $this->collLids = $lids;

        return $this;
    }

    /**
     * Gets the number of Lid objects related by a many-to-many relationship
     * to the current object by way of the team_has_lid cross-reference table.
     *
     * @param      Criteria $criteria Optional query object to filter the query
     * @param      boolean $distinct Set to true to force count distinct
     * @param      ConnectionInterface $con Optional connection object
     *
     * @return int the number of related Lid objects
     */
    public function countLids(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLidsPartial && !$this->isNew();
        if (null === $this->collLids || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLids) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getLids());
                }

                $query = ChildLidQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByTeam($this)
                    ->count($con);
            }
        } else {
            return count($this->collLids);
        }
    }

    /**
     * Associate a ChildLid to this object
     * through the team_has_lid cross reference table.
     * 
     * @param ChildLid $lid
     * @return ChildTeam The current object (for fluent API support)
     */
    public function addLid(ChildLid $lid)
    {
        if ($this->collLids === null) {
            $this->initLids();
        }

        if (!$this->getLids()->contains($lid)) {
            // only add it if the **same** object is not already associated
            $this->collLids->push($lid);
            $this->doAddLid($lid);
        }

        return $this;
    }

    /**
     * 
     * @param ChildLid $lid
     */
    protected function doAddLid(ChildLid $lid)
    {
        $teamHasLid = new ChildTeamHasLid();

        $teamHasLid->setLid($lid);

        $teamHasLid->setTeam($this);

        $this->addTeamHasLid($teamHasLid);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$lid->isTeamsLoaded()) {
            $lid->initTeams();
            $lid->getTeams()->push($this);
        } elseif (!$lid->getTeams()->contains($this)) {
            $lid->getTeams()->push($this);
        }

    }

    /**
     * Remove lid of this object
     * through the team_has_lid cross reference table.
     * 
     * @param ChildLid $lid
     * @return ChildTeam The current object (for fluent API support)
     */
    public function removeLid(ChildLid $lid)
    {
        if ($this->getLids()->contains($lid)) { $teamHasLid = new ChildTeamHasLid();

            $teamHasLid->setLid($lid);
            if ($lid->isTeamsLoaded()) {
                //remove the back reference if available
                $lid->getTeams()->removeObject($this);
            }

            $teamHasLid->setTeam($this);
            $this->removeTeamHasLid(clone $teamHasLid);
            $teamHasLid->clear();

            $this->collLids->remove($this->collLids->search($lid));
            
            if (null === $this->lidsScheduledForDeletion) {
                $this->lidsScheduledForDeletion = clone $this->collLids;
                $this->lidsScheduledForDeletion->clear();
            }

            $this->lidsScheduledForDeletion->push($lid);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aKlas) {
            $this->aKlas->removeTeam($this);
        }
        $this->id = null;
        $this->klassecode = null;
        $this->naam = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collTeamHasLids) {
                foreach ($this->collTeamHasLids as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTeamwedstrijds) {
                foreach ($this->collTeamwedstrijds as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleStatistics) {
                $this->singleStatistics->clearAllReferences($deep);
            }
            if ($this->collWTeams) {
                foreach ($this->collWTeams as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLids) {
                foreach ($this->collLids as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collTeamHasLids = null;
        $this->collTeamwedstrijds = null;
        $this->singleStatistics = null;
        $this->collWTeams = null;
        $this->collLids = null;
        $this->aKlas = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TeamTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
