<?php

namespace Data\Base;

use \Exception;
use Data\StatisticsQuery as ChildStatisticsQuery;
use Data\Team as ChildTeam;
use Data\TeamQuery as ChildTeamQuery;
use Data\Map\StatisticsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'statsview' table.
 *
 * 
 *
* @package    propel.generator.Data.Base
*/
abstract class Statistics implements ActiveRecordInterface 
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Data\\Map\\StatisticsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the teamid field.
     * @var        int
     */
    protected $teamid;

    /**
     * The value for the klassecode field.
     * @var        string
     */
    protected $klassecode;

    /**
     * The value for the naam field.
     * @var        string
     */
    protected $naam;

    /**
     * The value for the w field.
     * @var        int
     */
    protected $w;

    /**
     * The value for the p field.
     * @var        int
     */
    protected $p;

    /**
     * The value for the sv field.
     * @var        int
     */
    protected $sv;

    /**
     * The value for the st field.
     * @var        int
     */
    protected $st;

    /**
     * The value for the s field.
     * @var        int
     */
    protected $s;

    /**
     * The value for the strp field.
     * @var        int
     */
    protected $strp;

    /**
     * @var        ChildTeam
     */
    protected $aTeam;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Data\Base\Statistics object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Statistics</code> instance.  If
     * <code>obj</code> is an instance of <code>Statistics</code>, delegates to
     * <code>equals(Statistics)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Statistics The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        return array_keys(get_object_vars($this));
    }

    /**
     * Get the [teamid] column value.
     * 
     * @return int
     */
    public function getTeamid()
    {
        return $this->teamid;
    }

    /**
     * Get the [klassecode] column value.
     * 
     * @return string
     */
    public function getKlassecode()
    {
        return $this->klassecode;
    }

    /**
     * Get the [naam] column value.
     * 
     * @return string
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Get the [w] column value.
     * 
     * @return int
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * Get the [p] column value.
     * 
     * @return int
     */
    public function getP()
    {
        return $this->p;
    }

    /**
     * Get the [sv] column value.
     * 
     * @return int
     */
    public function getSv()
    {
        return $this->sv;
    }

    /**
     * Get the [st] column value.
     * 
     * @return int
     */
    public function getSt()
    {
        return $this->st;
    }

    /**
     * Get the [s] column value.
     * 
     * @return int
     */
    public function getS()
    {
        return $this->s;
    }

    /**
     * Get the [strp] column value.
     * 
     * @return int
     */
    public function getStrp()
    {
        return $this->strp;
    }

    /**
     * Set the value of [teamid] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setTeamid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->teamid !== $v) {
            $this->teamid = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_TEAMID] = true;
        }

        if ($this->aTeam !== null && $this->aTeam->getId() !== $v) {
            $this->aTeam = null;
        }

        return $this;
    } // setTeamid()

    /**
     * Set the value of [klassecode] column.
     * 
     * @param string $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setKlassecode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->klassecode !== $v) {
            $this->klassecode = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_KLASSECODE] = true;
        }

        return $this;
    } // setKlassecode()

    /**
     * Set the value of [naam] column.
     * 
     * @param string $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setNaam($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->naam !== $v) {
            $this->naam = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_NAAM] = true;
        }

        return $this;
    } // setNaam()

    /**
     * Set the value of [w] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setW($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->w !== $v) {
            $this->w = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_W] = true;
        }

        return $this;
    } // setW()

    /**
     * Set the value of [p] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setP($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->p !== $v) {
            $this->p = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_P] = true;
        }

        return $this;
    } // setP()

    /**
     * Set the value of [sv] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setSv($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sv !== $v) {
            $this->sv = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_SV] = true;
        }

        return $this;
    } // setSv()

    /**
     * Set the value of [st] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setSt($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->st !== $v) {
            $this->st = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_ST] = true;
        }

        return $this;
    } // setSt()

    /**
     * Set the value of [s] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setS($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->s !== $v) {
            $this->s = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_S] = true;
        }

        return $this;
    } // setS()

    /**
     * Set the value of [strp] column.
     * 
     * @param int $v new value
     * @return $this|\Data\Statistics The current object (for fluent API support)
     */
    protected function setStrp($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->strp !== $v) {
            $this->strp = $v;
            $this->modifiedColumns[StatisticsTableMap::COL_STRP] = true;
        }

        return $this;
    } // setStrp()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : StatisticsTableMap::translateFieldName('Teamid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->teamid = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : StatisticsTableMap::translateFieldName('Klassecode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->klassecode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : StatisticsTableMap::translateFieldName('Naam', TableMap::TYPE_PHPNAME, $indexType)];
            $this->naam = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : StatisticsTableMap::translateFieldName('W', TableMap::TYPE_PHPNAME, $indexType)];
            $this->w = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : StatisticsTableMap::translateFieldName('P', TableMap::TYPE_PHPNAME, $indexType)];
            $this->p = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : StatisticsTableMap::translateFieldName('Sv', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sv = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : StatisticsTableMap::translateFieldName('St', TableMap::TYPE_PHPNAME, $indexType)];
            $this->st = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : StatisticsTableMap::translateFieldName('S', TableMap::TYPE_PHPNAME, $indexType)];
            $this->s = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : StatisticsTableMap::translateFieldName('Strp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strp = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = StatisticsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Data\\Statistics'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aTeam !== null && $this->teamid !== $this->aTeam->getId()) {
            $this->aTeam = null;
        }
    } // ensureConsistency

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StatisticsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getTeamid();
                break;
            case 1:
                return $this->getKlassecode();
                break;
            case 2:
                return $this->getNaam();
                break;
            case 3:
                return $this->getW();
                break;
            case 4:
                return $this->getP();
                break;
            case 5:
                return $this->getSv();
                break;
            case 6:
                return $this->getSt();
                break;
            case 7:
                return $this->getS();
                break;
            case 8:
                return $this->getStrp();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Statistics'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Statistics'][$this->hashCode()] = true;
        $keys = StatisticsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getTeamid(),
            $keys[1] => $this->getKlassecode(),
            $keys[2] => $this->getNaam(),
            $keys[3] => $this->getW(),
            $keys[4] => $this->getP(),
            $keys[5] => $this->getSv(),
            $keys[6] => $this->getSt(),
            $keys[7] => $this->getS(),
            $keys[8] => $this->getStrp(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aTeam) {
                
                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'team';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'team';
                        break;
                    default:
                        $key = 'Team';
                }
        
                $result[$key] = $this->aTeam->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StatisticsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(StatisticsTableMap::COL_TEAMID)) {
            $criteria->add(StatisticsTableMap::COL_TEAMID, $this->teamid);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_KLASSECODE)) {
            $criteria->add(StatisticsTableMap::COL_KLASSECODE, $this->klassecode);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_NAAM)) {
            $criteria->add(StatisticsTableMap::COL_NAAM, $this->naam);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_W)) {
            $criteria->add(StatisticsTableMap::COL_W, $this->w);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_P)) {
            $criteria->add(StatisticsTableMap::COL_P, $this->p);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_SV)) {
            $criteria->add(StatisticsTableMap::COL_SV, $this->sv);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_ST)) {
            $criteria->add(StatisticsTableMap::COL_ST, $this->st);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_S)) {
            $criteria->add(StatisticsTableMap::COL_S, $this->s);
        }
        if ($this->isColumnModified(StatisticsTableMap::COL_STRP)) {
            $criteria->add(StatisticsTableMap::COL_STRP, $this->strp);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildStatisticsQuery::create();
        $criteria->add(StatisticsTableMap::COL_TEAMID, $this->teamid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getTeamid();

        $validPrimaryKeyFKs = 1;
        $primaryKeyFKs = [];

        //relation statsview_fk_ed5c76 to table team
        if ($this->aTeam && $hash = spl_object_hash($this->aTeam)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }
        
    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getTeamid();
    }

    /**
     * Generic method to set the primary key (teamid column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setTeamid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getTeamid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Data\Statistics (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTeamid($this->getTeamid());
        $copyObj->setKlassecode($this->getKlassecode());
        $copyObj->setNaam($this->getNaam());
        $copyObj->setW($this->getW());
        $copyObj->setP($this->getP());
        $copyObj->setSv($this->getSv());
        $copyObj->setSt($this->getSt());
        $copyObj->setS($this->getS());
        $copyObj->setStrp($this->getStrp());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Data\Statistics Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildTeam object.
     *
     * @param  ChildTeam $v
     * @return $this|\Data\Statistics The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTeam(ChildTeam $v = null)
    {
        if ($v === null) {
            $this->setTeamid(NULL);
        } else {
            $this->setTeamid($v->getId());
        }

        $this->aTeam = $v;

        // Add binding for other direction of this 1:1 relationship.
        if ($v !== null) {
            $v->setStatistics($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTeam object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTeam The associated ChildTeam object.
     * @throws PropelException
     */
    public function getTeam(ConnectionInterface $con = null)
    {
        if ($this->aTeam === null && ($this->teamid !== null)) {
            $this->aTeam = ChildTeamQuery::create()->findPk($this->teamid, $con);
            // Because this foreign key represents a one-to-one relationship, we will create a bi-directional association.
            $this->aTeam->setStatistics($this);
        }

        return $this->aTeam;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aTeam) {
            $this->aTeam->removeStatistics($this);
        }
        $this->teamid = null;
        $this->klassecode = null;
        $this->naam = null;
        $this->w = null;
        $this->p = null;
        $this->sv = null;
        $this->st = null;
        $this->s = null;
        $this->strp = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aTeam = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StatisticsTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
