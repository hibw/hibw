<?php

namespace Data;

use Data\Base\Team as BaseTeam;
				use Propel\Runtime\Propel;

/**
 * Skeleton subclass for representing a row from the 'team' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Team extends BaseTeam
{

}
