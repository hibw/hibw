<?php

namespace Data;

use Data\Base\User_GroupsQuery as BaseUser_GroupsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'user_groups' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User_GroupsQuery extends BaseUser_GroupsQuery
{
	
}
