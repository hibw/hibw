<?php

namespace Data;

use Data\Base\LidQuery as BaseLidQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'lid' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LidQuery extends BaseLidQuery
{

}
