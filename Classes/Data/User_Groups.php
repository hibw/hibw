<?php

namespace Data;

use Data\Base\User_Groups as BaseUser_Groups;

/**
 * Skeleton subclass for representing a row from the 'user_groups' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User_Groups extends BaseUser_Groups
{

}
