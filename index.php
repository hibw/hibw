<?php
// 
require __DIR__ . '/vendor/autoload.php';

// setup Propel
require_once '/generated-conf/config.php';

// First: determine if there is a session, and create the $current_user
session_start();
$current_user = null;
if (isset($_SESSION['username'])) {
	$userquery = new Data\UserQuery();
	$current_user = $userquery->requirePk($_SESSION['username']);
}

// Extract module and page and instantiate the $model
$redir = '';
if (isset($_SERVER['REDIRECT_URL']))
	$redir = $_SERVER['REDIRECT_URL'];
$home = substr($_SERVER['PHP_SELF'], 0, -1 * strlen('/index.php'));
$mod_path = substr($redir, strlen($home));
$dir = 'modules'.$mod_path;

$page='';

// Check to see if there is a view
if (!file_exists($dir.'/View.tpl')) {
	die('File not found!');
}

// Include the correct model file
$modelFile = $dir.'/Model.php';
if (file_exists($modelFile)) {
	include( $modelFile );
	if (class_exists('Model')) {
		$model = new Model($home, $home.''.$mod_path, $dir, $current_user);
	} 
} else {
	// If no specific model is present, load the default model
	$model = new Model\AbstractPageModel($home, '', $mod_path, $current_user);
}

// Check authentication
if (!$model->isAuthorized()){
	die('You are not authorized!');
}

// Check if there is an action to be done, and execute this
if(isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
		try {
			// try to Execute the action on the model
			$model->$action();
		} catch(Exception $e) {
			// And pass excpetions back to the model's errors array
			$model->errors[] = 'Caught exception: '.  $e->getMessage();
		}

		// Handle a request for a redirect
		if ($model->requestsRedirect()) {
			// Pass error and success messages to the redirect url
			$error = urlencode(serialize($model->errors));
			$success = urlencode(serialize($model->success));
			$sep = '?';
			if(strpos($model->redirect,'?') !== false)
				$sep = '&';
			//Send header
			header('Location: '.$model->redirect.$sep.'success='.$success.'&error='.$error);
			exit;
		}
}

// Load and display the template
// create object
$smarty = new Smarty;
$smarty->assign('model', $model);
$template = 'extends:site.tpl|'.$dir.'/View.tpl';
$smarty->display($template); 

?>